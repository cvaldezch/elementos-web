<?php

namespace Models\Admin;

use Core\Model;

/**
*
*/
class Social extends Model
{

	function __construct()
	{
		parent::__construct();
	}

	public function getSocial()
	{
		return $this->db->select("SELECT * FROM Social WHERE flag = true");
	}

	// public function insert_Social($data)
	// {
	// 	$this->db->insert("Social", $data);
	// }

	// public function update_Social($data, $where);
	// {
	// 	$this->mdb->update("Social", $data, $where);
	// }

}
?>