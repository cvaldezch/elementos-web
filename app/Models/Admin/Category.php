<?php

namespace Models\Admin;

use Core\Model;

/**
*
*/
class Category extends Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function getCategories()
    {
        return $this->db->select("SELECT * FROM category ORDER BY category");
    }

    public function lCategories($ids)
    {
        return $this->db->select("SELECT * FROM category WHERE category_id in (:ids)", array(':ids' => $ids));
    }

    public function nameCategory($name)
    {
        return $this->db->select("SELECT * FROM category WHERE lower(category) like lower(:name)", array(':name' => $name));
    }

    public function getCategory($id)
    {
        return $this->db->select("SELECT * FROM category WHERE category_id = :id", array(':id' => $id));
    }

  public function getCategoryId($name)
  {
    return $this->db->select("SELECT * FROM category WHERE lower(category) = lower(:name) LIMIT 1 OFFSET 0", array(':name' => $name))[0];
  }

    public function insert_category($data)
    {
        $this->db->insert("category", $data);
    }

    public function update_category($data, $where)
    {
        $this->db->update("category", $data, $where);
    }

    public function delete_category($where)
    {
        $this->db->delete("category", $where);
    }

}