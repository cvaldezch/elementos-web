<?php

namespace Models\Admin;

use Core\Model;

/**
*
*/
class Roles extends Model
{

	function __construct()
	{
		parent::__construct();
	}

	public function getRoles()
	{
		return $this->db->select("SELECT * FROM Roles WHERE flag = true ORDER BY description");
	}

  public function getLastCode()
  {
    $set = $this->db->select("SELECT rolesid FROM Roles ORDER BY register DESC LIMIT 1 OFFSET 0");
    return $set[0]->rolesid;
  }

	public function insert_roles($data)
	{
		$this->db->insert("Roles", $data);
	}

	public function update_roles($data, $where)
	{
		$this->db->update("Roles", $data, $where);
	}

	public function delete_roles($where)
	{
		$this->db->delete("Roles", $where);
	}
}