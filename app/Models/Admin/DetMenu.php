<?php

namespace Models\Admin;

use Core\Model;

/**
*
*/
class DetMenu extends Model
{

	function __construct()
	{
		parent::__construct();
	}

	public function getDetMenu()
	{
		return $this->db->select("SELECT * FROM detmenu ORDER BY menuid, position ASC");
	}

  public function getSubmenu($menuid)
  {
    return $this->db->select("SELECT * FROM detmenu WHERE menuid = :menuid ORDER BY menuid, position ASC", array(':menuid' => $menuid));
  }

	public function insert_detmenu($data)
	{
		$this->db->insert("detmenu", $data);
	}

	public function update_detmenu($data, $where)
	{
		$this->db->update("detmenu", $data, $where);
	}

	public function delete_detmenu($where)
	{
		$this->db->delete("detmenu", $where);
	}
}