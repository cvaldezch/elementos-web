<?php

namespace Models\Admin;

use Core\Model;

/**
*
*/
class Recovery extends Model
{

	function __construct()
	{
		parent::__construct();
	}

	// public function getCategories()
	// {
	// 	return $this->db->select("SELECT * FROM category ORDER BY category");
	// }
	// 
	public function getStatus($mail)
	{
		return $this->db->select("SELECT count(*) as count FROM RecoveryPwd WHERE email = :mail", array(':mail' => $mail))[0];
	}

	public function verifyRecovery($mail, $csrf)
	{
		return $this->db->select("SELECT count(*) as count FROM RecoveryPwd WHERE email = :mail and csrf = :csrf and flag = true ORDER BY register DESC LIMIT 1", array(':mail' => $mail, ':csrf' => $csrf))[0];
	}

	public function insert_Recovery($data)
	{
		$this->db->insert("RecoveryPwd", $data);
	}

	public function update_Recovery($data, $where)
	{
		$this->db->update("RecoveryPwd", $data, $where);
	}

	public function delete_Recovery($where)
	{
		$this->db->delete("RecoveryPwd", $where);
	}

}