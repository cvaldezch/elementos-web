<?php

namespace Models\Admin;

use Core\Model;

/**
*
*/
class SubMenu extends Model
{

	function __construct()
	{
		parent::__construct();
	}

	public function getSubmenu()
	{
		return $this->db->select("SELECT * FROM detmenu ORDER BY position, menuid ASC");
	}

	public function getSubmenubygroup($menu)
	{
		return $this->db->select("SELECT * FROM detmenu WHERE menuid LIKE :menu ORDER BY position, menuid ASC", array(':menu' => $menu));
	}

  public function getIdSubMenu($gname,$sname)
  {
    return $this->db->select("SELECT DISTINCT d.submenu, d.submenuid, d.menuid FROM detmenu d inner join menu m on m.menuid = d.menuid WHERE menu LIKE :gname and submenu LIKE :sname", array(':gname' => $gname, ':sname' => $sname));
  }

	public function insert_detmenu($data)
	{
		$this->db->insert("detmenu", $data);
	}

	public function update_detmenu($data, $where)
	{
		$this->db->update("detmenu", $data, $where);
	}

	public function delete_detmenu($where)
	{
		$this->db->delete("detmenu", $where);
	}
}