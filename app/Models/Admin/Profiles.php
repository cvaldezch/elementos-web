<?php

namespace Models\Admin;

use Core\Model;

/**
*
*/
class Profiles extends Model
{

	function __construct()
	{
		parent::__construct();
	}

	public function getuser($id)
	{
		return $this->db->select("SELECT * FROM users WHERE users_id = :id", array(':id' => $id));
	}

	public function getProfile($email)
	{
		return $this->db->select("SELECT * FROM Profile WHERE email LIKE :email LIMIT 1 OFFSET 0", array(':email' => $email));
	}

	public function validEmail($email)
	{
		return $this->db->select("SELECT * FROM Profile WHERE email LIKE :email", array(':email' => $email));
	}

  public function validUser($user)
  {
    return $this->db->select("SELECT * FROM users WHERE LOWER(username) LIKE LOWER(:user)", array(':user' => $user));
  }

  // public function userConfirm($token, $email, $user)
  // {
  //   return $this->db->select("SELECT * FROM users WHERE csrf LIKE :csrf AND email LIKE :email AND username LIKE :user", array(':csrf' => $token, ':email' => $email, ':user' => $user));
  // }

	public function insert_profile($data)
	{
		$this->db->insert("Profile", $data);
	}

	public function update_profile($data, $where)
	{
		$this->db->update("Profile", $data, $where);
	}

	public function insert_suscribe($data)
	{
		$result = $this->db->select("SELECT COUNT(*) as cmail FROM subcribe WHERE email LIKE :email", array(':email' => $data['email']));
		if (count($result['cmail']) == 0) {
			$this->db->insert("subcribe", $data);
		}
		return $result;
	}

}