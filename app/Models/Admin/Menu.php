<?php

namespace Models\Admin;

use Core\Model;

/**
*
*/
class Menu extends Model
{

	function __construct()
	{
		parent::__construct();
	}

	public function getMenu()
	{
		return $this->db->select("SELECT m.*,
        (SELECT COUNT(menuid) FROM detmenu d WHERE d.menuid LIKE m.menuid) as submenu
        FROM menu m ORDER BY position ASC");
	}

  public function getIdMenu($name)
  {
    return $this->db->select("SELECT DISTINCT menu, menuid FROM menu WHERE menu LIKE :name", array(':name' => $name));
  }

	public function insert_menu($data)
	{
		$this->db->insert("menu", $data);
	}

	public function update_menu($data, $where)
	{
		$this->db->update("menu", $data, $where);
	}

	public function delete_menu($where)
	{
		$this->db->delete("menu", $where);
	}
}