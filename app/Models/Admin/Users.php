<?php

namespace Models\Admin;

use Core\Model;

/**
*
*/
class Users extends Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function getUsers()
    {
        return $this->db->select("SELECT u.*, NULL as password, p.firstname, p.lastname, r.*
                                    FROM users u INNER JOIN Profile p
                                    ON u.email LIKE p.email
                                    INNER JOIN Roles r
                                    ON u.role LIKE r.rolesid
                                    WHERE u.role LIKE 'R%' and u.active = true
                                    ORDER BY u.register ASC");
    }

    public function getuser($id)
    {
        return $this->db->select("SELECT * FROM users WHERE users_id = :id and active = true", array(':id' => $id));
    }

    public function validEmail($email)
    {
        return $this->db->select("SELECT * FROM users WHERE email LIKE :email", array(':email' => $email));
    }

  public function validUser($user)
  {
    return $this->db->select("SELECT * FROM users WHERE LOWER(username) LIKE LOWER(:user)", array(':user' => $user));
  }

  public function userConfirm($token, $email, $user)
  {
    return $this->db->select("SELECT * FROM users WHERE csrf LIKE :csrf AND email LIKE :email AND username LIKE :user", array(':csrf' => $token, ':email' => $email, ':user' => $user));
  }

  public function pwdRestore($token, $email)
  {
    return $this->db->select("SELECT * FROM users WHERE csrfpwd LIKE :csrf AND email LIKE :email", array(':csrf' => $token, ':email' => $email));
  }

  public function getRequestUsers()
  {
    return $this->db->select("SELECT u.users_id, u.email, u.username, u.comment, m.menuid, m.menu, d.submenuid, d.submenu, (CONCAT(COALESCE(p.firstname, ''), ', ', COALESCE(p.lastname, ''))) as name
        FROM users u INNER JOIN Profile p
        ON u.email LIKE p.email
            INNER JOIN detmenu d
            ON u.section = d.submenuid
        INNER JOIN menu m
        ON m.menuid = d.menuid
        WHERE u.publish = false and u.section NOT LIKE ''");
  }

    public function insert_user($data)
    {
        $this->db->insert("users", $data);
    }

    public function update_user($data, $where)
    {
        $this->db->update("users", $data, $where);
    }

}