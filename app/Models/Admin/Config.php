<?php

namespace Models\Admin;

use Core\Model;

/**
*
*/
class Config extends Model
{

	function __construct()
	{
		parent::__construct();
	}

	public function getConfig()
	{
		return $this->db->select("SELECT * FROM config WHERE flag = true LIMIT 1 OFFSET 0");
	}

	public function insert_config($data)
	{
		$this->db->insert("config", $data);
	}

	public function update_config($data, $where)
	{
		$this->db->update("config", $data, $where);
	}

	public function delete_config($where)
	{
		$this->db->delete("config", $where);
	}

}