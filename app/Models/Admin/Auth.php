<?php

namespace Models\Admin;

use Core\Model;
/**
*
*/
class Auth extends Model
{

	function __construct()
	{
		parent::__construct();
	}

	public function getHash($username)
	{
		$data = $this->db->select("SELECT * FROM users WHERE username = :username and active = true", array(':username' => $username));
		return $data;//array('password' => $data[0]->password, 'email' => $data[0]->email);
	}

  public function getHashEmail($email)
  {
    $data = $this->db->select("SELECT * FROM users WHERE email LIKE :email and active = true", array(':email' => $email));
    return $data; //array('password' => $data[0]->password, 'email' => $data[0]->email);
  }

	public function getProfile($email)
	{
		return $this->db->select("SELECT * FROM Profile WHERE email LIKE :email", array(':email' => $email));
	}
}