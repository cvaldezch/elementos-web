<?php

namespace Models\Admin;

use Core\Model;

/**
*
*/
class Posts extends Model
{

  function __construct()
  {
    parent::__construct();
  }

  public function getPosts()
  {
    return $this->db->select("SELECT * FROM Posts WHERE publish <= :date AND flag LIKE :flag ORDER BY register DESC", array(':date' => strval(date("Y-m-d H:i:s")), ':flag' => '1'));
  }

  public function listPosts($limit)
  {
    return $this->db->select("SELECT *, (
        SELECT COUNT(c.posts_id) FROM Posts c WHERE c.flag = :flag
      ) as total FROM Posts WHERE flag LIKE :flag ORDER BY register DESC " .$limit, array(':flag' => '1'));
  }

  public function listPostsByTitle($title)
  {
    return $this->db->select("SELECT *, (
        SELECT COUNT(c.posts_id) FROM Posts c WHERE c.flag = :flag
      ) as total FROM Posts WHERE lower(title) LIKE lower(:title) AND flag LIKE :flag ORDER BY register DESC " .$limit, array(':flag' => '1', ':title' => '%'.$title.'%'));
  }

  public function getPost($id)
  {
    return $this->db->select("SELECT * FROM Posts WHERE posts_id = :id", array(':id' => $id));
  }

  public function lastTenPosts($limit)
  {
    return $this->db->select("SELECT p.*, (CONCAT(COALESCE(r.firstname, ''), ', ', COALESCE(r.lastname, ''))) as name,
      (
        SELECT COUNT(c.posts_id) FROM Posts c WHERE c.flag = :flag
      ) as total
      FROM Posts p INNER JOIN Profile r ON p.user_id LIKE r.email WHERE p.publish <= :date AND p.flag LIKE :flag ORDER BY p.register DESC ".$limit, array(':date' => strval(date("Y-m-d H:i:s")), ':flag' => '1'));
  }

  public function filterTag($limit, $tag)
  {
    return $this->db->select("SELECT p.*, (CONCAT(COALESCE(r.firstname, ''), ', ', COALESCE(r.lastname, ''))) as name,
      (
        SELECT COUNT(c.posts_id) FROM Posts c WHERE c.flag = :flag AND category REGEXP :tag
      ) as total
      FROM Posts p INNER JOIN Profile r ON p.user_id LIKE r.email WHERE p.publish <= :date AND p.flag LIKE :flag and category REGEXP :tag ORDER BY p.register DESC ".$limit, array(':date' => strval(date("Y-m-d H:i:s")), ':flag' => '1', ':tag' => $tag));
  }

  public function getPostSGroup($limit, $group)
  {
    return $this->db->select("SELECT p.*, (CONCAT(COALESCE(r.firstname, ''), ', ', COALESCE(r.lastname, ''))) as name,
      (
        SELECT COUNT(c.posts_id) FROM Posts c WHERE c.flag = :flag and c.sgroup = :sgroup
      ) as total
      FROM Posts p INNER JOIN Profile r ON p.user_id LIKE r.email WHERE p.publish <= :date AND p.flag LIKE :flag and p.sgroup = :sgroup ORDER BY p.register DESC ".$limit, array(':date' => strval(date("Y-m-d H:i:s")), ':flag' => '1', ':sgroup' => $group));
  }

  public function getPostSection($limit, $group, $section)
  {
    return $this->db->select("SELECT p.*, (CONCAT(COALESCE(r.firstname, ''), ', ', COALESCE(r.lastname, ''))) as name,
      (
        SELECT COUNT(c.posts_id) FROM Posts c WHERE c.flag = :flag and c.sgroup = :sgroup and c.section = :section
      ) as total
      FROM Posts p INNER JOIN Profile r ON p.user_id LIKE r.email WHERE p.publish <= :date AND p.flag LIKE :flag and p.sgroup = :sgroup and p.section = :section ORDER BY p.register DESC ".$limit, array(':date' => strval(date("Y-m-d H:i:s")), ':flag' => '1', ':sgroup' => $group, ':section' => $section));
  }

  public function getBackandNext($post)
  {
    $back = $this->db->select("SELECT * FROM Posts WHERE posts_id < :id and sgroup = :sgroup ORDER BY posts_id DESC LIMIT 1 OFFSET 0", array(":id" => $post[0]->posts_id, ":sgroup" => $post[0]->sgroup));
    $next = $this->db->select("SELECT * FROM Posts WHERE posts_id > :id and sgroup = :sgroup ORDER BY posts_id ASC LIMIT 1 OFFSET 0", array(":id" => $post[0]->posts_id, ":sgroup" => $post[0]->sgroup));
    return array("back" => $back, "next" => $next);
  }

  public function getPostByUser($email)
  {
    return $this->db->select("SELECT * FROM Posts WHERE flag = :flag and user_id LIKE :email ORDER BY register DESC", array(':flag' => '1', ':email' => $email));
  }

  public function getPostRelated($sgroup, $section)
  {
    return $this->db->select("SELECT p.* FROM Posts p, (
        SELECT posts_id AS sid
        FROM Posts
      where sgroup = :sgroup and section = :section
        ORDER BY RAND( )
        LIMIT 6
    ) tmp WHERE p.posts_id = tmp.sid and flag = true and p.section = :section  ORDER BY p.register DESC", array(':sgroup' => $sgroup, ':section' => $section));
  }

  public function getSliders()
  {
    $conf = $this->db->select("SELECT * FROM config");
    return $this->db->select("SELECT * FROM Posts WHERE publish <= :date AND flag LIKE :flag and slider LIKE :slider ORDER BY register DESC LIMIT :nslider", array(':date' => strval(date("Y-m-d H:i:s")), ':flag' => '1', ':slider' => '1', ':nslider' => intval($conf[0]->nsliders)));
  }

  public function getTotalVisited($posts)
  {
    return $this->db->select("SELECT visited FROM Posts WHERE posts_id = :posts LIMIT 1 OFFSET 0", array(":posts" => $posts))[0];
  }

  public function getPopularPost()
  {
    return $this->db->select("SELECT * FROM Posts WHERE flag = true ORDER BY visited DESC LIMIT 6 OFFSET 0");
  }

  public function getPopularDay()
  {
    return $this->db->select("SELECT posts_id, title, image FROM Posts WHERE publish = now() ORDER BY visited DESC LIMIT 1");
  }

  public function getPopularWeek()
  {
    return $this->db->select("SELECT posts_id, title, image FROM Posts WHERE publish BETWEEN DATE_SUB(now(), INTERVAL DAYOFWEEK(now()) -1 DAY) AND DATE_ADD(now(), INTERVAL 7-DAYOFWEEk(now()) DAY) ORDER BY visited DESC LIMIT 3 OFFSET 0");
  }

  public function getPopularMoth()
  {
    return $this->db->select("SELECT posts_id, title, image FROM Posts WHERE MONTH(publish) = MONTH(now()) AND YEAR(publish) = YEAR(now()) ORDER BY visited DESC LIMIT 3 OFFSET 0");
  }

  public function getPopularYear()
  {
    return $this->db->select("SELECT posts_id, title, image FROM Posts WHERE YEAR(publish) LIKE YEAR(now()) ORDER BY visited DESC LIMIT 6 OFFSET 0");
  }

  public function getPostsSearch($title)
  {
    return $this->db->select("SELECT * FROM Posts WHERE lower(title) LIKE lower(:title) AND flag LIKE :flag" .$limit, array(':flag' => '1', ':title' => '%'.$title.'%'));
  }

  // reports
  public function rptPostByYear()
  {
    return $this->db->select("SELECT posts_id, title, YEAR(publish) as year, visited FROM Posts WHERE visited in (select max(s.visited) from Posts s GROUP BY YEAR(s.publish)) and flag = true");
  }

  public function getYearPosts()
  {
    return $this->db->select("SELECT YEAR(publish) as year FROM Posts WHERE flag = true GROUP BY YEAR(publish) ORDER BY publish DESC");
  }

  public function getMonthsPosts($year)
  {
    return $this->db->select("SELECT MONTH(publish) as month FROM Posts WHERE YEAR(publish) LIKE :year AND flag = true GROUP BY MONTH(publish) ORDER BY publish", array(':year' => $year));
  }

  public function getMonthByYear($year, $month)
  {
    return $this->db->select("SELECT * FROM Posts WHERE flag = true AND year(publish) LIKE :year and month(publish) LIKE :month ORDER BY visited DESC", array(':year' => $year, ':month' => $month));
  }

  public function getMonthByVisited($year)
  {
    return $this->db->select("SELECT posts_id, title, year(publish) as year, month(publish) AS month, visited FROM Posts WHERE visited in (SELECT MAX(s.visited) FROM Posts s GROUP BY month(publish), month(s.publish)) AND flag = true AND year(publish) LIKE :year ORDER BY year(publish), month(publish) ASC LIMIT 50", array(':year' => $year));
    //return $this->db->select("SELECT * FROM Posts WHERE flag = true AND year(publish) LIKE :year and month(publish) LIKE :month ORDER BY visited DESC", array(':year' => $year, ':month' => $month));
  }

  public function getYearByVisited($year)
  {
    return $this->db->select("SELECT * FROM Posts WHERE flag = true AND year(publish) LIKE :year ORDER BY visited DESC LIMIT 50", array(':year' => $year));
  }

  public function insert_post($data)
  {
    $this->db->insert("Posts", $data);
  }

  public function update_post($data, $where)
  {
    $this->db->update("Posts", $data, $where);
  }

  public function delete_post($where)
  {
    $this->db->delete("Posts", $where);
  }

}