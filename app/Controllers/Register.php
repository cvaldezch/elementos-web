<?php

namespace Controllers;

use Core\Controller,
    Core\View,
    Models\Admin\Users;
// use Helpers\SimpleCurl;

/**
*  use for register of users
*/
class Register extends Controller
{

  function __construct()
  {
    parent::__construct();
  }

  public function usresgister()
  {
    if ($_GET['validEmail'] == true) {
      $usr = new Users();
      $valid = $usr->validEmail($_GET['email']);
      if (count($valid)) {
        echo json_encode(array('status' => true));
      }else{
        echo json_encode(array('status' => false));
      }
    }
    if ($_GET['validUser'] == true) {
      $usr = new Users();
      $valid = $usr->validUser($_GET['user']);
      if (count($valid)) {
        echo json_encode(array('status' => true));
      }else{
        echo json_encode(array('status' => false));
      }
    }
    if (count($_GET) == 1 && empty($_POST)) {
      $npost = new \Models\Admin\Config();
      $data['config'] = $npost->getConfig()[0];
      $socials = new \Models\Admin\Social();
      $data['socials'] = $socials->getSocial();
      $data['title'] = 'Regístrate';
      view::renderTemplate('header', $data);
      view::render('default/register', $data);
    }
    if (isset($_POST['ruser'])){
      $usr = new Users();
      $postdata = array(
        "username" => $_POST['user'],
        "password" => \Helpers\Password::make($_POST['passwd']),
        "email" => $_POST['email'],
        "csrf" => \Helpers\Csrf::makeToken(),
      );
      $usr->insert_user($postdata);
      $data = array(
        'email' => $_POST['email'],
        'firstname' => $_POST['firstname'],
        'lastname' => $_POST['lastname']
      );
      $profile = new \Models\Admin\Profiles();
      $profile->insert_profile($data);
      echo json_encode(array('status' => true, 'domain' => DIR, 'csrf' => $postdata['csrf'], 'email' => $postdata['email'], 'username' => $postdata['username']));
    }
  }

  public function send()
  {
    $data['title'] = 'confirmacion';
    $socials = new \Models\Admin\Social();
    $data['socials'] = $socials->getSocial();
    view::renderTemplate('header', $data);
    view::render('default/confirmreg', $data);
  }

  public function error()
  {
    $data['title'] = 'Error';
    $socials = new \Models\Admin\Social();
    $data['socials'] = $socials->getSocial();
    view::renderTemplate('header', $data);
    view::render('default/confirmfail', $data);
  }

  public function valid($token, $email, $username)
  {
    $data['title'] = 'confirmacion';
    // view::renderTemplate('header', $data);
    if (strlen($token) == 32 && filter_var($email, FILTER_VALIDATE_EMAIL) && strlen($username) > 0) {
      $_model = new Users();
      $confirm = $_model->userConfirm($token, $email, $username);
      if (count($confirm) == 1) {
        $where = array(
                      'csrf' => $token,
                      'email' => $email,
                      'username' => $username
                    );
        $update = array('active' => true);
        $_model = new Users();
        $_model->update_user($update, $where);
        $socials = new \Models\Admin\Social();
        $data['socials'] = $socials->getSocial();
        view::renderTemplate('header', $data);
        view::render('default/activereg');
        //\Helpers\Url::redirect('login');
      }else{
        $data['title'] = 'Invalido!';
        $data['msg'] = 'Los datos entregados son incorrectos. Error de validacion three.';
        view::render('error/msg', $data);
      }
    }else{
      $data['title'] = 'Invalido!';
      $data['msg'] = 'Los datos entregados son incorrectos. Error de validacion.';
      view::render('error/msg', $data);
    }
  }
}

?>