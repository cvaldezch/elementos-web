<?php

namespace Controllers;

use Core\Controller;
use Core\View;

/**
*
*/
class Post extends Controller
{

    private $_model;

    function __construct()
    {
        parent::__construct();
        $this->_model = new \Models\Admin\Posts();
    }

    public function details($id, $title)
    {
        $context['post'] = $this->_model->getPost($id);
        $conf = new \Models\Admin\Config();
        $context['config'] = $conf->getConfig()[0];
        $context['title'] = $context['post'][0]->title;

        $context['bn'] = $this->_model->getBackandNext($context['post']);

        $profile = new \Models\Admin\Profiles();
        $context['profile'] = $profile->getProfile(strval($context['post'][0]->user_id));
        $cat = new \Models\Admin\Category();
        $context['category'] = $cat->getCategories();

        $socials = new \Models\Admin\Social();
        $context['socials'] = $socials->getSocial();

        view::renderTemplate('header', $context);
        view::render('default/viewpost', $context);
        view::renderTemplate('footer', $context);
    }

    public function JSONResponse()
    {
        $context = array();
        try
        {
            if (isset($_GET['postRelated']) && $_GET['postRelated'] == true)
            {
                $recod = $this->_model->getPostRelated(intval($_GET['sgroup']), intval($_GET['section']));
                for ($i=0; $i < count($recod); $i++) {
                    $recod[$i]->uri = str_replace(' ', '_', $recod[$i]->title);
                    $recod[$i]->trunc = substr($recod[$i]->reference, 0, strpos($recod[$i]->reference, ' ' , 100));
                }
                $context['related'] = $recod;
                $context['status'] = true;
            }
            if (isset($_GET['visited']) && $_GET['visited'] == true) {
              $total = $this->_model->getTotalVisited($_GET['posts']);
              $this->_model->update_post(array("visited" => intval($total->visited) + 1), array("posts_id" => $_GET['posts']));
            }
            if ($_GET['popularday']) {
              // get data post popular day
              $context['day'] = $this->_model->getPopularDay();
              $context['status'] = true;
            }
            if ($_GET['popularweek']) {
              // get data post popular week
              $context['week'] = $this->_model->getPopularWeek();
              $context['status'] = true;
            }
            if ($_GET['popularmoth']) {
              // get data post popular month
              $context['month'] = $this->_model->getPopularMoth();
              $context['status'] = true;
            }
            if ($_GET['popularyear']) {
              // get data postpopular year
              $context['year'] = $this->_model->getPopularYear();
              $context['status'] = true;
            }
        }
        catch (Exception $e)
        {
            $context['raise'] = strval($e);
            $context['status'] = false;
        }
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($context, JSON_PRETTY_PRINT);
    }
}