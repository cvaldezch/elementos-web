<?php

namespace Controllers\Admin;

use Core\Controller,
    Core\View,
    Helpers\Session,
    Helpers\Url;

/**
* Category
*/
class Category extends Controller
{
    private $_model;

    function __construct()
    {
      if (!Session::get('loggedin')) {
        Url::redirect('login');
      }else{
        $mconfig = new \Models\Admin\Config();
        $config = $mconfig->getConfig();
        if (Session::get('roleid') != $config[0]->admin) {
          Url::redirect('');
        }
      }
      parent::__construct();
      $this->_model = new \Models\Admin\Category();
    }

    public function index()
    {
        $context['title'] = 'Category';
        if (isset($_GET['list']) && !empty($_GET['list'])) {
            echo json_encode($this->_model->getCategories());
        }else{
            View::renderTemplateAdmin('header', $context);
            View::render('admin/category', $context);
            View::renderTemplateAdmin('footer', $context);
        }
    }

    public function add()
    {
        if (isset($_POST['addTag']) && !empty($_POST['addTag'])) {
            $category = trim(strtolower($_POST['category']));
            if ($category == '') {
                $raise[] = 'Categoria esta vacia.';
            }
            if (!$raise) {
                $postdata = array('category' => $category);
                $this->_model->insert_category($postdata);
                echo json_encode( array('status' => true));
            }
        }else{
            echo json_encode( array('status' => false));
        }
    }

    public function edit()
    {
        if (isset($_POST['editTag']) && !empty($_POST['editTag'])) {
            $data = $this->_model->getCategory($_POST['category_id']);
            if (strtolower($data[0]->Category) != trim(strtolower($_POST['category']))){
                $category = $_POST['category'];
                $category_id = $_POST['category_id'];
                if ($category == '') {
                    $raise[] = 'Categoria esta vacia.';
                }
                if (!$raise) {
                    $postdata = array('category' => $category);
                    $where = array('category_id' => $category_id);
                    $this->_model->update_category($postdata, $where);
                    echo json_encode( array('status' => true));
                }
            }else{
                echo json_encode( array('status' => true));
            }
        }else{
            echo json_encode( array('status' => false));
        }
    }

    public function del()
    {
        if (isset($_POST['delTag'])) {
            $data = $this->_model->getCategory($_POST['category_id']);
            if (count($data) > 0){
                $category_id = $_POST['category_id'];
                if ($category_id == '') {
                    $raise[] = 'Categoria esta vacia.';
                }
                if (!$raise) {
                    $where = array('category_id' => $category_id);
                    $this->_model->delete_category($where);
                    echo json_encode( array('status' => true));
                }else{
                    echo json_encode( array('status_raise' => false));
                }
            }else{
                echo json_encode( array('status_row_none' => false));
            }
        }else{
            echo json_encode( array('status_None' => false));
        }
    }
};