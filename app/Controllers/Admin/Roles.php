<?php

namespace Controllers\Admin;

use Core\Controller;
use Core\View;
use Helpers\Url;
use Helpers\Session;

/**
*
*/
class Roles extends controller
{

  private $model;

  function __construct()
  {
    if (!Session::get('loggedin')) {
      Url::redirect('login');
    }else{
      $mconfig = new \Models\Admin\Config();
      $config = $mconfig->getConfig();
      if (Session::get('roleid') != $config[0]->admin) {
        Url::redirect('');
      }
    }
    parent::__construct();
    $this->model = new \Models\Admin\Roles();
  }

  private function generateId()
  {
    $old = $this->model->getLastCode();
    $new = substr(strval($old), 1);
    return sprintf("R%'.02d\n", (intval($new) + 1));
  }

  public function index()
  {
    $context['title'] = 'Admin Roles';
    view::renderTemplateAdmin('header', $context);
    view::render('admin/roles', $context);
  }

  public function ReponseJSON()
  {
    $context = array();
    try {
      $POST = json_decode(file_get_contents("php://input"),true);
      if (isset($_GET['list'])) {
        $context['roles'] = array();
        $record = $this->model->getRoles();
        foreach ($record as $row) {
          array_push($context['roles'], $row);
        }
        $context['status'] = true;
      }
      if (isset($POST['save'])) {
        $postdata = array(
          'description' => $POST['role']
        );
        if ($POST['save'] == 'new')
        {
          $postdata['rolesid'] = $this->generateId();
          $this->model->insert_roles($postdata);
          $context['status'] = true;
        }
        else if ($POST['save'] == 'edit')
        {
          $where = array(
            'rolesid' => $POST['roleid']
          );
          $this->model->update_roles($postdata, $where);
          $context['status'] = true;
        }
      }
      if (isset($POST['delete']) && $POST['delete'] == true) {
        $where = array("rolesid" => $POST['rolesid']);
        echo $this->model->delete_roles($where);
        $context['status'] = true;
        $context['post'] = $POST;
      }
    } catch (Exception $e) {
      $context['raise'] = strval($e);
      $context['status'] = false;
    }
    header('Content-Type: application/json; charset=utf-8');
    echo json_encode($context, JSON_PRETTY_PRINT);
  }

}

?>