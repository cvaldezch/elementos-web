<?php

namespace Controllers\Admin;

use Core\Controller,
  Core\View,
  \Helpers\Password,
  \Helpers\Session,
  \Helpers\Url;

/**
*
*/
class Auth extends Controller
{

  function __construct()
  {
    parent::__construct();
  }

  public function login()
  {
    $mconfig = new \Models\Admin\Config();
    $config = $mconfig->getConfig();
    if (Session::get('loggedin'))
    {
      if (Session::get('roleid') == $config[0]->admin) {
        Url::redirect('admin');
      }else{
        Url::redirect('');
      }
    }

    $model = new \Models\Admin\Auth();
    $context['title'] = 'Login';

    if (isset($_POST['submit'])) {
      $username = $_POST['username'];
      $password = $_POST['password'];
      if (!filter_var($username, FILTER_VALIDATE_EMAIL)) {
        $data = $model->getHash($_POST['username']);
      }else{
        $data = $model->getHashEmail($_POST['username']);
      }
      if (empty($data)) {
        $error[] = 'Cuenta no activa';
      }else{
        if (Password::verify($password, $data[0]->password) == 0) {
          $error[] = 'Usuario o password incorrecto.';
        } else {
          $profile = $model->getProfile($data[0]->email);
          if (count($profile) == 1) {
            Session::set('names', $profile[0]->firstname . ' ' . $profile[0]->lastname);
            Session::set('thumbnail', strval($profile[0]->photo));
            Session::set('range', $profile[0]->rphoto);
          }
          Session::set('username', $username);
          Session::set('email', $data[0]->email);
          Session::set('loggedin', true);
          if ($data[0]->role == $config[0]->admin) {
            Session::set('role', 'administrator');
            Session::set('roleid', $config[0]->admin);
            Session::set('conf', $config[0]);
            Url::redirect('admin');
          }else{
            Session::set('role', 'user');
            Session::set('roleid', $data[0]->role);
            Url::redirect('');
          }
        }
      }
    }
    View::renderTemplateAdmin('header', $context);
    View::render('admin/login', $context, $error);
  }

  public function logout()
  {
    Session::destroy();
    Url::redirect('login');
  }
}