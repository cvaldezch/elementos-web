<?php
namespace Controllers\Admin;

use Core\View;
use Core\Controller;
use Helpers\Session,
	  Helpers\Url;

/**
*
*/
class Admin extends Controller
{

	function __construct()
	{
		if (!Session::get('loggedin')) {
			Url::redirect('login');
		}else{
      $mconfig = new \Models\Admin\Config();
      $config = $mconfig->getConfig();
      if (Session::get('roleid') != $config[0]->admin) {
        Url::redirect('');
      }
    }
		parent::__construct();
	}

	public function index()
	{
		$context['title'] = "Bienvenido a Elementos";

		View::renderTemplateAdmin('header', $context);
		View::render('admin/admin', $context);
		View::renderTemplateAdmin('footer', $context);
	}
}