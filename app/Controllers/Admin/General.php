<?php

namespace Controllers\Admin;

use Core\Controller,
  Core\View,
  Helpers\Url,
  Helpers\Password,
  Helpers\Session;

/**
*
*/
class General extends Controller
{
  private $_model;

  function __construct()
  {
    if (!Session::get('loggedin')) {
      Url::redirect('login');
    }else{
      $mconfig = new \Models\Admin\Config();
      $config = $mconfig->getConfig();
      if (Session::get('roleid') != $config[0]->admin) {
        Url::redirect('');
      }
    }
    parent::__construct();
    $this->_model = new \Models\Admin\Config();
  }

  public function ResponseJSON()
  {
    $context = array();
    try {
      $POST = json_decode(file_get_contents("php://input"), true)['params'];
      if ($_GET['list']) {
        $context['config'] = $this->_model->getConfig();
        $context['status'] = true;
      }
      if ($POST['save'] == 'slider') {
        $postdata = array( 'nsliders' => $POST['nsliders']);
        $where = array('config' => 1);
        $this->_model->update_config($postdata, $where);
        $context['status'] = true;
      }
      if ($POST['save'] == 'roles') {
        $postdata = array(
          'admin' => $POST['admin'],
          'moderator' => $POST['moderator'],
          'editor' => $POST['editor']
        );
        $where = array('config' => 1);
        $this->_model->update_config($postdata, $where);
        $context['status'] = true;
      }
      if ($POST['save'] == 'npost') {
        $postdata = array(
          'npost' => $POST['npost']
        );
        $where = array('config' => 1);
        $this->_model->update_config($postdata, $where);
        $context['status'] = true;
      }
      //$context['post'] = $POST;
    } catch (Exception $e) {
      $context['raise'] = strval($e);
      $context['status'] = false;
    }
    header("Content-Type: application/json, charset=utf-8");
    echo json_encode($context, JSON_PRETTY_PRINT);
  }

  public function index()
  {
    $data['title'] = 'General';

    View::renderTemplateAdmin('header', $data);
    View::render('admin/general', $data);
  }
}