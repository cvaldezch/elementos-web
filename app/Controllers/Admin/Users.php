<?php

namespace Controllers\Admin;

use Core\Controller,
    Core\View,
    Helpers\Url,
    Helpers\Password,
    Helpers\Session;

/**
*
*/
class Users extends Controller
{
    private $_model;

    function __construct()
    {
        if (!Session::get('loggedin')) {
      Url::redirect('login');
    }else{
      $mconfig = new \Models\Admin\Config();
      $config = $mconfig->getConfig();
      if (Session::get('roleid') != $config[0]->admin) {
        Url::redirect('');
      }
    }
        parent::__construct();
        $this->_model = new \Models\Admin\Users();
    }

    public function ResponseJSON()
    {
        $context = array();
        try {
            $POST = json_decode(file_get_contents("php://input"), true)['params'];
            if ($_GET['list']) {
                $context['users'] = $this->_model->getUsers();
                $context['status'] = true;
            }

            if ($POST['deactive'] == true) {
                $postdata = array('active' => false);
                $where = array('email' => $POST['email'], 'users_id' => $POST['usersid']);
                $this->_model->update_user($postdata, $where);
                $context['status'] = true;
            }
            if ($POST['saveUsers'] == true) {
                $postdata = array('role' => 'R00', 'comment' => NULL, datepublisher => NULL, 'publish' => NULL, 'section' => NULL);
                $where = array('email' => $POST['email'], 'users_id' => $POST['usersid']);
                $this->_model->update_user($postdata, $where);
                $context['status'] = true;
            }
            if ($POST['saveAdmin'] == true) {
                // get role admin
                $mad = new \Models\Admin\Config();
                $ad = $mad->getConfig()[0];
                $postdata = array('role' => $ad->admin);
                $where = array('email' => $POST['email'], 'users_id' => $POST['usersid']);
                $this->_model->update_user($postdata, $where);
                $context['status'] = true;
            }
            if ($POST['saveEditor'] == true) {
                $mad = new \Models\Admin\Config();
                $ad = $mad->getConfig()[0];
                $postdata = array('role' => $ad->editor, 'publish' => $POST['publish'], 'section' => $POST['section'], 'datepublisher' => date("y-m-d H:i:s"));
                $where = array('users_id' => $POST['uid'], 'email' => $POST['mail']);
                $this->_model->update_user($postdata, $where);
                $context['status'] = true;
            }
            // $context['post'] = $POST;
        } catch (Exception $e) {
            $context['raise'] = strval($e);
            $context['status'] = false;
        }
        header("Content-Type: application/json, charset=utf-8");
        echo json_encode($context, JSON_PRETTY_PRINT);
    }

    public function index()
    {
        $data['title'] = 'Users';
        #$data['users'] = $this->_model->getUsers();
        View::renderTemplateAdmin('header', $data);
        View::render('admin/users', $data);
        View::renderTemplateAdmin('footer', $data);
    }

    public function add()
    {
        $data['title'] = 'Add Users';
        if (isset($_POST['submit'])) {
            $username = $_POST['username'];
            $password = $_POST['password'];
            $email = $_POST['email'];
            if ($username == '') {
                $error[] = 'Username is required';
            }
            if ($password == '') {
                $error[] = 'Password is required';
            }
            if (!filter_var($email, FILTER_VALIDATE_EMAIL))
            {
                $error[] = 'Email is required';
            }
            if (!$error) {
                $postdata = array(
                    'username' => $username,
                    'password' => Password::make($password),
                    'email' => $email
                );
                $this->_model->insert_user($postdata);

                Session::set('message', 'User Added');
                Url::redirect('admin/users');
            }
        }else{
            View::renderTemplateAdmin('header', $data);
            View::render('admin/usersadd', $data, $error);
            View::renderTemplateAdmin('footer', $data);
        }
    }

    public function edit($id='')
    {
        $data['title'] = 'Edit Users';
        $data['row'] = $this->_model->getuser($id);

        if (isset($_POST['submit'])) {
            $username = $_POST['username'];
            $password = $_POST['password'];
            $email = $_POST['email'];
            if ($username == '') {
                $error[] = 'Username is required';
            }
            if ($password == '') {
                $error[] = 'Password is required';
            }
            if (!filter_var($email, FILTER_VALIDATE_EMAIL))
            {
                $error[] = 'Email is required';
            }
            if (!$error) {
                $postdata = array(
                    'username' => $username,
                    'password' => Password::make($password),
                    'email' => $email
                );
                $where = array('users_id' => $id);
                $this->_model->update_user($postdata, $where);

                Session::set('message', 'User Modify');
                Url::redirect('admin/users');
            }
        }

        View::renderTemplateAdmin('header', $data);
        View::render('admin/usersedit', $data, $error);
        View::renderTemplateAdmin('footer', $data);
    }
}