<?php

namespace Controllers\Admin;

use Core\Controller;
use Core\View;
use Helpers\Url;
use Helpers\Session;

/**
*
*/
class Menu extends controller
{

  private $model;

  function __construct()
  {
    if (!Session::get('loggedin')) {
      Url::redirect('login');
    }else{
      $mconfig = new \Models\Admin\Config();
      $config = $mconfig->getConfig();
      if (Session::get('roleid') != $config[0]->admin) {
        Url::redirect('');
      }
    }
    parent::__construct();
    $this->model = new \Models\Admin\Menu();
  }

  public function index()
  {
    $context['title'] = 'Admin Menu';
    view::renderTemplateAdmin('header', $context);
    view::render('admin/menu', $context);
  }

  public function ReponseJSON()
  {
    $context = array();
    try {
      $POST = json_decode(file_get_contents("php://input"),true);
      if (isset($_GET['list'])) {
        $context['menus'] = array();
        $record = $this->model->getMenu();
        foreach ($record as $row) {
          array_push($context['menus'], $row);
        }
        $context['status'] = true;
      }
      if (isset($POST['save'])) {
        $postdata = array(
          'menu' => $POST['menu'],
          'url' => $POST['url'],
          'position' => $POST['position']
        );
        if ($POST['save'] == 'new')
        {
          $this->model->insert_menu($postdata);
          $context['status'] = true;
        }
        else if ($POST['save'] == 'edit')
        {
          $where = array(
            'menuid' => $POST['menuid']
          );
          $this->model->update_menu($postdata, $where);
          $context['status'] = true;
        }
      }
      if (isset($POST['delete']) && $POST['delete'] == true) {
        $where = array(
          'menuid' => $POST['menuid']
        );
        $this->model->delete_menu($where);
        $context['status'] = true;
      }
      $context['post'] = $POST;
    } catch (Exception $e) {
      $context['raise'] = strval($e);
      $context['status'] = false;
    }
    header('Content-Type: application/json; charset=utf-8');
    echo json_encode($context, JSON_PRETTY_PRINT);
  }

}

?>