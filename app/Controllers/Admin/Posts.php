<?php

namespace Controllers\Admin;

use Core\Controller,
Core\View,
Helpers\Session,
Helpers\Url;
use Helpers\Paginator;
/**
*
*/
class Posts extends Controller
{
    private $_model;

    function __construct()
    {
      if (!Session::get('loggedin')) {
        Url::redirect('login');
      }else{
        $mconfig = new \Models\Admin\Config();
        $config = $mconfig->getConfig();
        if (Session::get('roleid') != $config[0]->admin) {
          Url::redirect('');
        }
      }
      parent::__construct();
      $this->_model = new \Models\Admin\Posts();
    }

    public function index()
    {
        $data['title'] = 'Posts';
        // $pages = new Paginator('20', 'p');
        // $data['posts'] = $this->_model->listPosts($pages->getLimit());
        // $pages->setTotal($data['posts'][0]->total);
        // $data['page_links'] = $pages->pageLinks();

        View::renderTemplateAdmin('header', $data);
        View::render('admin/posts', $data);
        View::renderTemplateAdmin('footer', $data);
    }

    public function ResponseJSON()
    {
      $context = array();
      try {
        $POST = json_decode(file_get_contents("php://input"), true)['params'];
        if ($_GET['listPost']) {
          $context['pbp'] = '30';
          $pages = new Paginator($context['pbp'], 'p');
          $context['posts'] = $this->_model->listPosts($pages->getLimit());
          $pages->setTotal($context['posts'][0]->total);
          $context['total'] = $context['posts'][0]->total;
          $context['page_links'] = $pages->pageLinks();
          $context['status'] = true;
        }
        if ($_GET['listTitle']) {
          $context['posts'] = $this->_model->listPostsByTitle($_GET['title']);
          $context['status'] = true;
        }
        if ($_GET['changeSlider']) {
          $postdata = array();
          if ($_GET['status'] == 'true') {
            $postdata['slider'] = 1;
          }else{
            $postdata['slider'] = 0;
          }
          $where = array("posts_id" => $_GET['post_id']);
          $this->_model->update_post($postdata, $where);
          $context['status'] = true;
        }
        if ($_GET['rptYear']) {
          $context['year'] = $this->_model->rptPostByYear();
          $context['status'] = true;
        }
        if ($_GET['years']) {
          $context['year'] = $this->_model->getYearPosts();
          $context['status'] = true;
        }
        if ($_GET['months']) {
          $context['month'] = $this->_model->getMonthsPosts($_GET['year']);
          $context['status'] = true;
        }
        if ($_GET['postbyyear']) {
          $context['pyear'] = $this->_model->getYearByVisited($_GET['year']);
          $context['status'] = true;
        }
        if ($_GET['postbymonth']) {
          $context['pmonth'] = $this->_model->getMonthByYear($_GET['year'], $_GET['month']);
          $context['status'] = true;
        }
        if ($_GET['monthbyyear']) {
          $context['month'] = $this->_model->getMonthByVisited($_GET['year']);
          $context['status'] = true;
        }
        if ($POST['deletePost']) {
          $posts = $POST['post'];
          foreach ($posts as $post) {
            $pst = $this->_model->getPost($post);
            if (count($pst) > 0) {
              $old = getcwd() . '/app/' . $pst[0]->image;
              if (file_exists($old)) {
                unlink($old);
                $where = array('posts_id' => $post);
                $this->_model->delete_post($where);
                $context['status'] = true;
              }
            }
          }
        }
      } catch (Exception $e) {
        $context['raise'] = strval($e);
        $context['status'] = false;
      }
      header("Content-Type: application/json, charset=utf-8");
      echo json_encode($context, JSON_PRETTY_PRINT);
    }

    public function add()
    {
        $data['title'] = 'Add Post';
        $cat = new \Models\Admin\Category();
        $data['category'] = $cat->getCategories();

        if ($_POST['posts'] == true) {
            $title = $_POST['title'];
            $content = $_POST['content'];
            $reference = $_POST['reference'];
            /*if (empty($_POST['date'])) {
                $publish = date("y-m-d H:i:s");
            }else{
                $publish = strval($_POST['date']) . " " . strval($_POST['hour']);
            }*/
            $publish = date("y-m-d H:i:s");
            $slider = $_POST['slider'];
            if ($slider != '1') {
              $slider = '0';
            }
            $category = $_POST['category'];
            $ids = array();
            foreach (explode('#', $category) as $value) {
              $value = trim($value);
              if (strlen($value)) {
                // verify and insert new categories
                $cat = new \Models\Admin\Category();
                $vc = $cat->nameCategory($value);
                if (count($vc) == 0) {
                  $cat->insert_category(array('category' => $value));
                  $vc = $cat->nameCategory($value);
                  array_push($ids, $vc[0]->category_id);
                }else{
                  array_push($ids, $vc[0]->category_id);
                }
              }
            }
            $email = Session::get('email');
            $postdata = array();
            if (!empty($_FILES['image'])) {
                $file = $_FILES['image']['name'];
                $ext = pathinfo($file, PATHINFO_EXTENSION);
                $name = 'public/posts/' . strtolower(preg_replace('([^A-Za-z0-9])', '', $_POST['title'])) . str_replace(' ', '', $publish) . '.' . $ext;
                // $name = 'public/posts/' . strtolower($_POST['title']) . $publish . '.' . $ext;
                // echo json_encode(array('status'=> false, 'data'=> getcwd().$name));
                // $pre_path = dirname('inde.php');
                // echo $_FILES['image']['tmp_name'];
                // rename($_FILES['image']['tmp_name'], .getcwd().'/app/' . $name);
                if (move_uploaded_file($_FILES['image']['tmp_name'], strval(getcwd().'/app/' . $name))){
                  $postdata['image'] = $name;
                } else {
                  echo json_encode(array('status'=> false, 'data' => getcwd()));
                }
            }
            $postdata['anonymous'] = ($_POST['author'] == '1'? 1 : 0);
            $postdata['category'] = strval(implode(',', $ids));
            $postdata['reference'] = strval($reference);
            $postdata['user_id'] = strval($email);
            $postdata['publish'] = strval($publish);
            $postdata['content'] = strval($content);
            $postdata['title'] = strval($title);
            $postdata['slider'] = strval($slider);
            $postdata['sgroup'] = $_POST['menu'];
            $postdata['section'] = $_POST['submenu'];
            $this->_model->insert_post($postdata);
            echo json_encode(array('status'=> true));
        }else{
            $menu = new \Models\Admin\Menu();
            $data['menu'] = $menu->getMenu();
            $npost = new \Models\Admin\Config();
            $data['config'] = $npost->getConfig()[0];
            // $context['config'] = $post;
            View::renderTemplateAdmin('header', $data);
            View::render('admin/postsadd', $data, $error);
            //View::renderTemplateAdmin('footer', $data);
        }
    }

    public function edit($id)
    {
      $data['title'] = 'Editar Post';
      $cat = new \Models\Admin\Category();
      // $data['category'] = $cat->getCategories();
      $data['post'] = $this->_model->getPost($id);
      $npost = new \Models\Admin\Config();
      $data['config'] = $npost->getConfig()[0];
      if (isset($_POST['update'])) {
          $context = false;
          $title = $_POST['title'];
          $content = $_POST['content'];
          $reference = $_POST['reference'];
          // if ($_POST['date'] == '') {
          //   $publish = date("y-m-d H:i:s");
          // }else{
          //   $publish = $_POST['date'] . " " . $_POST['hour'];
          // }
          $publish = date("y-m-d H:i:s");
          $category = $_POST['category'];
          $ids = array();
          foreach (explode('#', $category) as $value) {
            $value = trim($value);
            if (strlen($value)) {
              // verify and insert new categories
              $cat = new \Models\Admin\Category();
              $vc = $cat->nameCategory($value);
              if (count($vc) == 0) {
                // $cat = new \Models\Admin\Category();
                $cat->insert_category(array('category' => $value));
                // $cat = new \Models\Admin\Category();
                $vc = $cat->nameCategory($value);
                array_push($ids, $vc[0]->category_id);
              }else{
                array_push($ids, $vc[0]->category_id);
              }
            }
          }
          
          $email = Session::get('email');
          $slider = $_POST['slider'];
          if ($slider != '1') {
            $slider = '0';
          }
          $postdata = array();
          if (!empty($_FILES['image'])) {
              $file = $_FILES['image']['name'];
              $ext = pathinfo($file, PATHINFO_EXTENSION);
              $name = 'public/posts/' . strtolower(preg_replace('([^A-Za-z0-9])', '', $_POST['title'])) . str_replace(' ', '', $publish) . '.' .$ext;
              if (move_uploaded_file($_FILES['image']['tmp_name'], getcwd().'/app/' . $name)){
                  $postdata['image'] = $name;
              }else{
                  echo json_encode(array('status'=> false));
              }
          }

          if (strtolower($data['post'][0]->title) != strtolower($title) && empty($_FILES['image'])) {
            // if title change
              $old = getcwd().'/app/'.$data['post'][0]->image;
              $ext = pathinfo($old, PATHINFO_EXTENSION);
              $replace = strtolower(preg_replace('([^A-Za-z0-9])', '', $_POST['title'])) . $publish .'.' .$ext;
              $name = getcwd().'/app/public/posts/'. $replace;
              // rename image
              rename($old, $name);
              $postdata['image'] = 'public/posts/'. $replace;
          }
          $postdata['anonymous'] = ($_POST['author'] == '1'? 1 : 0);
          $postdata['category'] = strval(implode(',', $ids));
          $postdata['reference'] = strval($reference);
          $postdata['user_id'] = strval($email);
          $postdata['publish'] = strval($publish);
          $postdata['modify'] = strval(date("y-m-d H:i:s"));
          $postdata['content'] = strval($content);
          $postdata['title'] = strval($title);
          $postdata['slider'] = strval($slider);
          $postdata['sgroup'] = $_POST['menu'];
          $postdata['section'] = $_POST['submenu'];
          $where = array('posts_id' => $id);
          $this->_model->update_post($postdata, $where);
          echo json_encode(array('status'=> true, 'old' => $data['post'][0]->title, 'new' => $title));
      } else {
        $menu = new \Models\Admin\Menu();
        $data['menu'] = $menu->getMenu();
        $submenu = new \Models\Admin\Submenu();
        $data['submenu'] = $submenu->getSubmenubygroup(trim($data['post'][0]->sgroup));
        $cat = new \Models\Admin\Category();
        //$ids_ = explode(',', $data['post'][0]->category);
        // $ids = array();
        // foreach ($ids_ as $value) {
        //   array_push($ids, $value);
        // }
        $cats = '';
        foreach (explode(',', $data['post'][0]->category) as $val)
        {
          $cat = new \Models\Admin\Category();
          $cats .= '#' . $cat->getCategory($val)[0]->category . ' ';
        }
        $data['categories'] = $cats;
        View::renderTemplateAdmin('header', $data);
        View::render('admin/postsedit', $data, $error);
        //View::renderTemplateAdmin('footer', $data);
      }
    }

    public function disabled($id)
    {
      $cat = new \Models\Admin\Category();
      $context['title'] = 'Deshabilitar Post';
      $context['post'] = $this->_model->getPost($id);
      $context['category'] = $cat->getCategories();
      if (isset($_POST['disabled'])) {
        $post_id = $_POST['postid'];
        if ($post_id != '') {
          $postdata['flag'] = '0';
          $where = array('posts_id' => $id);
          $this->_model->update_post($postdata, $where);
          Url::redirect('admin/posts');
        }
      }else{
        View::renderTemplateAdmin('header', $context);
        View::render('admin/postsdisabled', $context, $error);
        View::renderTemplateAdmin('footer', $context);
      }
    }
}