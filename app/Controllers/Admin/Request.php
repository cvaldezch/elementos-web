<?php

namespace Controllers\Admin;

use Core\Controller,
  Core\View,
  Helpers\Url,
  Helpers\Password,
  Helpers\Session;

/**
*
*/
class Request extends Controller
{
  private $_model;
  private $config;

  function __construct()
  {
    if (!Session::get('loggedin')) {
      Url::redirect('login');
    }else{
      $mconfig = new \Models\Admin\Config();
      $this->config = $mconfig->getConfig();
      if (Session::get('roleid') != $this->config[0]->admin) {
        Url::redirect('');
      }
    }
    parent::__construct();
    $this->_model = new \Models\Admin\Users();
  }

  public function index()
  {
    $data['title'] = 'Solicitud de Usuarios';
    $data['users'] = $this->_model->getUsers();
    $setr = new \Models\Admin\Roles();
    $data['roles'] = $setr->getRoles();
    $data['config'] = $this->config[0];

    View::renderTemplateAdmin('header', $data);
    View::render('admin/userrequest', $data);
  }

  public function ReponseJSON()
  {
    $context = array();
    try {
      $POST = json_decode(file_get_contents("php://input"), true);
      $POST = $POST['params'];
      if (isset($_GET['list'])) {
        $record = $this->_model->getRequestUsers();
        $context['users'] = $record;
        $context['status'] = true;
      }
      if (isset($POST['savepublisher'])) {
        if ($POST['publisher']) {
          $postdata = array(
            'role' => $POST['role'],
            'publish' => true,
            'datepublisher' => date("y-m-d H:i:s")
          );
        }else{
          $postdata = array(
            'role' => 'R00',
            'publish' => false,
            'section' => NULL,
            'comment' => NULL
          );
        }
        $where = array(
          'email' => $POST['email']
        );
        $this->_model->update_user($postdata, $where);
        $context['status'] = true;
      }
      $context['post'] = $POST;
    } catch (Exception $e) {
      $context['raise'] = strval($e);
      $context['status'] = false;
    }
    header('Content-Type: application/json; charset=utf-8');
    echo json_encode($context, JSON_PRETTY_PRINT);
  }

}