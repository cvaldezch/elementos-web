<?php

namespace Controllers\Admin;

use Core\Controller;
use Core\View;
use Helpers\Url;
use Helpers\Session;

/**
*
*/
class Submenu extends controller
{

  private $model;

  function __construct()
  {
    if (!Session::get('loggedin')) {
      Url::redirect('login');
    }else{
      $mconfig = new \Models\Admin\Config();
      $config = $mconfig->getConfig();
      if (Session::get('roleid') != $config[0]->admin) {
        Url::redirect('');
      }
    }
    parent::__construct();
    $this->model = new \Models\Admin\Submenu();
  }

  public function index()
  {
    $context['title'] = 'Admin Submenu';
    $menu = new \Models\Admin\Menu();
    $context['menu'] = $menu->getMenu();
    view::renderTemplateAdmin('header', $context);
    view::render('admin/submenu', $context);
  }

  public function ReponseJSON()
  {
    $context = array();
    try {
      $POST = json_decode(file_get_contents("php://input"),true);
      if (isset($_GET['list'])) {
        $context['menus'] = array();
        $record = $this->model->getSubmenubygroup($_GET['menu']);
        foreach ($record as $row) {
          array_push($context['menus'], $row);
        }
        $context['status'] = true;
      }
      if (isset($POST['save'])) {
        if ($POST['save'] == 'new')
        {
          $postdata = array(
            'menuid' => $POST['menuid'],
            'submenu' => $POST['submenu'],
            'url' => $POST['url'],
            'position' => $POST['position']
          );
          $this->model->insert_detmenu($postdata);
          $context['status'] = true;
        }
        else if ($POST['save'] == 'edit')
        {
          $postdata = array(
            'submenu' => $POST['submenu'],
            'url' => $POST['url'],
            'position' => $POST['position']
          );
          $where = array(
            'menuid' => $POST['menuid'],
            'submenuid' => $POST['submenuid']
          );
          $this->model->update_detmenu($postdata, $where);
          $context['status'] = true;
        }
      }
      if (isset($POST['delete']) && $POST['delete'] == true) {
        $where = array(
          'menuid' => $POST['menuid'],
          'submenuid' => $POST['submenuid']
        );
        $this->model->delete_detmenu($where);
        $context['status'] = true;
      }
      $context['post'] = $POST;
    } catch (Exception $e) {
      $context['raise'] = strval($e);
      $context['status'] = false;
    }
    header('Content-Type: application/json; charset=utf-8');
    echo json_encode($context, JSON_PRETTY_PRINT);
  }

}

?>