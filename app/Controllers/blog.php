<?php

namespace Controllers;

use Core\Controller;
use Core\View;
use Helpers\Session;
use Helpers\Paginator;

/**
*
*/
class Blog extends Controller
{
  private $_model;

  function __construct()
  {
    parent::__construct();
    $this->_model = new \Models\Admin\Posts();
  }

  public function index()
  {
    $npost = new \Models\Admin\Config();
    $post = $npost->getConfig()[0];
    $context['config'] = $post;
    $context['title'] = $post->sitename;

    $pages = new Paginator(strval($post->npost), 'p');

    $context['posts'] = $this->_model->lastTenPosts($pages->getLimit());
    # print_r( $context['posts']);
    $pages->setTotal($context['posts'][0]->total);

    $context['page_links'] = $pages->pageLinks();

    $context['sliders'] = $this->_model->getSliders();

    $context['popular'] = $this->_model->getPopularPost();

    $socials = new \Models\Admin\Social();
    $context['socials'] = $socials->getSocial();
    $context['searchf'] = true;

    view::renderTemplate('header', $context);
    view::renderTemplate('slider', $context);
    // print_r($this->_model->getPosts());
    view::render('default/blog', $context);
    view::renderTemplate('footer', $context);
  }

  public function ResponseJSON()
  {
    $context = array();
    try {
      if (isset($_POST['search'])) {
        $mpost = new \Models\Admin\Posts();
        $context['post'] = $mpost->getPostsSearch($_POST['search']);
        $context['status'] = true;
      }
      if (isset($_POST['suscribe']) && $_POST['suscribe'] == true) {
        $msuscribe = new \Models\Admin\Profiles();
        $context['as'] = $msuscribe->insert_suscribe(array("email" => $_POST['email']));
        $context['status'] = true;
      }
    } catch (Exception $e) {
      $context['status'] = false;
      $context['raise'] = strval($e);
    }
    header('Content-Type: application/json; charset=utf-8');
    echo json_encode($context, JSON_PRETTY_PRINT);
  }
}
