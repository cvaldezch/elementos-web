<?php

namespace Controllers;

use Core\Controller;
use Core\View;
use Helpers\Session;
use Helpers\Url;
/**
*
*/
class Profile extends Controller
{

  private $_model;

  function __construct()
  {
    if (!Session::get('loggedin')) {
      Url::redirect('login');
    }else{
      $mconfig = new \Models\Admin\Config();
      $config = $mconfig->getConfig();
    }
    parent::__construct();
    $this->_model = new \Models\Admin\Profiles();
  }

  public function index()
  {
    $data['title'] = 'Configuracion';
    $profile = $this->_model->getProfile(Session::get('email'));
    if (count($profile) > 0) {
      $data['profile'] = $profile;
    }
    $menu = new \Models\Admin\Menu();
    $data['menu'] = $menu->getMenu();
    $detm = new \Models\Admin\DetMenu();
    $data['detmenu'] = $detm->getDetMenu();
    $user = new \Models\Admin\Users();
    $data['users'] = $user->validEmail(Session::get('email'));
    $cat = new \Models\Admin\Category();
    $data['category'] = $cat->getCategories();
    view::render('default/adminprofile', $data);
  }

  public function saveProfile()
  {
    try {
      if (isset($_POST['email'])) {
        // verificar if exists profile
        $result = $this->_model->validEmail($_POST['email']);
        $postdata = array(
          'firstname' => $_POST['firstname'],
          'lastname' => $_POST['lastname'],
          'sex' => $_POST['sex'],
          'lifer' => $_POST['lifer'],
          'lifef' => $_POST['lifef'],
        );
        if (count($result) > 0) {
          $where = array(
            'profile_id' => $result[0]->profile_id,
            'email' => $_POST['email']
          );
          $this->_model->update_profile($postdata, $where);
        }else{
          $postdata['email'] = $_POST['email'];
          $this->_model->insert_profile($postdata);
        }
        echo json_encode(array('status' => true));
      }else{
        echo json_encode(array('status' => false));
      }
    } catch (Exception $e) {
      echo json_encode(array('status' => false, 'raise' => strval($e)));
    }
  }

  public function imgTmp()
  {
    try {
      if (isset($_POST['temp']) && isset($_POST['email'])) {
        if (!empty($_FILES['photo'])) {
          $file = $_FILES['photo']['name'];
          $ext = pathinfo($file, PATHINFO_EXTENSION);
          $name = 'public/ptmp/' . strtolower(str_replace(' ','_', $_POST['email']));
          $absolutePath = getcwd() . '/app/' . $name;
          if (file_exists($absolutePath . '*')) {
            unlink($absolutePath . '*');
          }
          $absolutePath = $absolutePath . '.' .$ext;
          if (move_uploaded_file($_FILES['photo']['tmp_name'], strval($absolutePath))){
            // $result = $this->_model->validEmail($_POST['email']);
            // $postdata = array(
            //   'photo' => strval($name . '.' .$ext)
            // );
            // if (count($result) > 0) {
            //   $where = array(
            //     'profile_id' => $result[0]->profile_id,
            //     'email' => $_POST['email']
            //   );
            //   $this->_model->update_profile($postdata, $where);
            // }else{
            //   $postdata['email'] = $_POST['email'];
            //   $this->_model->insert_profile($postdata);
            // }
            echo json_encode(array('status' => true, 'filename' => $name . '.' .$ext));
          }else{
            echo json_encode(array('status' => false, 'type' => 'error', 'raise' => strval('Image not saved')));
          }
        }else{
          echo json_encode(array('status' => false, 'type' => 'error', 'raise' => strval('Image not photo')));
        }
      }else{
        echo json_encode(array('status' => false, 'type' => 'error', 'raise' => strval('Image not temp and emial empty')));
      }
    } catch (Exception $e) {
      echo json_encode(array('status' => false, 'type' =>'error', 'raise' => strval($e)));
    }
  }


  public function saveAccount()
  {
    try {
      if (isset($_POST['email'])) {
        $result = $this->_model->validEmail($_POST['email']);
        $postdata = array(
          'slogan' => strval($_POST['slogan']),
          'rphoto' => floatval($_POST['range'])
        );
        if (isset($_POST['photo'])) {
          $name = explode("/", $_POST['photo']);
          $photo = 'public/profile/' . $name[count($name) - 1];
          rename(getcwd() . '/app/' . $_POST['photo'], getcwd() . '/app/' . $photo);
          $postdata['photo'] = $photo;
        }
        if (count($result) > 0) {
          $where = array(
            'profile_id' => $result[0]->profile_id,
            'email' => $_POST['email']
          );
          $this->_model->update_profile($postdata, $where);
        }else{
          $postdata['email'] = $_POST['email'];
          $this->_model->insert_profile($postdata);
        }
        echo json_encode(array('status' => true, 'type' => 'success'));
      }else{
        echo json_encode(array('status' => false, 'raise' => 'falta de params', 'type' => 'error'));
      }
    } catch (Exception $e) {
      echo json_encode(array('status' => false, 'raise' => strval($e), 'type' => 'error'));
    }
  }

  public function changePasswd()
  {
    try {
      if (isset($_POST['email'])) {
        $model = new \Models\Admin\Auth();
        $data = $model->getHashEmail($_POST['email']);
        $password = $_POST['old'];
        if (\Helpers\Password::verify($password, $data[0]->password) == 0) {
          echo json_encode(array('status' => false, 'raise' => strval('La contraseña actual ingresa no coincide.'), 'type' => 'error'));
        } else {
          $postdata = array(
            'password' => \Helpers\Password::make($_POST['passwd'])
          );
          $where = array("email" => $_POST['email'], "users_id" => $data[0]->users_id);
          $user = new \Models\Admin\Users();
          $user->update_user($postdata, $where);
          echo json_encode(array('status' => true, 'type' => 'success'));
          $mail = new \Helpers\PhpMailer\Mail();
          $mail->setFrom('noreply@elementos.com.pe');
          $mail->addAddress($_POST['email']);
          $mail->subject('Cambio de Contraseña');
          $cuerpo = '<html lang="es">
                      <head>
                        <meta charset="utf-8">
                        <title>Elementos</title>
                      </head>
                      <body>
                      <div style="text-align: center;">
                          <a href="http://www.elementos.com.pe" title="Elements">
                            <img style="height: 95px;  width: 86px;" src="http://www.elementos.com.pe/img/logo.png" alt="Elementos">
                          </a>
                      </div>
                      <h3>Cambio de contraseña</h3>
                      <p>
                        La contraseña para su cuenta "'. $_POST['email'] .'", se a cambiado recientemente.
                      </p>
                      <p>
                        Si la has cambiado tú, no necesitas realizar ninguna otra acción.
                      </p>
                      <p>
                        Si no la has cambiado tú, es posible que tu cuenta haya sido vulnerada. Para poder volver a iniciar sesión en tu cuenta, deberás restablecer la contraseña.
                      </p>
                      <p>
                        <a href="#">Restablecer Contraseña</a>
                      </p>
                      <p>
                        <small style="text-align:center;">
                          Los signos ortográficos fueron omitidos intencionalmente en este correo electrónico.<br/>
                                Estás suscrito con la siguiente dirección: '.$_POST['email'].' Por favor, no respondas a este correo electrónico.
                        </small>
                      </p>
                      </body>
                      </html>';
          $mail->body($cuerpo);
          $mail->send();
        }
      }
    } catch (Exception $e) {
      echo json_encode(array('status' => false, 'raise' => strval($e), 'type' => 'error'));
    }
  }

  public function sendPublish()
  {
    header('Content-Type: application/json; charset=utf-8');
    try {
      if (isset($_POST['email'])) {
        $users = new \Models\Admin\Users();
        $postdata = array(
          'section' => $_POST['section'],
          'comment' => $_POST['comment']
        );
        $where = array('email' => $_POST['email']);
        $users->update_user($postdata, $where);
        echo json_encode(array('status' => true, 'raise' => strval($e), 'type' => 'success'));
      }
    } catch (Exception $e) {
      echo json_encode(array('status' => false, 'raise' => strval($e), 'type' => 'error'));
    }
  }

  public function savePost()
  {
    header('Content-Type: application/json; charset=utf-8');
    try {
      if (isset($_POST['savePost'])) {
        $title = $_POST['title'];
        $content = $_POST['content'];
        $reference = $_POST['references'];
        if (empty($_POST['date'])) {
            $publish = date("y-m-d H:i:s");
        }else{
            $publish = $_POST['date'] . " " . $_POST['hour'];
        }
        $slider = $_POST['slider'];
        if ($slider != '1') {
          $slider = '0';
        }
        $category = $_POST['category'];
        $email = Session::get('email');
        $postdata = array();
        if (!empty($_FILES['image'])) {
            $file = $_FILES['image']['name'];
            $ext = pathinfo($file, PATHINFO_EXTENSION);
            $name = 'public/posts/' . strtolower(str_replace(' ','_', $_POST['title'])) . $publish .'.' .$ext;
            if (move_uploaded_file($_FILES['image']['tmp_name'], getcwd().'/app/' . $name)){
                $postdata['image'] = $name;
            }else{
                echo json_encode(array('status'=> false));
            }
        }
        $postdata['category'] = strval($category);
        $postdata['reference'] = strval($reference);
        $postdata['user_id'] = strval($email);
        $postdata['publish'] = strval($publish);
        $postdata['content'] = strval($content);
        $postdata['title'] = strval($title);
        $postdata['slider'] = strval($slider);
        $postdata['sgroup'] = strval($_POST['sgroup']);
        $postdata['section'] = strval($_POST['section']);
        $post = new \Models\Admin\Posts();
        $post->insert_post($postdata);
        echo json_encode(array('status'=> true));
      }
      if (isset($_POST['editPost'])) {
        $post = new \Models\Admin\Posts();
        $data['post'] = $post->getPost($_POST['posts_id']);
        $title = $_POST['title'];
        $content = $_POST['content'];
        $reference = $_POST['references'];
        if ($_POST['date'] == '') {
          $publish = date("y-m-d H:i:s");
        }else{
          $publish = $_POST['date'] . " " . $_POST['hour'];
        }
        $category = $_POST['category'];
        $email = $data['post'][0]->user_id; //Session::get('email');
        $slider = '0';
        $postdata = array();
        if (!empty($_FILES['image'])) {
          $file = $_FILES['image']['name'];
          $ext = pathinfo($file, PATHINFO_EXTENSION);
          $name = 'public/posts/' . strtolower(str_replace(' ','_', $_POST['title'])) . $publish . '.' .$ext;
          if (move_uploaded_file($_FILES['image']['tmp_name'], getcwd().'/app/' . $name)){
            $postdata['image'] = $name;
          }else{
            echo json_encode(array('status'=> false, 'raise' => 'image no load'));
          }
        }
        if (strtolower($data['post'][0]->title) != strtolower($title) && empty($_FILES['image'])) {
          // if title change
          $old = getcwd().'/app/'.$data['post'][0]->image;
          $ext = pathinfo($old, PATHINFO_EXTENSION);
          $replace = strtolower(str_replace(' ','_', $_POST['title'])) . $publish .'.' .$ext;
          $name = getcwd().'/app/public/posts/'. $replace;
          // rename image
          rename($old, $name);
          $postdata['image'] = 'public/posts/'. $replace;
        }
        $postdata['category'] = strval($category);
        $postdata['reference'] = strval($reference);
        $postdata['user_id'] = strval($email);
        $postdata['publish'] = strval($publish);
        $postdata['modify'] = strval(date("y-m-d H:i:s"));
        $postdata['content'] = strval($content);
        $postdata['title'] = strval($title);
        $postdata['slider'] = strval($slider);
        $where = array('posts_id' => $_POST['posts_id']);
        $post->update_post($postdata, $where);
        echo json_encode(array('status'=> true, 'old' => $data['post'][0]->title, 'new' => $title));
      }
    } catch (Exception $e) {
      echo json_encode(array('status' => false, 'raise' => strval($e), 'type' => 'error'));
    }
  }

  public function ReponseJSON()
  {
    $context = array();
    try {
      $POST = json_decode(file_get_contents("php://input"),true);
      if (isset($_GET['list'])) {
        $post = new \Models\Admin\Posts();
        $context['posts'] = $post->getPostByUser(Session::get('email'));
        $context['status'] = true;
      }
    } catch (Exception $e) {
      $context['raise'] = strval($e);
      $context['status'] = false;
    }
    header('Content-Type: application/json; charset=utf-8');
    echo json_encode($context, JSON_PRETTY_PRINT);
  }

}
?>