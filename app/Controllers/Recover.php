<?php

namespace Controllers;

use Core\Controller;
use Core\View;
use Helpers\Url;
/**
*
*/
class Recover extends Controller
{

  private $_model;

  function __construct()
  {
    parent::__construct();
    $this->_model = new \Models\Admin\Users();
  }

  public function registerRestore()
  {
    try {
      if (isset($_POST['email'])) {
        $postdata = array('csrfpwd' => \Helpers\Csrf::makeToken());
        $where = array('email' => $_POST['email']);
        $this->_model->update_user($postdata, $where);
        echo json_encode(array('status' => true));
        $mail = new \Helpers\PhpMailer\Mail();
        $mail->setFrom('noreply@elementos.com.pe');
        $mail->addAddress($_POST['email']);
        $mail->subject('Recuperar Contraseña');
        $cuerpo = '<html lang="es">
                    <head>
                      <meta charset="utf-8">
                      <title>Elementos</title>
                    </head>
                    <body>
                    <div style="text-align: center;">
                        <a href="http://www.elementos.com.pe" title="Elements">
                          <img style="height: 95px;  width: 86px;" src="http://www.elementos.com.pe/img/logo.png" alt="Elementos">
                        </a>
                    </div>
                    <h3>Solicitud cambio de contraseña</h3>
                    <p>
                      La contraseña para su cuenta "'. $_POST['email'] .'", se a cambiado recientemente.
                    </p>
                    <p>
                      Si no has solicitado cambiar tú contraseña, no necesitas realizar ninguna otra acción.
                    </p>
                    <p>
                      Si has solicitado cambiar tú contraseña,  deberás restablecer realizarlo desde el siguiente formulario.
                    </p>
                    <p>
                      <a href="'. DIR .'recover/passwd/'.$postdata['csrfpwd'].'/'.$_POST['email'].'">Recuperar Contraseña</a>
                    </p>
                    <p>
                      <small style="text-align:center;">
                        Los signos ortográficos fueron omitidos intencionalmente en este correo electrónico.<br/>
                              Estás suscrito con la siguiente dirección: '.$_POST['email'].' Por favor, no respondas a este correo electrónico.
                      </small>
                    </p>
                    </body>
                    </html>';
        $mail->body($cuerpo);
        $mail->send();
      }
    }catch (Exception $e) {
      echo json_encode(array('status' => false, 'raise' => strval($e)));
    }
  }

  public function recoverPasswd($token, $email)
  {
    if (strlen($token) == 32 && filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $usr = $this->_model->pwdRestore($token, $email);
      //$rec = \Models\Admin\Recovery();
      if (count($user) > 0) {
        $data['title'] = 'Recuperar Contraseña';
        $data['csrf'] = $token;
        $data['email'] = $email;
        view::render('default/getpassword', $data);
      }else{
        view::renderTemplate('header');
        view::render('error/404');
      }
    }else{
      view::renderTemplate('header');
      view::render('error/404');
    }
  }

  public function restorePwd()
  {
    try {
      if (isset($_POST['email'])) {
        $postdata = array(
          'password' => \Helpers\Password::make($_POST['password']),
          'csrfpwd' => ""
        );
        $where = array('email' => $_POST['email'], 'csrfpwd' => $_POST['csrf']);
        $this->_model->update_user($postdata, $where);
        echo json_encode(array('status' => true, 'type' => 'success'));
        $mail = new \Helpers\PhpMailer\Mail();
        $mail->setFrom('noreply@elementos.com.pe');
        $mail->addAddress($_POST['email']);
        $mail->subject('Cambio de Contraseña');
        $cuerpo = '<html lang="es">
                    <head>
                      <meta charset="utf-8">
                      <title>Elementos</title>
                    </head>
                    <body>
                    <div style="text-align: center;">
                        <a href="http://www.elementos.com.pe" title="Elements">
                          <img style="height: 95px;  width: 86px;" src="http://www.elementos.com.pe/img/logo.png" alt="Elementos">
                        </a>
                    </div>
                    <h3>Cambio de contraseña</h3>
                    <p>
                      La contraseña para su cuenta "'. $_POST['email'] .'", se a cambiado recientemente.
                    </p>
                    <p>
                      Si la has cambiado tú, no necesitas realizar ninguna otra acción.
                    </p>
                    <p>
                      Si no la has cambiado tú, es posible que tu cuenta haya sido vulnerada. Para poder volver a iniciar sesión en tu cuenta, deberás restablecer la contraseña.
                    </p>
                    <p>
                      <a href="#">Restablecer Contraseña</a>
                    </p>
                    <p>
                      <small style="text-align:center;">
                        Los signos ortográficos fueron omitidos intencionalmente en este correo electrónico.<br/>
                              Estás suscrito con la siguiente dirección: '.$_POST['email'].' Por favor, no respondas a este correo electrónico.
                      </small>
                    </p>
                    </body>
                    </html>';
        $mail->body($cuerpo);
        $mail->send();
      }
    } catch (Exception $e) {
      echo json_encode(array('status' => false, 'raise' => strval($e)));
    }
  }

}
?>