<?php

namespace Controllers;

use Core\Controller,
  Core\View,
  Helpers\Url,
  Helpers\Password,
  Helpers\Session;

/**
*
*/
class Section extends Controller
{
  private $_model;

  function __construct()
  {
    parent::__construct();
    $this->_model = new \Models\Admin\Posts();
  }

  public function index($g, $s='')
  {
    $data['title'] = 'elementos';
    $npost = new \Models\Admin\Config();
    $post = $npost->getConfig()[0];
    $data['config'] = $post;
    $data['title'] = $post->sitename;
    $pages = new \Helpers\Paginator(strval($post->npost), 'p');
    if (!empty($g) && empty($s) || $s == '')
    {
      $menu = new \Models\Admin\Menu();
      $id = $menu->getIdMenu($g);
      $data['posts'] = $this->_model->getPostSGroup($pages->getLimit(), $id[0]->menuid);
      $data['group'] = $g;
    }
    else if(!empty($g) && !empty($s))
    {
      // $menu = new \Models\Admin\Menu();
      // $gid = $menu->getIdMenu($g);
      $data['group'] = $g;
      $submenu = new \Models\Admin\Submenu();
      $sid = $submenu->getIdSubMenu($g, $s);

      $data['posts'] = $this->_model->getPostSection($pages->getLimit(), $sid[0]->menuid, $sid[0]->submenuid);
      $data['section'] = $s;
    }
    $data['popular'] = $this->_model->getPopularPost();
    $pages->setTotal($data['posts'][0]->total);
    $data['page_links'] = $pages->pageLinks();
    $socials = new \Models\Admin\Social();
    $data['socials'] = $socials->getSocial();
    View::renderTemplate('header', $data);
    // echo $g;
    // print_r($_GET);
    //print_r($data);
    View::render('default/sections', $data);
    View::renderTemplate('footer', $data);
  }
}