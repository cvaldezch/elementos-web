<?php

namespace Controllers;

use \Core\Controller;
use \Core\View;
use \Helpers\Session;
use \Helpers\Paginator;
use \Models\Admin\Posts;

/**
* Filter posts by tags
*/
class Tags extends Controller
{

  private $_model;

  function __construct()
  {
    parent::__construct();
    $this->_model = new Posts();
  }

  public function index($strtag)
  {
    $npost = new \Models\Admin\Config();
    $post = $npost->getConfig();
    $context['title'] = $strtag;

    $tagm = new \Models\Admin\Category();
    $tid = $tagm->getCategoryId($strtag);

    $pages = new Paginator(strval($post[0]->npost), 'p');

    $context['posts'] = $this->_model->filterTag( $pages->getLimit(), $tid->category_id );
    $pages->setTotal($context['posts'][0]->total);

    $context['page_links'] = $pages->pageLinks();

    // $context['sliders'] = $this->_model->getSliders();

    $context['popular'] = $this->_model->getPopularPost();
    $socials = new \Models\Admin\Social();
    $context['socials'] = $socials->getSocial();
    view::renderTemplate('header', $context);
    // view::renderTemplate('slider', $context);
    view::render('default/filtertag', $context);
    view::renderTemplate('footer', $context);
  }
}

?>