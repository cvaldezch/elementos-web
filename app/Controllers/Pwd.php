<?php

namespace Controllers;

use \Core\Controller;
use \Helpers\Password;
use \Core\View;
/**
*
*/
class Pwd extends Controller
{

    function __construct()
    {
        parent::__construct();
    }

    public function make()
    {

        $m = new \Models\Admin\Posts();
        print_r($m->getPopularMoth());
        if (isset($_POST['pwd'])) {
            echo Password::make($_POST['pwd']);
        }else{
            echo "<form action=\"\" method=\"post\"><label class=\"control-label\">Ingrese su Password</label><br><input type=\"password\" name=\"pwd\"><br><input type=\"submit\" value=\"Generar Passwrod\"></form>";
            // echo strlen(\Helpers\Csrf::makeToken());
            // echo "<br>";
            echo \Helpers\Csrf::makeToken();
        }
    }

    public function makeimg()
    {
        $posts = new \Models\Admin\getPosts();
        foreach ($posts as $row) {
            $ext = explode('.', $row->image);
            $publish = $row->publish->format('Y-m-dH:i:s');
            $name = 'public/posts/' . strtolower(preg_replace('([^A-Za-z0-9])', '', $row->title)) . $publish . $ext[count($ext)-1];
            $path = getcwd().'/app/';
            rename($path.$row->image, $path.$name);
            $postdata = array('image' => $name);
            $where = array('posts_id' => $row->posts_id);
            $post = new \Models\Admin\Posts();
            $post->update_post($postdata, $where);
        }
    }

    public function recovery()
    {
        // echo "Hello World";
        if (count($_GET) == 1 && empty($_POST)) {
            $npost = new \Models\Admin\Config();
            $data['config'] = $npost->getConfig()[0];
            $socials = new \Models\Admin\Social();
            $data['socials'] = $socials->getSocial();
            $data['title'] = 'Solicitar cambio de Contraseña';
            // $socials = new \Models\Admin\Social();
            // $data['socials'] = $socials->getSocial();
            $data['showm'] = false;
            view::renderTemplate('header', $data);
            view::render('default/forgetpwd', $data);
        }
    }

    public function registerRecover()
    {
        header('Content-Type: application/json; charset=utf-8');
        if (count($_POST) >= 1) 
        {
            $context = array();
            if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) 
            {
                $mail = $_POST['email'];
                $csrf = \Helpers\Csrf::makeToken();
                $postdata = array(
                    'email' => $mail,
                    'csrf' => $csrf);
                $rec = new \Models\Admin\Recovery();
                if ($rec->getStatus($mail)->count > 0) {
                    $recu = new \Models\Admin\Recovery();
                    $pdata = array('flag' => 0);
                    $where = array('email' => $mail);
                    $recu->update_Recovery($pdata, $where);
                }
                $rec = new \Models\Admin\Recovery();
                $rec->insert_Recovery($postdata);
                echo json_encode(array('status' => true, 'csrf' => $csrf, 'mail' => $mail, 'count' => $rec->getStatus($mail)->count));
            }
            else
            {
                echo json_encode(array('status' => false, 'raise' => 'email witout format.'));
            }
        }
        else
        {
            echo json_encode(array('status' => false, 'raise' => 'method not support.', 'post' => $_POST, 'get' => $_GET));
        }
    }

    public function RestorePasswd($csrf, $mail)
    {
        $npost = new \Models\Admin\Config();
        $context['config'] = $npost->getConfig()[0];
        $socials = new \Models\Admin\Social();
        $context['socials'] = $socials->getSocial();
        $context['title'] = 'Solicitar cambio de Contraseña';
        $context['showm'] = false;
        // $context = array();
        $val = new \Models\Admin\Recovery();
        view::renderTemplate('header', $context);
        //print_r($val->verifyRecovery($mail, $csrf));
        //echo $val->verifyRecovery($mail, $csrf)->count;
        if ($val->verifyRecovery($mail, $csrf)->count > 0) {
            $context['csrfval'] = true;
            $context['csrf'] = $csrf;
            $context['mail'] = $mail;
        }else{
            $context['csrfval'] = false;
        }
        view::render('default/changepasswd', $context);
    }

    public function ChangePasswd()
    {
        header('Content-Type: application/json; charset=utf-8');
        if (count($_POST) > 0)
        {
            $val = new \Models\Admin\Recovery();
            $mail = $_POST['mail'];
            $csrf = $_POST['csrf'];
            if ($val->verifyRecovery($mail, $csrf)->count > 0) {
                $pass = Password::make($_POST['passwd']);
                $postdata = array('password' => $pass);
                $where = array('email' => $mail);
                $uuser = new \Models\Admin\Users();
                $uuser->update_user($postdata, $where);
                // change status id csrf used
                $recu = new \Models\Admin\Recovery();
                $pdata = array('flag' => 0);
                $where = array('email' => $mail, 'csrf' => $csrf);
                $recu->update_Recovery($pdata, $where);
                echo json_encode(array('status' => true));
            }
            else
            {
                echo json_encode(array('status' => false, 'raise' => 'Codigo CSRF invalido'));
            }
        }
        // echo json_encode(array('status' => false, 'post' => $_POST));
    }
}

?>
