var restore, uri;

uri = document.getElementsByName("uri")[0].value;

require.config({
  baseUrl: uri + "app/public/",
  removeCombined: true,
  paths: {
    "jquery": "vendor/jquery/dist/jquery.min",
    "formvalidate": "vendor/jquery-form-validator/form-validator/jquery.form-validator.min",
    "swal": "vendor/sweetalert/dist/sweetalert.min",
    "users": "js/modules/users"
  },
  shim: {
    formvalidate: {
      deps: ['jquery']
    },
    users: ['jquery', 'swal']
  }
});

requirejs(['jquery', 'formvalidate', 'users'], function($) {
  $(function() {
    $.validate({
      modules: 'security',
      form: "#formrecover",
      onSuccess: function() {
        restore();
        return false;
      }
    });
  });
});

restore = function(event) {
  require(['users'], function(Users) {
    var user;
    user = new Users({
      email: $("[name=email]").val(),
      password: $("[name=passwd]").val(),
      csrf: $("[name=csrf]").val()
    });
    user.restorePwd();
  });
};
