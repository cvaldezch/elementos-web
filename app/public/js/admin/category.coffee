app = angular.module "appCategory", []

app.controller "categoryController", [ "$scope", "$http", ($scope, $http) ->
    $base = document.getElementById("urlBase").value
    $scope.params = 
        list: true
    $scope.listCategory = ->
        $http.get "#{$base}admin/categories", params: $scope.params
            .success (response) ->
                $scope.categories = response
                $scope.params = new Object
                return response
            .error (error) ->
                console.error error
                return
        return
    # show add category
    $scope.showAddCategory = ->
        $("#addCategory").modal "show"
        return
    # Request add category
    $scope.addCategory = (category)->
        category.addTag = true
        $scope.params = category
        $.post "#{$base}admin/categories/add", $scope.params, (response) ->
            if response.status
                #$scope.categories = category
                $("#addCategory").modal "hide"
                location.reload()
                return
            else
                console.error "response.raise"
                return
        , "json"
        return

    $scope.showEditCategory = (id, category)->
        $("#editCategory").modal "show"
        $("[name=ecategory]").val category
        $("[name=ecategoryid]").val id
        return
    $scope.editCategory = ->
        $scope.params = 
            editTag: true
            category: $("[name=ecategory]").val()
            category_id: $("[name=ecategoryid]").val()
        $.post "#{$base}admin/categories/edit", $scope.params, (response) ->
            if response.status
                $("#editCategory").modal "hide"
                location.reload()
                return
            else
                console.error "response.raise"
                return
        , "json"
        return
    $scope.showDel = (id) ->
        $("#delCategory").modal "show"
        $("[name=dcategoryid]").val id
        return
    $scope.delCategory= ->
        $scope.params = 
            delTag: true
            category_id: $("[name=dcategoryid]").val()
        $.post "#{$base}admin/categories/del", $scope.params, (response) ->
            if response.status
                $("#editCategory").modal "hide"
                location.reload()
                return
            else
                console.error "response.raise"
                return
        , "json"
        return
    # Request list category on load
    $scope.listCategory()
    return
    ]