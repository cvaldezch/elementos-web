google.charts.load('43', {'packages':['corechart', 'table']})
app = angular.module 'postApp', ['ngSanitize']

app.controller 'postCtrl', ($scope, $http, $timeout) ->
  $scope.months = ['Enero','Febrero','Marzo','Abril', 'Mayo','Junio', 'Julio', 'Agosto', 'Setiembre','Octubre', 'Noviembre', 'Diciembre']
  $scope.uri = ''
  $scope.pinit = 1
  $scope.lposts = [1]
  $scope.cm = $scope.lposts.length
  $scope.range = new Array(0)
  angular.element(document).ready ->
    console.log "document init"
    $scope.listPosts $scope.pinit
    $scope.getYears()
    return

  $scope.listPosts = (val) ->
    $scope.pinit = val
    data =
      listPost: true
      p: val
    $http.get "#{$('#urlBase').val()}admin/posts/services", params: data
      .success (response) ->
        if response.status
          $scope.listposts = response.posts
          if response.page_links isnt ''
            $scope.tpage = Math.ceil(response.total / parseFloat response.pbp)
            # $(".clinks").find("a").each (index, element) ->
          return
        else
          console.log "Error get data #{response.raise}"
          return
    return

  $scope.listPostsTitle = (val) ->
    # $scope.pinit = val
    data =
      listTitle: true
      title: val
    $http.get "#{$('#urlBase').val()}admin/posts/services", params: data
      .success (response) ->
        if response.status
          $scope.listposts = response.posts
          return
        else
          console.log "Error get data #{response.raise}"
          return
    return

  $scope.changeSlider = ($event) ->
    data =
      changeSlider: true
      post_id: $event.srcElement.value
      status: $event.srcElement.checked
    $http.get "#{$('#urlBase').val()}admin/posts/services", params: data
      .success (response) ->
        if response.status
          swal
            title: "Correcto!"
            text: ""
            type: "success"
            showConfirmButton: false
            timer: 1800
          return
        else
          swal "Error.", "No se pudo realizar la transacción", "error"
          return
    return
  
  $scope.getPDay = ->
    $http.get "", data, (response) ->
      if response.status

      else
        console.log "Nothing result for this request"
    return

  $scope.getPWeek = ->
    $http.get "", data, (response) ->
      if response.status
        
      else
        console.log "Nothing result for this request"
    return

  $scope.getPMonth = ->
    $http.get "", data, (response) ->
      if response.status
        
      else
        console.log "Nothing result for this request"
    return

  $scope.getPYear = ->
    $http.get "", data, (response) ->
      if response.status
        
      else
        console.log "Nothing result for this request"
    return
  
  $scope.openwindow = ->
    window.open "#{$('#urlBase').val()}#{this.x.posts_id}/#{this.x.title.replace(' ','_')}", ""
    console.log "#{$('#urlBase').val()}/#{this.x.posts_id}/#{this.x.title.replace(' ','_')}"
    return

  $scope.changelpost = ->
    console.log this
    if $scope.lposts.length is 0
      $scope.listPostsTitle $scope.search
    return

  $scope.getYears = ->
    $http.get "#{location.origin}/admin/posts/services", params: 'years': true
    .success (response) ->
      if response.status
        $scope.years = response.year
        return
      else
        console.log "error"
        return
    return

  $scope.getMonths = ->
    data =
      year: $scope.slyear
      months: true
    $http.get "#{location.origin}/admin/posts/services", params: data
    .success (response) ->
      if response.status
        $scope.monthbyy = response.month
        return
      else
        console.log "No hay meses para este anio."
        return
    return

  $scope.drawByAnioAreaChart = ->
    # Set a callback to run when the Google Visualization API is loaded.
    # google.charts.setOnLoadCallback(drawc)
    google.charts.setOnLoadCallback ->
      $http.get "#{location.origin}/admin/posts/services", params: 'rptYear': true
      .success (response) ->
        console.log response
        if response.status
          arr = new Array()
          arr.push new Array('Year', 'Visitados')
          for k, v of response.year
            arr.push new Array v.year, parseInt v.visited
          data = google.visualization.arrayToDataTable arr
          options = {
            title: 'Publicaciones mas Visitadas por Año',
            hAxis: {title: 'Año',  titleTextStyle: {color: '#333'}},
            vAxis: {minValue: 0}
          }
          chart = new google.visualization.AreaChart(document.getElementById('byanio_area'))
          chart.draw(data, options)

          dt = new google.visualization.DataTable()
          dt.addColumn 'string', 'Titulo'
          dt.addColumn 'string', 'Año'
          dt.addColumn 'number', 'Vistas'
          arr = new Array()
          for k, v of response.year
            arr.push new Array v.title, v.year, parseInt v.visited
          
          dt.addRows arr
          table = new google.visualization.Table document.getElementById "byanio_table"
          table.draw dt,
            showRowNumber: true
            width: '100%'
            height: '100%'
          return
        else
          console.log "error http"
          return
      return
    return

  $scope.drawByMonthAreaChart = ->
    google.charts.setOnLoadCallback ->
      data =
        'monthbyyear': true
        'year': $scope.mysel
      $http.get "#{location.origin}/admin/posts/services", params: data
      .success (response) ->
        if response.status
          month = new Array 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Juilo', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'
          arr = new Array()
          arr.push new Array 'Mes', 'Visto'
          for k, v of response.month
            arr.push new Array month[parseInt(v.month) - 1], parseInt v.visited
          dt = google.visualization.arrayToDataTable(arr)
          options = {
            title: "Publicaciones mas Visitadas por Meses #{$scope.mysel}",
            hAxis: {title: 'Meses',  titleTextStyle: {color: '#333'}},
            vAxis: {minValue: 0}
          }
          chart = new google.visualization.AreaChart(document.getElementById('bymonth_area'))
          chart.draw(dt, options)
          dt = new google.visualization.DataTable()
          dt.addColumn 'string', 'Title'
          dt.addColumn 'string', 'Mes'
          dt.addColumn 'number', 'Visto'
          arr = new Array()
          for k, v of response.month
            arr.push new Array v.title, month[parseInt(v.month) - 1], parseInt v.visited
          dt.addRows arr
          table = new google.visualization.Table document.getElementById 'bymonth_table'
          table.draw dt,
            showRowNumber: true
            width: '100%'
            height: '100%'
          return
      return
    return

  $scope.getDataYears = ->
    data =
      year: $scope.svyear
      postbyyear: true
    $http.get "#{location.origin}/admin/posts/services", params: data
    .success (response) ->
      if response.status
        $scope.vtyear = response.pyear
        return
      else
        console.log "no hay datos para mostrar"
        return
    return

  $scope.getDataMonths = ->
    data =
      year: $scope.slyear
      month: $scope.vmonth
      postbymonth: true
    $http.get "#{location.origin}/admin/posts/services", params: data
    .success (response) ->
      if response.status
        $scope.vtmonth = response.pmonth
        return
      else
        console.log "No hay datos registrados para esta consulta"
        return
    return

  $scope.deletePost = ->
    prm =
      deletePost: true
      post: new Array
    chk = document.getElementsByName "postdel"
    angular.forEach angular.element(chk), (value, key) ->
      el = angular.element(value)
      if el[0].checked
        prm.post.push el.val()
        return
    $timeout ->
      # console.info prm.post.length
      if prm.post.length
        swal
          title: "Realmente desea eliminar la(s) Publicacion(es)?"
          text: ""
          type: "warning"
          showCancelButton: true
          confirmButtonColor: '#3085d6'
          cancelButtonColor: '#d33'
          confirmButtonText: 'Si, Eliminar!'
          cancelButtonText: 'No!'
          confirmButtonClass: 'confirm-class'
          cancelButtonClass: 'cancel-class'
        , (isConfirm) ->
          if isConfirm
            $http.post "#{location.origin}/admin/posts/services", params: prm
            .success (response) ->
              if response.status
                $scope.listPosts $scope.pinit
                return
              else
                swal "Error", "No se han encontrado datos.", "error"
                return
            return
        return
    , 800
    return

  $scope.$watch 'cm', ->
    if $scope.cm is 0
      $scope.listPostsTitle $scope.search
      return
  return
