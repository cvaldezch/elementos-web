var app;

app = angular.module("MenuApp", []);

app.controller("MenuCtrl", function($scope, $http) {
  var uri;
  uri = document.getElementById("uri").value;
  $scope.params = {};
  $scope.menuList = [];
  $scope.paneltitle = 'Agregar Submenu';
  $scope.loadSubmenu = function() {
    $scope.params = {
      list: true,
      menu: $scope.menuid
    };
    $http.get(uri + "wservices/submenu", {
      params: $scope.params
    }).success(function(response) {
      $scope.params = {};
      $scope.menuList = response.menus;
    }).error(function(error) {
      console.log(error);
    });
  };
  $scope.showEdit = function(menu, id, submenu, url, pos) {
    console.log(id, menu, pos);
    $scope.paneltitle = 'Editar Menu';
    $scope.menuid = menu;
    $scope.menu = {
      submenuid: id,
      submenu: submenu,
      url: url,
      position: pos
    };
    $scope.btnadd = true;
  };
  $scope.$watch('btnadd', function(nval) {
    console.log(nval, 'change value');
    if (nval === false) {
      $scope.menu = new Object;
      $scope.paneltitle = 'Agregar Submenu';
      console.log($scope.menuid, "menu id");
    }
  });
  $scope.saveSubmenu = function() {
    var context;
    context = {
      menuid: $scope.menuid,
      submenuid: $scope.menu.submenuid,
      submenu: $scope.menu.submenu,
      url: $scope.menu.url,
      position: $scope.menu.position
    };
    if (context.submenuid === void 0 || context.submenuid === "") {
      context.save = 'new';
      delete context['submenuid'];
    } else {
      context.save = 'edit';
    }
    console.log(context, "context send");
    $http({
      withCredentials: false,
      method: 'post',
      url: uri + "wservices/submenu",
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      data: context
    }).success(function(response) {
      $scope.loadSubmenu();
      $scope.btnadd = false;
    });
  };
  $scope.deleteMenu = function(menu, id) {
    if (id === void 0 || id === "" && menu === void 0 || menu === "") {
      swal("Oops!", "El código no es valido.", "warning");
      return false;
    }
    swal({
      title: "Desea eliminar el Submenu?",
      text: "Realmente desea eliminar submenu, debe de tener encuenta que se borrar permanentemente y todo su contenido!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Si!",
      cancelButtonText: "No!",
      closeOnConfirm: false,
      closeOnCancel: true
    }, function(isConfirm) {
      var context;
      if (isConfirm) {
        context = {
          menuid: menu,
          submenuid: id,
          "delete": true
        };
        $http({
          withCredentials: true,
          method: 'post',
          url: uri + "wservices/submenu",
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: context
        }).success(function(response) {
          if (response.status) {
            $scope.loadSubmenu();
            return swal("Felicidades!", "se a eliminado correctamente.", "success");
          }
        });
      }
    });
  };
});
