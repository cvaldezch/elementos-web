var app;

app = angular.module("MenuApp", []);

app.controller("MenuCtrl", function($scope, $http) {
  var load, uri;
  uri = document.getElementById("uri").value;
  $scope.params = {};
  $scope.menuList = [];
  $scope.paneltitle = 'Agregar Menú';
  load = function() {
    $scope.params = {
      list: true
    };
    $http.get(uri + "wservices/menu", {
      params: $scope.params
    }).success(function(response) {
      $scope.params = {};
      $scope.menuList = response.menus;
    }).error(function(error) {
      console.log(error);
    });
  };
  load();
  $scope.showEdit = function(id, menu, url, pos) {
    console.log(id, menu, pos);
    $scope.paneltitle = 'Editar Menu';
    $scope.menu = {
      menuid: id,
      menu: menu,
      url: url,
      position: pos
    };
    $scope.btnadd = true;
  };
  $scope.$watch('btnadd', function(nval) {
    console.log(nval, 'change value');
    if (nval === false) {
      $scope.menu = new Object;
      $scope.paneltitle = 'Agregar Menú';
    }
  });
  $scope.saveMenu = function() {
    var context;
    context = {
      menuid: $scope.menu.menuid,
      menu: $scope.menu.menu,
      url: $scope.menu.url,
      position: $scope.menu.position
    };
    if (context.menuid === void 0 || context.menuid === "") {
      context.save = 'new';
      delete context['menuid'];
    } else {
      context.save = 'edit';
    }
    console.log(context, "context send");
    $http({
      withCredentials: false,
      method: 'post',
      url: uri + "wservices/menu",
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      data: context
    }).success(function(response) {
      load();
      $scope.btnadd = false;
    });
  };
  $scope.deleteMenu = function(id) {
    if (id === void 0 || id === "") {
      swal("Oops!", "El código no es valido.", "warning");
      return false;
    }
    swal({
      title: "Desea eliminar el Menú?",
      text: "Realmente desea eliminar menú, debe de tener encuenta que se borrar permanentemente y todo su contenido!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Si!",
      cancelButtonText: "No!",
      closeOnConfirm: false,
      closeOnCancel: true
    }, function(isConfirm) {
      var context;
      if (isConfirm) {
        context = {
          menuid: id,
          "delete": true
        };
        $http({
          withCredentials: true,
          method: 'post',
          url: uri + "wservices/menu",
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: context
        }).success(function(response) {
          if (response.status) {
            load();
            return swal("Felicidades!", "se a eliminado correctamente.", "success");
          }
        });
      }
    });
  };
});
