var app;

app = angular.module("RequestApp", []);

app.controller("ReqCtrl", function($scope, $http) {
  var uri;
  uri = document.getElementById("uri").value;
  $scope.listRequest = [];
  $scope.user = {};
  $scope.loadRequest = function() {
    var context;
    context = {
      'list': true
    };
    $http.get(uri + "wservices/request", {
      params: context
    }).success(function(response) {
      if (response.status) {
        $scope.listRequest = response.users;
      } else {
        swal("Oops!", "Error al traer datos, " + response.raise, "error");
      }
    });
  };
  $scope.loadRequest();
  $scope.showDetails = function(user, name, email, mid, smid, menu, submenu, comment) {
    console.log(" here show");
    console.log(name, email);
    $scope.user = {
      username: user,
      name: name,
      email: email,
      menuid: mid,
      menu: menu,
      submenuid: smid,
      submenu: submenu,
      comment: comment
    };
    $scope.btnshow = true;
  };
  $scope.$watch('btnshow', function(nval) {
    console.log(nval);
    if (!nval) {
      $scope.user = {};
    }
  });
  $scope.savePublisher = function() {
    var context;
    context = $scope.user;
    if (context.email === void 0 || context.email === "") {
      swal("Alerta!", "Parametros invalidos", "warning");
      return false;
    }
    if (context.publisher === "false") {
      context.publisher = false;
      context.role = "R00";
    } else {
      context.role = context.publisher;
      context.publisher = true;
    }
    context.savepublisher = true;
    console.log(context);
    $http.post(uri + "wservices/request", {
      params: context
    }).success(function(response) {
      if (response.status) {
        $scope.loadRequest();
        $scope.btnshow = false;
      } else {
        swal("Opps!", "Error al procesar la solicitud. " + response.raise, "error");
      }
    }).error(function(error) {
      swal("Opps!", "El servidor se ha quedado dormido. " + error, "error");
    });
  };
});
