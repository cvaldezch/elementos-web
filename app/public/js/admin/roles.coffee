require.config
  baseUrl: "../app/public/"
  paths:
    angular: 'vendor/angularjs/angular.min'
    jquery: 'vendor/jquery/dist/jquery.min'
    bootstrap: 'vendor/bootstrap/dist/js/bootstrap.min'
    swal: 'vendor/sweetalert/dist/sweetalert.min'
  shim:
    angular:
      exports: 'angular'
    bootstrap:
      deps: ['jquery']

requirejs ['angular', 'bootstrap', 'swal'], (angular) ->
  app = angular.module "RoleApp", []
  app.controller "RoleCtrl", ($scope, $http) ->
    uri = document.getElementById("uri").value
    # console.log "load Controller"
    # $scope.subtitle = "Hello World!"
    $scope.params = {}
    $scope.rolesList = []
    $scope.paneltitle = 'Agregar Rol'
    # Load list menus
    $scope.loadRoles = ->
      $scope.params =
        list: true
        menu: $scope.menuid
      $http.get "#{uri}wservices/roles", params: $scope.params
        .success (response) ->
          $scope.params = {}
          $scope.rolesList = response.roles
          return
        .error (error) ->
          console.log error
          return
        return
    $scope.loadRoles()
    # show menu edit
    $scope.showEdit = (id, role) ->
      console.log id, role
      $scope.paneltitle = 'Editar Rol'
      $scope.role =
        rolesid: id
        role: role
      $scope.btnadd = true
      return
    # watch change status btnadd for show or hide panel add or list
    $scope.$watch 'btnadd', (nval) ->
      if nval is false
        console.log $scope.role
        $scope.role = new Object
        $scope.paneltitle = 'Agregar Rol'
      return
    # method for save edit or new menu
    $scope.saveRole = ->
      context =
        roleid: $scope.role.rolesid
        role: $scope.role.role
      if context.roleid is undefined or context.roleid is ""
        context.save = 'new'
        delete context['roleid']
      else
        context.save = 'edit'
      console.log context, "context send"
      $http
        withCredentials: false
        method: 'post'
        url: "#{uri}wservices/roles"
        headers:
          'Content-Type': 'application/x-www-form-urlencoded'
        data: context
      .success (response) ->
        $scope.loadRoles()
        # $scope.menu.clean()
        $scope.btnadd = false
        return
      return
    $scope.deleteRole = (id) ->
      if id is undefined or id is ""
        swal "Oops!", "El código no es valido.", "warning"
        return false
      swal
        title: "Desea eliminar el Rol?"
        text: "Realmente desea eliminar rol, debe de tener encuenta que se borrar permanentemente y todo su contenido!"
        type: "warning"
        showCancelButton: true
        confirmButtonColor: "#DD6B55"
        confirmButtonText: "Si!"
        cancelButtonText: "No!"
        closeOnConfirm: false
        closeOnCancel: true
      , (isConfirm) ->
        if (isConfirm)
          context =
            rolesid: id
            delete: true
          console.log context
          $http
            withCredentials: true
            method: 'post'
            url: "#{uri}wservices/roles"
            headers:
              'Content-Type': 'application/x-www-form-urlencoded'
            data: context
          .success (response) ->
            if response.status
              $scope.loadRoles()
              swal "Felicidades!", "se a eliminado correctamente.", "success"
          return
      return
    return
  return