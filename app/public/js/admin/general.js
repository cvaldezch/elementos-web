(function() {
  var confCtrl;
  angular.module('confApp', []);
  confCtrl = function($scope, $http) {
    var uri;
    uri = document.getElementById("uri").value;
    $scope.conf = {
      nsliders: 0,
      admin: ''
    };
    $scope.listRoles = [];
    $scope.loadConf = function() {
      $http.get(uri + "wservices/conf", {
        params: {
          list: true
        }
      }).success(function(response) {
        if (response.status) {
          $scope.conf.nsliders = response.config[0].nsliders;
          $scope.conf.admin = response.config[0].admin;
          $scope.conf.moderator = response.config[0].moderator;
          $scope.conf.editor = response.config[0].editor;
          $scope.conf.npost = response.config[0].npost;
        } else {
          swal("Oops!", "No se a recuperado los datos, " + response.raise);
        }
      });
    };
    $scope.loadConf();
    $scope.loadRoles = function() {
      $http.get(uri + "wservices/roles", {
        params: {
          list: true
        }
      }).success(function(response) {
        if (response.status) {
          $scope.listRoles = response.roles;
        } else {
          swal("Oops!", "No se a recuperado los datos.", "error");
        }
      });
    };
    $scope.loadRoles();
    $scope.saveSlider = function() {
      var context;
      context = {
        save: 'slider',
        nsliders: $scope.conf.nsliders
      };
      $http.post(uri + "wservices/conf", {
        params: context
      }).success(function(response) {
        if (response.status) {
          swal({
            title: "Felicidades!",
            text: "Se guardo correctamente.",
            type: "success",
            showConfirmButton: false,
            timer: 2000
          });
        } else {
          swal("Error!", "al guardar los datos", "error");
        }
      });
    };
    $scope.saveRoles = function() {
      var context;
      context = {
        save: 'roles',
        admin: $scope.conf.admin,
        moderator: $scope.conf.moderator,
        editor: $scope.conf.editor
      };
      $http.post(uri + "wservices/conf", {
        params: context
      }).success(function(response) {
        if (response.status) {
          swal({
            title: "Felicidades!",
            text: "Se guardo correctamente.",
            type: "success",
            showConfirmButton: false,
            timer: 2000
          });
        } else {
          swal("Error!", "al guardar los datos. " + response.raise, "error");
        }
      });
    };
    $scope.saveNPost = function() {
      var context;
      context = {
        save: 'npost',
        npost: $scope.conf.npost
      };
      $http.post(uri + "wservices/conf", {
        params: context
      }).success(function(response) {
        if (response.status) {
          swal({
            title: "Felicidades!",
            text: "Se guardo correctamente.",
            type: "success",
            showConfirmButton: false,
            timer: 2000
          });
        } else {
          swal("Error!", "al guardar los datos. " + response.raise, "error");
        }
      });
    };
  };
  angular.module('confApp').controller('confCtrl', confCtrl);
})();
