var app;

app = angular.module("appCategory", []);

app.controller("categoryController", [
  "$scope", "$http", function($scope, $http) {
    var $base;
    $base = document.getElementById("urlBase").value;
    $scope.params = {
      list: true
    };
    $scope.listCategory = function() {
      $http.get($base + "admin/categories", {
        params: $scope.params
      }).success(function(response) {
        $scope.categories = response;
        $scope.params = new Object;
        return response;
      }).error(function(error) {
        console.error(error);
      });
    };
    $scope.showAddCategory = function() {
      $("#addCategory").modal("show");
    };
    $scope.addCategory = function(category) {
      category.addTag = true;
      $scope.params = category;
      $.post($base + "admin/categories/add", $scope.params, function(response) {
        if (response.status) {
          $("#addCategory").modal("hide");
          location.reload();
        } else {
          console.error("response.raise");
        }
      }, "json");
    };
    $scope.showEditCategory = function(id, category) {
      $("#editCategory").modal("show");
      $("[name=ecategory]").val(category);
      $("[name=ecategoryid]").val(id);
    };
    $scope.editCategory = function() {
      $scope.params = {
        editTag: true,
        category: $("[name=ecategory]").val(),
        category_id: $("[name=ecategoryid]").val()
      };
      $.post($base + "admin/categories/edit", $scope.params, function(response) {
        if (response.status) {
          $("#editCategory").modal("hide");
          location.reload();
        } else {
          console.error("response.raise");
        }
      }, "json");
    };
    $scope.showDel = function(id) {
      $("#delCategory").modal("show");
      $("[name=dcategoryid]").val(id);
    };
    $scope.delCategory = function() {
      $scope.params = {
        delTag: true,
        category_id: $("[name=dcategoryid]").val()
      };
      $.post($base + "admin/categories/del", $scope.params, function(response) {
        if (response.status) {
          $("#editCategory").modal("hide");
          location.reload();
        } else {
          console.error("response.raise");
        }
      }, "json");
    };
    $scope.listCategory();
  }
]);
