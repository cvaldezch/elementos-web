require.config({
  baseUrl: "../app/public/",
  paths: {
    angular: 'vendor/angularjs/angular.min',
    jquery: 'vendor/jquery/dist/jquery.min',
    bootstrap: 'vendor/bootstrap/dist/js/bootstrap.min',
    swal: 'vendor/sweetalert/dist/sweetalert.min'
  },
  shim: {
    angular: {
      exports: 'angular'
    },
    bootstrap: {
      deps: ['jquery']
    }
  }
});

requirejs(['angular', 'bootstrap', 'swal'], function(angular) {
  var app;
  app = angular.module("RoleApp", []);
  app.controller("RoleCtrl", function($scope, $http) {
    var uri;
    uri = document.getElementById("uri").value;
    $scope.params = {};
    $scope.rolesList = [];
    $scope.paneltitle = 'Agregar Rol';
    $scope.loadRoles = function() {
      $scope.params = {
        list: true,
        menu: $scope.menuid
      };
      $http.get(uri + "wservices/roles", {
        params: $scope.params
      }).success(function(response) {
        $scope.params = {};
        $scope.rolesList = response.roles;
      }).error(function(error) {
        console.log(error);
      });
    };
    $scope.loadRoles();
    $scope.showEdit = function(id, role) {
      console.log(id, role);
      $scope.paneltitle = 'Editar Rol';
      $scope.role = {
        rolesid: id,
        role: role
      };
      $scope.btnadd = true;
    };
    $scope.$watch('btnadd', function(nval) {
      if (nval === false) {
        console.log($scope.role);
        $scope.role = new Object;
        $scope.paneltitle = 'Agregar Rol';
      }
    });
    $scope.saveRole = function() {
      var context;
      context = {
        roleid: $scope.role.rolesid,
        role: $scope.role.role
      };
      if (context.roleid === void 0 || context.roleid === "") {
        context.save = 'new';
        delete context['roleid'];
      } else {
        context.save = 'edit';
      }
      console.log(context, "context send");
      $http({
        withCredentials: false,
        method: 'post',
        url: uri + "wservices/roles",
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: context
      }).success(function(response) {
        $scope.loadRoles();
        $scope.btnadd = false;
      });
    };
    $scope.deleteRole = function(id) {
      if (id === void 0 || id === "") {
        swal("Oops!", "El código no es valido.", "warning");
        return false;
      }
      swal({
        title: "Desea eliminar el Rol?",
        text: "Realmente desea eliminar rol, debe de tener encuenta que se borrar permanentemente y todo su contenido!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Si!",
        cancelButtonText: "No!",
        closeOnConfirm: false,
        closeOnCancel: true
      }, function(isConfirm) {
        var context;
        if (isConfirm) {
          context = {
            rolesid: id,
            "delete": true
          };
          console.log(context);
          $http({
            withCredentials: true,
            method: 'post',
            url: uri + "wservices/roles",
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: context
          }).success(function(response) {
            if (response.status) {
              $scope.loadRoles();
              return swal("Felicidades!", "se a eliminado correctamente.", "success");
            }
          });
        }
      });
    };
  });
});
