( ->
  angular.module "usersApp", []
  userCtrl = ($scope, $http, $timeout) ->
    uri = document.getElementById("uri").value
    $scope.usersList = []
    $scope.titletable = "Lista de Usuarios"
    $scope.spublish = ''
    $scope.ssection = ''
    $scope.userid = ''
    $scope.mail = ''
    $scope.loadUsers = ->
      $http.get "#{uri}wservices/users", params: list: true
      .success (response) ->
        if response.status
          $scope.usersList = response.users
          return
        else
          swal "Alerta!", "Error al recuperar los datos", "warning"
          return
      return
    $scope.loadRoles = ->
      $http.get "#{uri}wservices/roles", params: list: true
      .success (response) ->
        if response.status
          $scope.rolesList = response.roles
        else
          swal "Alerta!", "Error al recuperar roles", "warning"
          return
      return
    $scope.loadMenu = ->
      $http.get "#{location.origin}/wservices/menu", params: list: true
      .success (response) ->
        if response.status
          $scope.lsgroup = response.menus
          return
        else
          swal "Alerta!", "Error al recuperar menu", "warning"
          return
      return
    $scope.loadSubmenus = ->
      data =
        list: true
        menu: $scope.sgroup
      $http.get "#{uri}wservices/submenu", params: data
      .success (response) ->
        if response.status
          $scope.lsection = response.menus
          return
        else
          swal "Alerta!", "Error al recuperar submenu", "warning"
          return
      return
    $scope.conAdmin = (id, email) ->
      context =
        usersid: id
        email: email
        saveAdmin: true
      if typeof(email) is "undefined" and email.indexof("@") is -1
        swal "Alerta!", "Parametro invalido.", "warning"
        return false
      $http.post "#{uri}wservices/users", params: context
      .success (response) ->
        if response.status
          $scope.loadUsers()
          swal "Felicidades!", "Se convirtio en Administrador.", "success"
          return
        else
          swal "Error!", "al convetir en admin. #{response.raise}", "error"
          return
      return
    $scope.conUsers = (id, email) ->
      context =
        usersid: id
        email: email
        saveUsers: true
      if typeof(email) is "undefined" and email.indexof("@") is -1
        swal "Alerta!", "Parametro invalido.", "warning"
        return false
      $http.post "#{uri}wservices/users", params: context
      .success (response) ->
        if response.status
          $scope.loadUsers()
          swal "Felicidades!", "se a convertido en usuario a la cuenta #{email}.", "success"
          return
        else
          swal "Error!", "al convetir a usuario", "warning"
          return
      return
    $scope.Desactive = (id, email) ->
      context =
        usersid: id
        email: email
        deactive: true
      if typeof(email) is "undefined" and email.indexof("@") is -1
        swal "Alerta!", "Parametro invalido.", "warning"
        return false
      swal
        title: "Desactivar Cuenta!"
        text: "Realmente desea desactivar la cuenta #{email}?"
        type: 'warning'
        showCancelButton: true
        confirmButtonColor: '#3085d6'
        cancelButtonColor: '#d33'
        confirmButtonText: 'Si, Desactivar!'
        cancelButtonText: 'No, cancelar!'
        confirmButtonClass: 'confirm-class'
        cancelButtonClass: 'cancel-class'
        # closeOnConfirm: false
        # closeOnCancel: true
      , (isConfirm) ->
        if isConfirm
          console.log "account deactive"
          $http.post "#{uri}wservices/users", params: context
          .success (response) ->
            if response.status
              swal "Felicidades", "Se ha desactivado la cuenta", "success"
              console.log response
              return
            else
              swal "Error", "no se a desactivado la cuenta #{email}. #{response.raise}", "error"
              return
          return
      return
    $scope.showRoles = ->
      $scope.gshow = true
      console.log this
      $scope.spublish = this.x.publish
      $scope.ssection = this.x.section
      $scope.sgroup = $scope.spublish
      $scope.userid = this.x.users_id
      $scope.mail = this.x.email
      $timeout ->
        $scope.loadSubmenus()
      , 600
      return
    $scope.saveEditor = ->
      if $scope.section is ""
        swal "Seleccione una Sección!", "debe de seleccionar una sección para el editor.", "warning"
        return false
      data =
        publish: $scope.sgroup
        section: $scope.section
        uid: $scope.userid
        mail: $scope.mail
        saveEditor: true
      swal
        title: "Desea asignar permisos de editor para #{data.mail}"
        text: ""
        type: "warning"
        showCancelButton: true
        confirmButtonColor: '#3085d6'
        cancelButtonColor: '#d33'
        confirmButtonText: 'Si!'
        cancelButtonText: 'No!'
        confirmButtonClass: 'confirm-class'
        cancelButtonClass: 'cancel-class'
      , (isConfirm) ->
        if isConfirm
          $http.post "#{uri}wservices/users", params: data
          .success (response) ->
            if response.status
              $scope.loadUsers()
              swal "Felicidades!", "#{data.mail} se ha hecho EDITOR!", "success"
              $timeout ->
                $scope.gshow = false 
                return
              , 2600
              return
            else
              swal "Error!", "al convertir al usuario en editor, #{response.raise}", "error"
              return
          return
      return
    $scope.$watch 'gshow', ->
      if $scope.gshow
        $scope.loadMenu()
        return
    $scope.$watch 'sgroup', (old, nw) ->
      console.log old, "old"
      console.log nw, "new"
      return
    
    $scope.loadUsers()
    return
  angular.module "usersApp"
  .controller "userCtrl",  userCtrl
  return
)()