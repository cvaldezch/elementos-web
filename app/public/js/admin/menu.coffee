# require.config
#   baseUrl: "../app/public/"
#   paths:
#     angular: 'vendor/angularjs/angular.min'
#     jquery: 'vendor/jquery/dist/jquery.min'
#     bootstrap: 'vendor/bootstrap/dist/js/bootstrap.min'
#     swal: 'vendor/sweetalert/dist/sweetalert.min'
#   shim:
#     angular:
#       exports: 'angular'
#     bootstrap:
#       deps: ['jquery']

# requirejs ['angular', 'bootstrap', 'swal'], (angular) ->
app = angular.module "MenuApp", []

app.controller "MenuCtrl", ($scope, $http) ->
  uri = document.getElementById("uri").value
  # console.log "load Controller"
  # $scope.subtitle = "Hello World!"
  $scope.params = {}
  $scope.menuList = []
  $scope.paneltitle = 'Agregar Menú'
  # Load list menus
  load = ->
    $scope.params =
      list: true
    $http.get "#{uri}wservices/menu", params: $scope.params
      .success (response) ->
        $scope.params = {}
        $scope.menuList = response.menus
        return
      .error (error) ->
        console.log error
        return
      return
  load()
  # show menu edit
  $scope.showEdit = (id, menu, url, pos) ->
    console.log id, menu, pos
    $scope.paneltitle = 'Editar Menu'
    $scope.menu =
      menuid: id
      menu: menu
      url: url
      position: pos
    $scope.btnadd = true
    return
  # watch change status btnadd for show or hide panel add or list
  $scope.$watch 'btnadd', (nval) ->
    console.log nval, 'change value'
    if nval is false
      #console.log $scope.menu
      $scope.menu = new Object
      $scope.paneltitle = 'Agregar Menú'
    return
  # method for save edit or new menu
  $scope.saveMenu = ->
    context =
      menuid: $scope.menu.menuid
      menu: $scope.menu.menu
      url: $scope.menu.url
      position: $scope.menu.position
    if context.menuid is undefined or context.menuid is ""
      context.save = 'new'
      delete context['menuid']
    else
      context.save = 'edit'
    console.log context, "context send"
    $http
      withCredentials: false
      method: 'post'
      url: "#{uri}wservices/menu"
      headers:
        'Content-Type': 'application/x-www-form-urlencoded'
      data: context
    .success (response) ->
      load()
      # $scope.menu.clean()
      $scope.btnadd = false
      return
    return
  # clean model Menu
  # $scope.menu.clean = ->
  #   $scope.menu =
  #     menuid: ''
  #     menu: ''
  #     position: ''
  #   return
  # delete model / item Menu
  $scope.deleteMenu = (id) ->
    if id is undefined or id is ""
      swal "Oops!", "El código no es valido.", "warning"
      return false
    swal
      title: "Desea eliminar el Menú?"
      text: "Realmente desea eliminar menú, debe de tener encuenta que se borrar permanentemente y todo su contenido!"
      type: "warning"
      showCancelButton: true
      confirmButtonColor: "#DD6B55"
      confirmButtonText: "Si!"
      cancelButtonText: "No!"
      closeOnConfirm: false
      closeOnCancel: true
    , (isConfirm) ->
      if (isConfirm)
        context =
          menuid: id
          delete: true
        $http
          withCredentials: true
          method: 'post'
          url: "#{uri}wservices/menu"
          headers:
            'Content-Type': 'application/x-www-form-urlencoded'
          data: context
        .success (response) ->
          if response.status
            load()
            swal "Felicidades!", "se a eliminado correctamente.", "success"
        return
    return
  return
  # services = angular.module "MenuApp.services", []
  # services.factory "Menu", ($http) ->
  #   Menu = (data) ->
  #     `angular.extends(this, data)`
  #     return
  #   Menu.list = (menu) ->
  #     $http.get "#{uri}wservices/menu", params: menu.params
  #     .success (response) ->
  #       return response.menus
  #   return Menu
  # return
