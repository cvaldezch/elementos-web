(->
  angular.module 'confApp', []
  confCtrl = ($scope, $http) ->
    uri = document.getElementById("uri").value
    $scope.conf =
      nsliders: 0
      admin: ''
    $scope.listRoles = []
    $scope.loadConf = ->
      $http.get "#{uri}wservices/conf", params: list: true
      .success (response) ->
        if response.status
          $scope.conf.nsliders = response.config[0].nsliders
          $scope.conf.admin = response.config[0].admin
          $scope.conf.moderator = response.config[0].moderator
          $scope.conf.editor = response.config[0].editor
          $scope.conf.npost = response.config[0].npost
          return
        else
          swal "Oops!", "No se a recuperado los datos, #{response.raise}"
          return
      return
    $scope.loadConf()
    $scope.loadRoles = ->
      $http.get "#{uri}wservices/roles", params: list: true
        .success (response) ->
          if response.status
            $scope.listRoles = response.roles
            return
          else
            swal "Oops!", "No se a recuperado los datos.", "error"
            return
      return
    $scope.loadRoles()
    $scope.saveSlider = ->
      context =
        save: 'slider'
        nsliders: $scope.conf.nsliders
      $http.post "#{uri}wservices/conf", params: context
      .success (response) ->
        if response.status
          swal
            title: "Felicidades!"
            text: "Se guardo correctamente."
            type: "success"
            showConfirmButton: false
            timer: 2000
          return
        else
          swal "Error!", "al guardar los datos", "error"
          return
      return
    $scope.saveRoles = ->
      context =
        save: 'roles'
        admin: $scope.conf.admin
        moderator: $scope.conf.moderator
        editor: $scope.conf.editor
      $http.post "#{uri}wservices/conf", params: context
      .success (response) ->
        if response.status
          swal
            title: "Felicidades!"
            text: "Se guardo correctamente."
            type: "success"
            showConfirmButton: false
            timer: 2000
          return
        else
          swal "Error!", "al guardar los datos. #{response.raise}", "error"
          return
      return
    $scope.saveNPost = ->
      context =
        save: 'npost'
        npost: $scope.conf.npost
      $http.post "#{uri}wservices/conf", params: context
      .success (response) ->
        if response.status
          swal
            title: "Felicidades!"
            text: "Se guardo correctamente."
            type: "success"
            showConfirmButton: false
            timer: 2000
          return
        else
          swal "Error!", "al guardar los datos. #{response.raise}", "error"
          return
      return
    return
  angular.module 'confApp'
  .controller 'confCtrl', confCtrl
  return
)()