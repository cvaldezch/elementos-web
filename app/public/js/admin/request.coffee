# require.config
#   baseUrl: "../../app/public/"
#   paths:
#     angular: 'vendor/angularjs/angular.min'
#     jquery: 'vendor/jquery/dist/jquery.min'
#     bootstrap: 'vendor/bootstrap/dist/js/bootstrap.min'
#     swal: 'vendor/sweetalert/dist/sweetalert.min'
#   shim:
#     angular:
#       exports: 'angular'
#     bootstrap:
#       deps: ['jquery']

# requirejs ['angular', 'bootstrap', 'swal'], (angular) ->
app = angular.module "RequestApp", []
app.controller "ReqCtrl", ($scope, $http) ->
  uri = document.getElementById("uri").value
  $scope.listRequest = []
  $scope.user = {}
  $scope.loadRequest = ->
    context =
      'list': true
    $http.get "#{uri}wservices/request", params: context
      .success (response) ->
        if response.status
          $scope.listRequest = response.users
          return
        else
          swal "Oops!", "Error al traer datos, #{response.raise}", "error"
          return
    return
  $scope.loadRequest()
  $scope.showDetails = (user, name, email, mid, smid, menu, submenu, comment) ->
    console.log " here show"
    console.log name, email
    $scope.user =
      username: user
      name: name
      email: email
      menuid: mid
      menu: menu
      submenuid: smid
      submenu: submenu
      comment: comment
    $scope.btnshow = true
    return
  $scope.$watch 'btnshow', (nval) ->
    console.log nval
    if not nval
      $scope.user = {}
    return
  $scope.savePublisher = ->
    context = $scope.user
    if context.email is undefined or context.email is ""
      swal "Alerta!", "Parametros invalidos", "warning"
      return false
    if context.publisher is "false"
      context.publisher = false
      context.role = "R00"
    else
      context.role = context.publisher
      context.publisher = true
    context.savepublisher = true
    console.log context
    $http.post "#{uri}wservices/request", params: context
      .success (response) ->
        if response.status
          $scope.loadRequest()
          $scope.btnshow = false
          return
        else
          swal "Opps!", "Error al procesar la solicitud. #{response.raise}", "error"
          return
      .error (error) ->
        swal "Opps!", "El servidor se ha quedado dormido. #{error}", "error"
        return
    return
  return