(function() {
  var userCtrl;
  angular.module("usersApp", []);
  userCtrl = function($scope, $http, $timeout) {
    var uri;
    uri = document.getElementById("uri").value;
    $scope.usersList = [];
    $scope.titletable = "Lista de Usuarios";
    $scope.spublish = '';
    $scope.ssection = '';
    $scope.userid = '';
    $scope.mail = '';
    $scope.loadUsers = function() {
      $http.get(uri + "wservices/users", {
        params: {
          list: true
        }
      }).success(function(response) {
        if (response.status) {
          $scope.usersList = response.users;
        } else {
          swal("Alerta!", "Error al recuperar los datos", "warning");
        }
      });
    };
    $scope.loadRoles = function() {
      $http.get(uri + "wservices/roles", {
        params: {
          list: true
        }
      }).success(function(response) {
        if (response.status) {
          return $scope.rolesList = response.roles;
        } else {
          swal("Alerta!", "Error al recuperar roles", "warning");
        }
      });
    };
    $scope.loadMenu = function() {
      $http.get(location.origin + "/wservices/menu", {
        params: {
          list: true
        }
      }).success(function(response) {
        if (response.status) {
          $scope.lsgroup = response.menus;
        } else {
          swal("Alerta!", "Error al recuperar menu", "warning");
        }
      });
    };
    $scope.loadSubmenus = function() {
      var data;
      data = {
        list: true,
        menu: $scope.sgroup
      };
      $http.get(uri + "wservices/submenu", {
        params: data
      }).success(function(response) {
        if (response.status) {
          $scope.lsection = response.menus;
        } else {
          swal("Alerta!", "Error al recuperar submenu", "warning");
        }
      });
    };
    $scope.conAdmin = function(id, email) {
      var context;
      context = {
        usersid: id,
        email: email,
        saveAdmin: true
      };
      if (typeof email === "undefined" && email.indexof("@") === -1) {
        swal("Alerta!", "Parametro invalido.", "warning");
        return false;
      }
      $http.post(uri + "wservices/users", {
        params: context
      }).success(function(response) {
        if (response.status) {
          $scope.loadUsers();
          swal("Felicidades!", "Se convirtio en Administrador.", "success");
        } else {
          swal("Error!", "al convetir en admin. " + response.raise, "error");
        }
      });
    };
    $scope.conUsers = function(id, email) {
      var context;
      context = {
        usersid: id,
        email: email,
        saveUsers: true
      };
      if (typeof email === "undefined" && email.indexof("@") === -1) {
        swal("Alerta!", "Parametro invalido.", "warning");
        return false;
      }
      $http.post(uri + "wservices/users", {
        params: context
      }).success(function(response) {
        if (response.status) {
          $scope.loadUsers();
          swal("Felicidades!", "se a convertido en usuario a la cuenta " + email + ".", "success");
        } else {
          swal("Error!", "al convetir a usuario", "warning");
        }
      });
    };
    $scope.Desactive = function(id, email) {
      var context;
      context = {
        usersid: id,
        email: email,
        deactive: true
      };
      if (typeof email === "undefined" && email.indexof("@") === -1) {
        swal("Alerta!", "Parametro invalido.", "warning");
        return false;
      }
      swal({
        title: "Desactivar Cuenta!",
        text: "Realmente desea desactivar la cuenta " + email + "?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, Desactivar!',
        cancelButtonText: 'No, cancelar!',
        confirmButtonClass: 'confirm-class',
        cancelButtonClass: 'cancel-class'
      }, function(isConfirm) {
        if (isConfirm) {
          console.log("account deactive");
          $http.post(uri + "wservices/users", {
            params: context
          }).success(function(response) {
            if (response.status) {
              swal("Felicidades", "Se ha desactivado la cuenta", "success");
              console.log(response);
            } else {
              swal("Error", "no se a desactivado la cuenta " + email + ". " + response.raise, "error");
            }
          });
        }
      });
    };
    $scope.showRoles = function() {
      $scope.gshow = true;
      console.log(this);
      $scope.spublish = this.x.publish;
      $scope.ssection = this.x.section;
      $scope.sgroup = $scope.spublish;
      $scope.userid = this.x.users_id;
      $scope.mail = this.x.email;
      $timeout(function() {
        return $scope.loadSubmenus();
      }, 600);
    };
    $scope.saveEditor = function() {
      var data;
      if ($scope.section === "") {
        swal("Seleccione una Sección!", "debe de seleccionar una sección para el editor.", "warning");
        return false;
      }
      data = {
        publish: $scope.sgroup,
        section: $scope.section,
        uid: $scope.userid,
        mail: $scope.mail,
        saveEditor: true
      };
      swal({
        title: "Desea asignar permisos de editor para " + data.mail,
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si!',
        cancelButtonText: 'No!',
        confirmButtonClass: 'confirm-class',
        cancelButtonClass: 'cancel-class'
      }, function(isConfirm) {
        if (isConfirm) {
          $http.post(uri + "wservices/users", {
            params: data
          }).success(function(response) {
            if (response.status) {
              $scope.loadUsers();
              swal("Felicidades!", data.mail + " se ha hecho EDITOR!", "success");
              $timeout(function() {
                $scope.gshow = false;
              }, 2600);
            } else {
              swal("Error!", "al convertir al usuario en editor, " + response.raise, "error");
            }
          });
        }
      });
    };
    $scope.$watch('gshow', function() {
      if ($scope.gshow) {
        $scope.loadMenu();
      }
    });
    $scope.$watch('sgroup', function(old, nw) {
      console.log(old, "old");
      console.log(nw, "new");
    });
    $scope.loadUsers();
  };
  angular.module("usersApp").controller("userCtrl", userCtrl);
})();
