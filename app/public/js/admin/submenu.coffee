# require.config
#   baseUrl: "../app/public/"
#   paths:
#     angular: 'vendor/angularjs/angular.min'
#     jquery: 'vendor/jquery/dist/jquery.min'
#     bootstrap: 'vendor/bootstrap/dist/js/bootstrap.min'
#     swal: 'vendor/sweetalert/dist/sweetalert.min'
#   shim:
#     angular:
#       exports: 'angular'
#     bootstrap:
#       deps: ['jquery']

# requirejs ['angular', 'bootstrap', 'swal'], (angular) ->
app = angular.module "MenuApp", []

app.controller "MenuCtrl", ($scope, $http) ->
  uri = document.getElementById("uri").value
  # console.log "load Controller"
  # $scope.subtitle = "Hello World!"
  $scope.params = {}
  $scope.menuList = []
  $scope.paneltitle = 'Agregar Submenu'
  # Load list menus
  $scope.loadSubmenu = ->
    $scope.params =
      list: true
      menu: $scope.menuid
    $http.get "#{uri}wservices/submenu", params: $scope.params
      .success (response) ->
        $scope.params = {}
        $scope.menuList = response.menus
        return
      .error (error) ->
        console.log error
        return
      return
  # show menu edit
  $scope.showEdit = (menu, id, submenu, url, pos) ->
    console.log id, menu, pos
    $scope.paneltitle = 'Editar Menu'
    $scope.menuid = menu
    $scope.menu =
      submenuid: id
      submenu: submenu
      url: url
      position: pos
    $scope.btnadd = true
    return
  # watch change status btnadd for show or hide panel add or list
  $scope.$watch 'btnadd', (nval) ->
    console.log nval, 'change value'
    if nval is false
      #console.log $scope.menu
      $scope.menu = new Object
      $scope.paneltitle = 'Agregar Submenu'
      console.log $scope.menuid, "menu id"
    return
  # method for save edit or new menu
  $scope.saveSubmenu = ->
    context =
      menuid: $scope.menuid
      submenuid: $scope.menu.submenuid
      submenu: $scope.menu.submenu
      url: $scope.menu.url
      position: $scope.menu.position
    if context.submenuid is undefined or context.submenuid is ""
      context.save = 'new'
      delete context['submenuid']
    else
      context.save = 'edit'
    console.log context, "context send"
    $http
      withCredentials: false
      method: 'post'
      url: "#{uri}wservices/submenu"
      headers:
        'Content-Type': 'application/x-www-form-urlencoded'
      data: context
    .success (response) ->
      $scope.loadSubmenu()
      # $scope.menu.clean()
      $scope.btnadd = false
      return
    return
  $scope.deleteMenu = (menu, id) ->
    if id is undefined or id is "" and menu is undefined or menu is ""
      swal "Oops!", "El código no es valido.", "warning"
      return false
    swal
      title: "Desea eliminar el Submenu?"
      text: "Realmente desea eliminar submenu, debe de tener encuenta que se borrar permanentemente y todo su contenido!"
      type: "warning"
      showCancelButton: true
      confirmButtonColor: "#DD6B55"
      confirmButtonText: "Si!"
      cancelButtonText: "No!"
      closeOnConfirm: false
      closeOnCancel: true
    , (isConfirm) ->
      if (isConfirm)
        context =
          menuid: menu
          submenuid: id
          delete: true
        $http
          withCredentials: true
          method: 'post'
          url: "#{uri}wservices/submenu"
          headers:
            'Content-Type': 'application/x-www-form-urlencoded'
          data: context
        .success (response) ->
          if response.status
            $scope.loadSubmenu()
            swal "Felicidades!", "se a eliminado correctamente.", "success"
        return
    return
  return