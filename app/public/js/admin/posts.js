var app;

google.charts.load('43', {
  'packages': ['corechart', 'table']
});

app = angular.module('postApp', ['ngSanitize']);

app.controller('postCtrl', function($scope, $http, $timeout) {
  $scope.months = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'];
  $scope.uri = '';
  $scope.pinit = 1;
  $scope.lposts = [1];
  $scope.cm = $scope.lposts.length;
  $scope.range = new Array(0);
  angular.element(document).ready(function() {
    console.log("document init");
    $scope.listPosts($scope.pinit);
    $scope.getYears();
  });
  $scope.listPosts = function(val) {
    var data;
    $scope.pinit = val;
    data = {
      listPost: true,
      p: val
    };
    $http.get(($('#urlBase').val()) + "admin/posts/services", {
      params: data
    }).success(function(response) {
      if (response.status) {
        $scope.listposts = response.posts;
        if (response.page_links !== '') {
          $scope.tpage = Math.ceil(response.total / parseFloat(response.pbp));
        }
      } else {
        console.log("Error get data " + response.raise);
      }
    });
  };
  $scope.listPostsTitle = function(val) {
    var data;
    data = {
      listTitle: true,
      title: val
    };
    $http.get(($('#urlBase').val()) + "admin/posts/services", {
      params: data
    }).success(function(response) {
      if (response.status) {
        $scope.listposts = response.posts;
      } else {
        console.log("Error get data " + response.raise);
      }
    });
  };
  $scope.changeSlider = function($event) {
    var data;
    data = {
      changeSlider: true,
      post_id: $event.srcElement.value,
      status: $event.srcElement.checked
    };
    $http.get(($('#urlBase').val()) + "admin/posts/services", {
      params: data
    }).success(function(response) {
      if (response.status) {
        swal({
          title: "Correcto!",
          text: "",
          type: "success",
          showConfirmButton: false,
          timer: 1800
        });
      } else {
        swal("Error.", "No se pudo realizar la transacción", "error");
      }
    });
  };
  $scope.getPDay = function() {
    $http.get("", data, function(response) {
      if (response.status) {

      } else {
        return console.log("Nothing result for this request");
      }
    });
  };
  $scope.getPWeek = function() {
    $http.get("", data, function(response) {
      if (response.status) {

      } else {
        return console.log("Nothing result for this request");
      }
    });
  };
  $scope.getPMonth = function() {
    $http.get("", data, function(response) {
      if (response.status) {

      } else {
        return console.log("Nothing result for this request");
      }
    });
  };
  $scope.getPYear = function() {
    $http.get("", data, function(response) {
      if (response.status) {

      } else {
        return console.log("Nothing result for this request");
      }
    });
  };
  $scope.openwindow = function() {
    window.open("" + ($('#urlBase').val()) + this.x.posts_id + "/" + (this.x.title.replace(' ', '_')), "");
    console.log(($('#urlBase').val()) + "/" + this.x.posts_id + "/" + (this.x.title.replace(' ', '_')));
  };
  $scope.changelpost = function() {
    console.log(this);
    if ($scope.lposts.length === 0) {
      $scope.listPostsTitle($scope.search);
    }
  };
  $scope.getYears = function() {
    $http.get(location.origin + "/admin/posts/services", {
      params: {
        'years': true
      }
    }).success(function(response) {
      if (response.status) {
        $scope.years = response.year;
      } else {
        console.log("error");
      }
    });
  };
  $scope.getMonths = function() {
    var data;
    data = {
      year: $scope.slyear,
      months: true
    };
    $http.get(location.origin + "/admin/posts/services", {
      params: data
    }).success(function(response) {
      if (response.status) {
        $scope.monthbyy = response.month;
      } else {
        console.log("No hay meses para este anio.");
      }
    });
  };
  $scope.drawByAnioAreaChart = function() {
    google.charts.setOnLoadCallback(function() {
      $http.get(location.origin + "/admin/posts/services", {
        params: {
          'rptYear': true
        }
      }).success(function(response) {
        var arr, chart, data, dt, k, options, ref, ref1, table, v;
        console.log(response);
        if (response.status) {
          arr = new Array();
          arr.push(new Array('Year', 'Visitados'));
          ref = response.year;
          for (k in ref) {
            v = ref[k];
            arr.push(new Array(v.year, parseInt(v.visited)));
          }
          data = google.visualization.arrayToDataTable(arr);
          options = {
            title: 'Publicaciones mas Visitadas por Año',
            hAxis: {
              title: 'Año',
              titleTextStyle: {
                color: '#333'
              }
            },
            vAxis: {
              minValue: 0
            }
          };
          chart = new google.visualization.AreaChart(document.getElementById('byanio_area'));
          chart.draw(data, options);
          dt = new google.visualization.DataTable();
          dt.addColumn('string', 'Titulo');
          dt.addColumn('string', 'Año');
          dt.addColumn('number', 'Vistas');
          arr = new Array();
          ref1 = response.year;
          for (k in ref1) {
            v = ref1[k];
            arr.push(new Array(v.title, v.year, parseInt(v.visited)));
          }
          dt.addRows(arr);
          table = new google.visualization.Table(document.getElementById("byanio_table"));
          table.draw(dt, {
            showRowNumber: true,
            width: '100%',
            height: '100%'
          });
        } else {
          console.log("error http");
        }
      });
    });
  };
  $scope.drawByMonthAreaChart = function() {
    google.charts.setOnLoadCallback(function() {
      var data;
      data = {
        'monthbyyear': true,
        'year': $scope.mysel
      };
      $http.get(location.origin + "/admin/posts/services", {
        params: data
      }).success(function(response) {
        var arr, chart, dt, k, month, options, ref, ref1, table, v;
        if (response.status) {
          month = new Array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Juilo', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre');
          arr = new Array();
          arr.push(new Array('Mes', 'Visto'));
          ref = response.month;
          for (k in ref) {
            v = ref[k];
            arr.push(new Array(month[parseInt(v.month) - 1], parseInt(v.visited)));
          }
          dt = google.visualization.arrayToDataTable(arr);
          options = {
            title: "Publicaciones mas Visitadas por Meses " + $scope.mysel,
            hAxis: {
              title: 'Meses',
              titleTextStyle: {
                color: '#333'
              }
            },
            vAxis: {
              minValue: 0
            }
          };
          chart = new google.visualization.AreaChart(document.getElementById('bymonth_area'));
          chart.draw(dt, options);
          dt = new google.visualization.DataTable();
          dt.addColumn('string', 'Title');
          dt.addColumn('string', 'Mes');
          dt.addColumn('number', 'Visto');
          arr = new Array();
          ref1 = response.month;
          for (k in ref1) {
            v = ref1[k];
            arr.push(new Array(v.title, month[parseInt(v.month) - 1], parseInt(v.visited)));
          }
          dt.addRows(arr);
          table = new google.visualization.Table(document.getElementById('bymonth_table'));
          table.draw(dt, {
            showRowNumber: true,
            width: '100%',
            height: '100%'
          });
        }
      });
    });
  };
  $scope.getDataYears = function() {
    var data;
    data = {
      year: $scope.svyear,
      postbyyear: true
    };
    $http.get(location.origin + "/admin/posts/services", {
      params: data
    }).success(function(response) {
      if (response.status) {
        $scope.vtyear = response.pyear;
      } else {
        console.log("no hay datos para mostrar");
      }
    });
  };
  $scope.getDataMonths = function() {
    var data;
    data = {
      year: $scope.slyear,
      month: $scope.vmonth,
      postbymonth: true
    };
    $http.get(location.origin + "/admin/posts/services", {
      params: data
    }).success(function(response) {
      if (response.status) {
        $scope.vtmonth = response.pmonth;
      } else {
        console.log("No hay datos registrados para esta consulta");
      }
    });
  };
  $scope.deletePost = function() {
    var chk, prm;
    prm = {
      deletePost: true,
      post: new Array
    };
    chk = document.getElementsByName("postdel");
    angular.forEach(angular.element(chk), function(value, key) {
      var el;
      el = angular.element(value);
      if (el[0].checked) {
        prm.post.push(el.val());
      }
    });
    $timeout(function() {
      if (prm.post.length) {
        swal({
          title: "Realmente desea eliminar la(s) Publicacion(es)?",
          text: "",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si, Eliminar!',
          cancelButtonText: 'No!',
          confirmButtonClass: 'confirm-class',
          cancelButtonClass: 'cancel-class'
        }, function(isConfirm) {
          if (isConfirm) {
            $http.post(location.origin + "/admin/posts/services", {
              params: prm
            }).success(function(response) {
              if (response.status) {
                $scope.listPosts($scope.pinit);
              } else {
                swal("Error", "No se han encontrado datos.", "error");
              }
            });
          }
        });
      }
    }, 800);
  };
  $scope.$watch('cm', function() {
    if ($scope.cm === 0) {
      $scope.listPostsTitle($scope.search);
    }
  });
});
