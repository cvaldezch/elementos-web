var app;

app = angular.module('chpapp', []);

app.controller('chpCtrl', function($scope, $http) {
  $scope.bsend = true;
  $scope.repass = '';
  angular.element(document).ready(function() {
    $.validate({
      modules: 'security',
      borderColorOnError: '#FFF'
    });
  });
  $scope.change = function($event) {
    if ($scope.pass === $scope.repass) {
      $scope.bsend = false;
    } else {
      $scope.bsend = true;
    }
  };
  $scope.changePasswd = function($event) {
    var prm;
    console.log("send data server");
    prm = {
      'mail': $scope.mail,
      'csrf': $scope.csrf,
      'passwd': $scope.repass
    };
    console.log(prm);
    return $http({
      url: location.origin + "/change/passwd",
      method: 'post',
      data: $.param(prm),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }).success(function(response) {
      if (response.status) {
        prm = {
          name: $scope.name,
          from: "noreply@" + location.hostname,
          email: $scope.mail,
          subject: "Cambio de Clave",
          body: "<html lang=\"es\">\n<head>\n  <meta charset=\"utf-8\">\n  <title>" + $scope.name + "</title>\n</head>\n<body>\n  <!--<div style=\"text-align: center;\">\n      <a href=\"" + location.origin + "\" title=\"" + $scope.name + "\">\n        <img style=\"height: 95px;  width: 86px;\" src=\"" + location.origin + "/app/public/images/" + $scope.name + ".png\" alt=\"" + $scope.name + "\">\n      </a>\n  </div>-->\n  <br>\n  <strong>Se cambio la clave de su cuenta de " + $scope.name + "</strong>.\n  <p>\n  La contraseña para tu cuenta de " + $scope.name + ", " + $scope.mail + ", se ha cambiado recientemente. <br>\n  Si la has cambiado tú, no necesitas realizar ninguna otra acción. \n  </p>\n  <p>\n  Si no la has cambiado tú, es posible que tu cuenta haya sido vulnerada. Para poder volver a iniciar sesión en tu cuenta, deberás restablecer la contraseña. \n  </p>\n  <p style=\"text-align:center;\">\n      Los signos ortográficos fueron omitidos intencionalmente en este correo electrónico.<br/>\n      Estás suscrito con la siguiente dirección: " + $scope.mail + " Por favor, no respondas a este correo electrónico.\n  </p>\n</body>\n</html>"
        };
        return $.ajax({
          url: "http://188.166.105.23:3000",
          data: prm,
          type: 'GET',
          crossDomain: true,
          dataType: "text",
          success: function(result) {
            result = String(result);
            result = JSON.parse(result);
            if (result.status) {
              swal("", "Se ha modificado correctamente su contraseña.", "success");
              setTimeout(function() {
                return location.href = "/";
              }, 2600);
            } else {
              swal("", "No se ha cambiado el contraseña intentelo mas tarde!", "warning");
            }
          }
        });
      } else {
        swal("", "No se ha cambiado el contraseña intentelo mas tarde!", "warning");
      }
    });
  };
  $scope.test = function() {
    var prm;
    prm = {
      name: $scope.name,
      from: "noreply@" + location.hostname,
      email: $scope.mail,
      subject: "Cambio de Clave",
      body: "<html lang=\"es\">\n<head>\n  <meta charset=\"utf-8\">\n  <title>" + $scope.name + "</title>\n</head>\n<body>\n  <!--<div style=\"text-align: center;\">\n      <a href=\"" + location.origin + "\" title=\"" + $scope.name + "\">\n        <img style=\"height: 95px;  width: 86px;\" src=\"" + location.origin + "app/public/images/" + $scope.name + ".png\" alt=\"" + $scope.name + "\">\n      </a>\n  </div>-->\n  <br>\n  <strong>Se cambio la clave de su cuenta de " + $scope.name + "</strong>.\n  <p>\n  La contraseña para tu cuenta de " + $scope.name + ", " + $scope.mail + ", se ha cambiado recientemente. <br>\n  Si la has cambiado tú, no necesitas realizar ninguna otra acción. \n  </p>\n  <p>\n  Si no la has cambiado tú, es posible que tu cuenta haya sido vulnerada. Para poder volver a iniciar sesión en tu cuenta, deberás restablecer la contraseña. \n  </p>\n  <p style=\"text-align:center;\">\n      Los signos ortográficos fueron omitidos intencionalmente en este correo electrónico.<br/>\n      Estás suscrito con la siguiente dirección: " + $scope.mail + " Por favor, no respondas a este correo electrónico.\n  </p>\n</body>\n</html>"
    };
    $.ajax({
      url: "http://188.166.105.23:3000",
      data: prm,
      type: 'GET',
      crossDomain: true,
      dataType: "text",
      success: function(result) {
        result = String(result);
        result = JSON.parse(result);
        if (result.status) {
          swal("", "", "warning");
          location.href = "/register/send";
        } else {
          swal("", "", "error");
          location.href = "/register/error";
        }
      }
    });
  };
});
