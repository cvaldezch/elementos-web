app = angular.module 'chpapp', []

app.controller 'chpCtrl', ($scope, $http) ->
  $scope.bsend = true
  $scope.repass = ''
  angular.element(document).ready ->
    $.validate
      modules: 'security'
      borderColorOnError : '#FFF'
    return

  $scope.change = ($event) ->
    if $scope.pass is $scope.repass
      $scope.bsend = false
    else
      $scope.bsend = true
    return
  
  $scope.changePasswd = ($event) ->
    console.log "send data server"
    prm =
      'mail': $scope.mail
      'csrf': $scope.csrf
      'passwd': $scope.repass
    console.log prm
    $http
      url: "#{location.origin}/change/passwd"
      method: 'post'
      data: $.param prm
      headers:
        'Content-Type': 'application/x-www-form-urlencoded'
    .success (response) ->
      if response.status
        prm =
          name: $scope.name
          from: "noreply@#{location.hostname}"
          email: $scope.mail
          subject: "Cambio de Clave"
          body: """<html lang="es">
                <head>
                  <meta charset="utf-8">
                  <title>#{$scope.name}</title>
                </head>
                <body>
                  <!--<div style="text-align: center;">
                      <a href="#{location.origin}" title="#{$scope.name}">
                        <img style="height: 95px;  width: 86px;" src="#{location.origin}/app/public/images/#{$scope.name}.png" alt="#{$scope.name}">
                      </a>
                  </div>-->
                  <br>
                  <strong>Se cambio la clave de su cuenta de #{$scope.name}</strong>.
                  <p>
                  La contraseña para tu cuenta de #{$scope.name}, #{$scope.mail}, se ha cambiado recientemente. <br>
                  Si la has cambiado tú, no necesitas realizar ninguna otra acción. 
                  </p>
                  <p>
                  Si no la has cambiado tú, es posible que tu cuenta haya sido vulnerada. Para poder volver a iniciar sesión en tu cuenta, deberás restablecer la contraseña. 
                  </p>
                  <p style="text-align:center;">
                      Los signos ortográficos fueron omitidos intencionalmente en este correo electrónico.<br/>
                      Estás suscrito con la siguiente dirección: #{$scope.mail} Por favor, no respondas a este correo electrónico.
                  </p>
                </body>
                </html>"""
        # $http.jsonp "http://188.166.105.23:300?#{$.param(prm)}&callback=JSON_CALLBACK"
        #   .success (response) ->
        #     console.log response
        #     if response.status
        #       href = location.origin
        # return
        $.ajax
          url: "http://188.166.105.23:3000"
          data: prm
          type: 'GET'
          crossDomain: true
          dataType: "text"
          success: (result) ->
            result = String result
            result = JSON.parse result
            if result.status
              swal "", "Se ha modificado correctamente su contraseña.", "success"
              setTimeout ->
                location.href = "/"
              , 2600
              return
            else
              swal "", "No se ha cambiado el contraseña intentelo mas tarde!", "warning"
              return
      else
        swal "", "No se ha cambiado el contraseña intentelo mas tarde!", "warning"
        return

  $scope.test = ->
    prm =
      name: $scope.name
      from: "noreply@#{location.hostname}"
      email: $scope.mail
      subject: "Cambio de Clave"
      body: """<html lang="es">
                <head>
                  <meta charset="utf-8">
                  <title>#{$scope.name}</title>
                </head>
                <body>
                  <!--<div style="text-align: center;">
                      <a href="#{location.origin}" title="#{$scope.name}">
                        <img style="height: 95px;  width: 86px;" src="#{location.origin}app/public/images/#{$scope.name}.png" alt="#{$scope.name}">
                      </a>
                  </div>-->
                  <br>
                  <strong>Se cambio la clave de su cuenta de #{$scope.name}</strong>.
                  <p>
                  La contraseña para tu cuenta de #{$scope.name}, #{$scope.mail}, se ha cambiado recientemente. <br>
                  Si la has cambiado tú, no necesitas realizar ninguna otra acción. 
                  </p>
                  <p>
                  Si no la has cambiado tú, es posible que tu cuenta haya sido vulnerada. Para poder volver a iniciar sesión en tu cuenta, deberás restablecer la contraseña. 
                  </p>
                  <p style="text-align:center;">
                      Los signos ortográficos fueron omitidos intencionalmente en este correo electrónico.<br/>
                      Estás suscrito con la siguiente dirección: #{$scope.mail} Por favor, no respondas a este correo electrónico.
                  </p>
                </body>
                </html>"""
    # $http.jsonp "http://188.166.105.23:300?#{$.param(prm)}&callback=JSON_CALLBACK"
    #   .success (response) ->
    #     console.log response
    #     return
    $.ajax
      url: "http://188.166.105.23:3000"
      data: prm
      type: 'GET'
      crossDomain: true
      dataType: "text"
      success: (result) ->
        result = String result
        result = JSON.parse result
        if result.status
          swal "", "", "warning"
          location.href = "/register/send"
          return
        else
          swal "", "", "error"
          location.href = "/register/error"
          return
    return
  return
