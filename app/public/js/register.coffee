$(document).ready ->
    $("[name=email]").on("blur", validEmail)
    $("[name=user]").on("blur", validUser)
    $(document).on "submit", "#frmregister", (event) ->
        event.preventDefault()
        data = $('#frmregister').serializeArray()
        $.ajax
            url: ""
            type: "post"
            dataType: "json"
            data: data
            success: (response) ->
                if response.status
                    prm = {}
                    prm.name = $("input[name=dname]").val()
                    prm.from = "noreply@#{location.hostname}"
                    prm.email = response.email
                    prm.subject = "Confirmación de Registro"
                    prm.body = """<html lang="es">
                              <head>
                                <meta charset="utf-8">
                                <title>#{prm.name}</title>
                              </head>
                              <body>
                              <!--<div style="text-align: center;">
                                  <a href="#{response.domain}" title="Elements">
                                    <img style="height: 95px;  width: 86px;" src="#{response.domain}app/public/images/#{prm.name}.png" alt="Elementos">
                                  </a>
                              </div>-->
                              <h1>Hola!</h1>
                              <p>
                                  <b>Bienvenidos a #{prm.name}</b>.
                                  Gracias por registrarse haga click en el siguiente enlace para confirmar su registro. <a href="#{response.domain}active/register/#{response.csrf}/#{response.email}/#{response.username}" target="_blank">Click aqui.</a>
                              </p>
                              <p>
                                  Gracias y te invitamos a seguir con nosotros.
                              </p>
                              <p>
                                  Site: <a href="http://www.elementos.com.pe">http://elementos.com.pe</a><br />
                                  Twitter: <a href="https://twitter.com/elementospe" target="_blank">https://twitter.com/elementospe</a><br />
                                  Facebook: <a href="https://www.facebook.com/elementospe" target="_blank">https://www.facebook.com/elementospe</a><br />
                                  Instagram: <a href="https://instagram.com/elementos_pe">https://instagram.com/elementos.pe</a><br />
                                  Tumblr: <a href="http://elementospe.tumblr.com">http://elementospe.tumblr.com</a><br />
                                  YouTube: <a href="https://www.youtube.com/elementospe" targe="_blank">https://www.youtube.com/elementospe</a><br />
                              </p>
                              <p style="text-align:center;">
                                  Los signos ortográficos fueron omitidos intencionalmente en este correo electrónico.<br/>
                                  Estás suscrito con la siguiente dirección: #{response.email} Por favor, no respondas a este correo electrónico.
                              </p>
                              </body>
                              </html>"""
                    $.ajax
                        url: "http://188.166.105.23:3000"
                        data: prm
                        type: 'GET'
                        crossDomain: true
                        dataType: "text"
                        success: (result) ->
                            result = String result
                            result = JSON.parse result
                            if result.status
                                location.href = "/register/send"
                                return
                            else
                                location.href = "/register/error"
                                return
                    return
                else
                    location.href = "/register/error"
                    return
        return
    $.validate
        modules: 'security'
    return
validEmail = (event) ->
    context = 
      validEmail: true,
      email: $.trim(this.value)
    if context['email'] == ""
      return false;
    $.getJSON "", context, (response) ->
      $email = $(".vemail")
      if response.status
        $email.find("input").val("")
        $email.find("span").html("").append("<span></span>").find("span").addClass("fa fa-times")
        swal("Oops!", "El correo ingresado ya existe!", "warning")
        return
      else
        $email.find("span").html("").append("<span></span>").find("span").addClass("fa fa-check")
        return
    return
  # valid user if exists
validUser = (event) ->
    context =
      validUser: true,
      user: $.trim(this.value)
    if context.user == ""
      return false
    $.getJSON "", context, (response) ->
      $user = $(".vuser")
      if response.status
        $user.find("input").val ""
        $user.find("span").html("").append("<span></span>").find("span").addClass("fa fa-times")
        swal("Oops!", "El usuario ingresado ya existe!", "warning")
        return
      else
        $user.find("span").html("").append("<span></span>").find("span").addClass "fa fa-check"
        return
    return
