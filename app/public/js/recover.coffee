uri = document.getElementsByName("uri")[0].value
require.config
  baseUrl: "#{uri}app/public/"
  removeCombined: true
  paths:
    "jquery": "vendor/jquery/dist/jquery.min"
    "formvalidate": "vendor/jquery-form-validator/form-validator/jquery.form-validator.min"
    "swal": "vendor/sweetalert/dist/sweetalert.min"
    "users": "js/modules/users"
  shim:
    formvalidate:
      deps: ['jquery']
    users: ['jquery', 'swal']

requirejs ['jquery', 'formvalidate', 'users'], ($) ->
  $ ->
    $.validate
      modules: 'security'
      form: "#formrecover"
      onSuccess: ->
        restore()
        return false
    return
  return

restore = (event) ->
  require ['users'], (Users) ->
    user = new Users
      email: $("[name=email]").val()
      password: $("[name=passwd]").val()
      csrf: $("[name=csrf]").val()
    user.restorePwd()
    return
  return
