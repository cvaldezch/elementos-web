var validEmail, validUser;

$(document).ready(function() {
  $("[name=email]").on("blur", validEmail);
  $("[name=user]").on("blur", validUser);
  $(document).on("submit", "#frmregister", function(event) {
    var data;
    event.preventDefault();
    data = $('#frmregister').serializeArray();
    $.ajax({
      url: "",
      type: "post",
      dataType: "json",
      data: data,
      success: function(response) {
        var prm;
        if (response.status) {
          prm = {};
          prm.name = $("input[name=dname]").val();
          prm.from = "noreply@" + location.hostname;
          prm.email = response.email;
          prm.subject = "Confirmación de Registro";
          prm.body = "<html lang=\"es\">\n<head>\n  <meta charset=\"utf-8\">\n  <title>" + prm.name + "</title>\n</head>\n<body>\n<!--<div style=\"text-align: center;\">\n    <a href=\"" + response.domain + "\" title=\"Elements\">\n      <img style=\"height: 95px;  width: 86px;\" src=\"" + response.domain + "app/public/images/" + prm.name + ".png\" alt=\"Elementos\">\n    </a>\n</div>-->\n<h1>Hola!</h1>\n<p>\n    <b>Bienvenidos a " + prm.name + "</b>.\n    Gracias por registrarse haga click en el siguiente enlace para confirmar su registro. <a href=\"" + response.domain + "active/register/" + response.csrf + "/" + response.email + "/" + response.username + "\" target=\"_blank\">Click aqui.</a>\n</p>\n<p>\n    Gracias y te invitamos a seguir con nosotros.\n</p>\n<p>\n    Site: <a href=\"http://www.elementos.com.pe\">http://elementos.com.pe</a><br />\n    Twitter: <a href=\"https://twitter.com/elementospe\" target=\"_blank\">https://twitter.com/elementospe</a><br />\n    Facebook: <a href=\"https://www.facebook.com/elementospe\" target=\"_blank\">https://www.facebook.com/elementospe</a><br />\n    Instagram: <a href=\"https://instagram.com/elementos_pe\">https://instagram.com/elementos.pe</a><br />\n    Tumblr: <a href=\"http://elementospe.tumblr.com\">http://elementospe.tumblr.com</a><br />\n    YouTube: <a href=\"https://www.youtube.com/elementospe\" targe=\"_blank\">https://www.youtube.com/elementospe</a><br />\n</p>\n<p style=\"text-align:center;\">\n    Los signos ortográficos fueron omitidos intencionalmente en este correo electrónico.<br/>\n    Estás suscrito con la siguiente dirección: " + response.email + " Por favor, no respondas a este correo electrónico.\n</p>\n</body>\n</html>";
          $.ajax({
            url: "http://188.166.105.23:3000",
            data: prm,
            type: 'GET',
            crossDomain: true,
            dataType: "text",
            success: function(result) {
              result = String(result);
              result = JSON.parse(result);
              if (result.status) {
                location.href = "/register/send";
              } else {
                location.href = "/register/error";
              }
            }
          });
        } else {
          location.href = "/register/error";
        }
      }
    });
  });
  $.validate({
    modules: 'security'
  });
});

validEmail = function(event) {
  var context;
  context = {
    validEmail: true,
    email: $.trim(this.value)
  };
  if (context['email'] === "") {
    return false;
  }
  $.getJSON("", context, function(response) {
    var $email;
    $email = $(".vemail");
    if (response.status) {
      $email.find("input").val("");
      $email.find("span").html("").append("<span></span>").find("span").addClass("fa fa-times");
      swal("Oops!", "El correo ingresado ya existe!", "warning");
    } else {
      $email.find("span").html("").append("<span></span>").find("span").addClass("fa fa-check");
    }
  });
};

validUser = function(event) {
  var context;
  context = {
    validUser: true,
    user: $.trim(this.value)
  };
  if (context.user === "") {
    return false;
  }
  $.getJSON("", context, function(response) {
    var $user;
    $user = $(".vuser");
    if (response.status) {
      $user.find("input").val("");
      $user.find("span").html("").append("<span></span>").find("span").addClass("fa fa-times");
      swal("Oops!", "El usuario ingresado ya existe!", "warning");
    } else {
      $user.find("span").html("").append("<span></span>").find("span").addClass("fa fa-check");
    }
  });
};
