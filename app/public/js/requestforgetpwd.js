var app;

app = angular.module('reqFP', []);

app.controller('reqFPCtrl', function($scope, $http) {
  $scope.vemail = true;
  $scope.baseURI = location.host;
  $scope.name = "";
  $scope.psend = false;
  $scope.pfail = false;
  $scope.pp = false;
  angular.element(document).ready(function() {
    $scope.name = angular.element("#name")[0].value;
    console.log("here init log");
  });
  $scope.validEmail = function($event) {
    var data;
    console.log($event);
    data = {
      email: $event.currentTarget.value,
      validEmail: true
    };
    $http.get("/user/register", {
      params: data
    }).success(function(response) {
      if (response.status) {
        return $scope.vemail = false;
      } else {
        $scope.vemail = true;
        console.log('emial not exists');
      }
    });
  };
  $scope.keyMail = function($event) {
    var data, patterns;
    patterns = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    console.log($event.currentTarget.value);
    if (patterns.test($event.currentTarget.value)) {
      data = {
        email: $event.currentTarget.value,
        validEmail: true
      };
      console.log(data);
      $http.get("/user/register", {
        params: data
      }).success(function(response) {
        if (response.status) {
          return $scope.vemail = false;
        } else {
          console.log('emial not exists');
          $scope.vemail = true;
        }
      });
    }
  };
  $scope.sendEmail = function($event) {
    var data;
    console.log("log en send mail");
    data = {
      email: $scope.mail,
      registerRec: true
    };
    $http({
      data: $.param(data),
      url: '/wservices/pwd/send/recover',
      method: 'post',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }).success(function(response) {
      var html, prm;
      if (response.status) {
        html = "<html lang=\"es\">\n<head>\n  <meta charset=\"utf-8\">\n  <title>" + $scope.name + "</title>\n</head>\n<body>\n<!--<div style=\"text-align: center;\">\n    <a href=\"" + location.origin + "\" title=\"Elements\">\n      <img style=\"height: 95px;  width: 86px;\" src=\"" + location.host + "/app/public/images/" + $scope.name + ".png\" alt=\"Elementos\">\n    </a>\n</div>-->\n<h3>Solicitud cambio de contraseña</h3>\n<p>\n  Se ha solicitado el cambiado de contraseña recientemente para su cuenta " + response.mail + ".\n</p>\n<p>\n  Si has solicitado cambiar tú contraseña,  deberás restablecer desde el siguiente formulario.\n</p>\n<p>\n  <a href=\"" + location.origin + "/request/change/passwd/" + response.csrf + "/" + $scope.mail + "\">Recuperar Contraseña</a>\n</p>\n<p>\n  <small style=\"text-align:center;\">\n    Los signos ortográficos fueron omitidos intencionalmente en este correo electrónico.<br/>\n          Estás suscrito con la siguiente dirección: " + $scope.mail + " Por favor, no respondas a este correo electrónico.\n  </small>\n</p>\n</body>\n</html>";
        prm = {
          name: $scope.name,
          email: response.mail,
          from: "noreply@" + location.hostname,
          subject: "Recupera tu clave de " + $scope.name,
          body: html
        };
        console.log(prm);
        $.ajax({
          url: "http://188.166.105.23:3000",
          data: prm,
          type: 'GET',
          crossDomain: true,
          dataType: "text",
          success: function(result) {
            result = String(result);
            result = JSON.parse(result);
            if (result.status) {
              $scope.psend = true;
              $scope.pp = true;
              $scope.psend = true;
              $scope.pp = true;
            } else {
              $scope.pfail = true;
              $scope.pp = true;
              $scope.pfail = true;
              $scope.pp = true;
            }
          }
        });
      } else {
        return console.log("error al registar recuperacion de password!.");
      }
    });
  };
});
