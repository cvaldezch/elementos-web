define ["jquery", "swal", "mustache"], ($, swal, Mustache) ->
  class Post
    constructor: (options = {}) ->
      {@posts_id, @title, @image, @content, @references, @date, @hour, @sgroup, @section, @category} = options

    uri = $("[name=uri]").val()

    this::listPosts = ->
      context =
        list: true
      $.getJSON "#{uri}wservices/profile", context, (response) ->
        if response.status
          template = """
          {{#posts}}
          <tr>
            <td>{{ index }}</td>
            <td>{{ title }}</td>
            <td>{{ register }}</td>
            <td>{{ publish }}</td>
            <td>
              <div class="dropdown">
                <button class="btn btn-xs btn-link text-black dropdown-toggle" data-toggle="dropdown">
                  <span class="glyphicon glyphicon-cog"></span>
                </button>
                <ul class="dropdown-menu">
                  <li>
                    <button type="button" class="btn btn-xs btn-link text-black editPost" data-id="{{ posts_id }}" data-title="{{ title }}" data-content="{{ content }}" data-publish="{{ publish }}" data-email="{{ user_id }}" data-image="{{ image }}" data-category="{{ category }}" data-sgroup="{{ sgroup }}" data-section="{{ section }}">
                      Editar
                    </button>
                  </li>
                  <li>
                    <button type="button" class="btn btn-xs btn-link text-black" value="{{ posts_id }}">
                      Eliminar
                    </button>
                  </li>
                </ul>
              </div>
            </td>
          </tr>
          {{/posts}}
          """
          $tb = $(".table-posts > tbody")
          $tb.empty()
          count = 1
          response.index = -> return count++
          $tb.html Mustache.render template, response
          return
        else
          swal "Oops!", "No se a podido traer los datos, intentelo en unos minutos. #{response.raise}", "error"
          return

    this::savePost =  ->
      if this.title is undefined or this.title is ""
        swal "Alerta!", "El Título no debe de estar vacio.", "warning"
        return false
      if this.content is undefined or this.content is ""
        swal "Alerta!", "El contenido de post no debe estar vacio.", "warning"
        return false
      if this.posts_id is undefined or this.posts_id is ""
        if this.image is undefined or this.image.length is 0
          swal "Alerta!", "Ingrese una imagen.", "warning"
          return false
      if this.title is undefined or this.title is ""
        swal "Alerta!", "El Título no debe de estar vacio.", "warning"
        return false
      if this.sgroup is undefined or this.sgroup is ""
        swal "Alerta!", "Al parecer no estas asignado a un grupo.", "warning"
        return false
      if this.section is undefined or this.section is ""
        swal "Alerta!", "No tienes una sección para publicar.", "warning"
        return false
      context = new FormData()
      if this.posts_id isnt undefined or this.posts_id isnt ""
        context.append "posts_id", this.posts_id
        context.append "editPost", true
      else
        context.append "savePost", true
      context.append "title", this.title
      if typeof(this.image) isnt "undefined"
        context.append "image", this.image
      context.append "content", this.content
      context.append "references", this.references
      context.append "date", this.date
      context.append "hour", this.hour
      context.append "sgroup", this.sgroup
      context.append "section", this.section
      context.append "category", this.category
      $.ajax
        url: "#{uri}wservices/post"
        data: context
        type: "post"
        dataType: "json"
        contentType: false
        processData: false
        cache: false
        success: (response) ->
          if response.status
            swal
              title: "Felicidades!"
              text: "Se público correctamente!"
              type: "success"
              showButtonConfirm: false
              timer: 2600
            setTimeout ->
              location.reload()
            , 2600
            return
          else
            swal "Error", "No se a podido guardar la publicación, por favor intenté en unos minutos.", "warning"
            return
      return

  return Post