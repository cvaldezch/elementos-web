define(["jquery", "swal", "nail"], function($, sw) {
  var Profile;
  Profile = (function() {
    var $base;

    function Profile(options) {
      if (options == null) {
        options = {};
      }
      this.email = options.email, this.firstname = options.firstname, this.lastname = options.lastname, this.flag = options.flag, this.slogan = options.slogan, this.photo = options.photo, this.real = options.real, this.fictional = options.fictional, this.sex = options.sex, this.range = options.range;
    }

    $base = $("[name=base]").val();

    Profile.prototype.saveProfileBasic = function() {
      var context;
      context = {
        email: this.email,
        firstname: this.firstname,
        lastname: this.lastname,
        lifer: this.real,
        lifef: this.fictional,
        sex: this.sex
      };
      if (context.email === void 0 || context.email === "") {
        console.error("data invalid email empty!");
        return false;
      }
      $.post($base + "/saveprofile", context, function(response) {
        if (response.status) {
          sw({
            title: "Felicidades!",
            text: "se ha guardado correctamente los datos del perfil.",
            timer: 3000,
            type: "success",
            showButtonConfirm: false
          });
        } else {
          sw({
            title: 'Oops!',
            text: "No hemos podido almacenar tus datos, por favor vuelva a intentarlo.",
            timer: 3000,
            type: "error",
            showButtonConfirm: false
          });
        }
      }, "json");
    };

    Profile.prototype.uploadTmp = function(imgLoad) {
      var form;
      if (this.photo === void 0 || this.photo.length <= 0) {
        sw('Alerta carga de imagen', 'No se a seleccionado una imagen valida!');
        return false;
      }
      console.info(this);
      form = new FormData;
      form.append("email", this.email);
      form.append("photo", this.photo);
      form.append("temp", true);
      $.ajax({
        url: $base + "/img/tmp",
        data: form,
        type: "POST",
        dataType: "json",
        cache: false,
        processData: false,
        contentType: false,
        success: function(response) {
          console.log(response);
          if (response.status) {
            $(".photo-loagind").hide();
            $("." + imgLoad).attr("src", $base + "../../../app/" + response.filename);
            $("." + imgLoad).attr("data-src", "" + response.filename);
            $("." + imgLoad).attr("data-range", "0.2");
            $("." + imgLoad).css("background-repeat", "no-repeat");
            $("." + imgLoad).nailthumb({
              width: 200,
              height: 200,
              proportions: 0.2,
              method: 'crop',
              fitDirection: 'bottom center'
            });
            $(".imgsrc, .slider").show();
          } else {
            sw("Oops!!", "" + response.raise, response.type);
          }
        }
      });
    };

    Profile.prototype.saveAccount = function() {
      var context;
      if (this.email === void 0) {
        return false;
      }
      context = {
        email: this.email,
        photo: this.photo,
        slogan: this.slogan,
        range: this.range
      };
      if (context.range === void 0 || context.range === "" || context.range === null) {
        context.range = 0.2;
      }
      $.post($base + "/save/account", context, function(response) {
        if (response.status) {
          sw({
            title: "Felicidades!",
            text: "se ha guardado correctamente los datos del perfil.",
            timer: 3000,
            type: "success",
            showButtonConfirm: false
          });
          location.reload();
        } else {
          sw({
            title: 'Oops!',
            text: "No hemos podido almacenar tus datos, por favor vuelva a intentarlo. " + response.raise,
            timer: 3000,
            type: "error",
            showButtonConfirm: false
          });
        }
      }, "json");
    };

    return Profile;

  })();
  console.log("module load Profile");
  return Profile;
});
