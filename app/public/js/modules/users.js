define(['jquery', 'swal'], function() {
  var Users;
  Users = (function() {
    var $base, $uri;

    function Users(options) {
      if (options == null) {
        options = {};
      }
      this.email = options.email, this.username = options.username, this.password = options.password, this.passwdold = options.passwdold, this.active = options.active, this.csrf = options.csrf, this.publish = options.publish, this.section = options.section, this.comment = options.comment;
    }

    $base = $("[name=base]").val();

    $uri = $("[name=uri]").val();

    Users.prototype.changePasswd = function() {
      var context;
      context = {
        email: this.email,
        passwd: this.password,
        old: this.passwdold
      };
      if (context.email === void 0 || context.email === null || context.email === "") {
        return false;
      }
      $.post($base + "/change/passwd", context, function(response) {
        if (response.status) {
          swal("Felicidades!", "se ha cambiado correctamente la contraseña.", "success");
        } else {
          swal("Oops!", "No se ha cambiado la contraseña. " + response.raise, "error");
        }
      }, "json");
    };

    Users.prototype.requestChange = function() {
      var context;
      context = {
        email: this.email
      };
      if (context.email === void 0 || context.email === null || context.email === "") {
        return false;
      }
      $.post($uri + "register/restore", context, function(response) {
        if (response.status) {
          swal("Alerta!", "Se a enviado un correo a la dirección establecida. desde ahí puede restablecer su contraseña.", "info");
        } else {
          swal("Oops!", "No se a registrado la solicitud para restaurar la Contraseña.", "error");
        }
      }, "json");
    };

    Users.prototype.restorePwd = function() {
      var context;
      context = {
        email: this.email,
        password: this.password,
        csrf: this.csrf
      };
      $.post($uri + "restore/pwd", context, function(response) {
        if (response.status) {
          location.href = $uri + "login";
        } else {
          swal({
            title: "Opps!",
            text: "Error al cambiar la Contraseña",
            timer: 3000,
            type: "error",
            showButtonCornfirm: false
          });
        }
      }, "json");
    };

    Users.prototype.sendPublish = function() {
      var context;
      context = {
        email: this.email,
        section: this.section,
        comment: this.comment
      };
      $.post($base + "/send/publish", context, function(response) {
        if (response.status) {
          swal({
            title: "Felicidades!",
            text: "se a enviado la solicitud, despues de la evalución se le enviara un correo de confirmación.",
            timer: 3000,
            type: "success",
            showButtonCornfirm: false
          }, setTimeout(function() {
            location.reload();
          }, 2600));
        } else {
          return swal({
            title: "Opps!",
            text: "Error enviar la solicitud de publicación",
            timer: 3000,
            type: "error",
            showButtonCornfirm: false
          });
        }
      }, "json");
    };

    return Users;

  })();
  return Users;
});
