define(["jquery", "swal", "mustache"], function($, swal, Mustache) {
  var Post;
  Post = (function() {
    var uri;

    function Post(options) {
      if (options == null) {
        options = {};
      }
      this.posts_id = options.posts_id, this.title = options.title, this.image = options.image, this.content = options.content, this.references = options.references, this.date = options.date, this.hour = options.hour, this.sgroup = options.sgroup, this.section = options.section, this.category = options.category;
    }

    uri = $("[name=uri]").val();

    Post.prototype.listPosts = function() {
      var context;
      context = {
        list: true
      };
      return $.getJSON(uri + "wservices/profile", context, function(response) {
        var $tb, count, template;
        if (response.status) {
          template = "{{#posts}}\n<tr>\n  <td>{{ index }}</td>\n  <td>{{ title }}</td>\n  <td>{{ register }}</td>\n  <td>{{ publish }}</td>\n  <td>\n    <div class=\"dropdown\">\n      <button class=\"btn btn-xs btn-link text-black dropdown-toggle\" data-toggle=\"dropdown\">\n        <span class=\"glyphicon glyphicon-cog\"></span>\n      </button>\n      <ul class=\"dropdown-menu\">\n        <li>\n          <button type=\"button\" class=\"btn btn-xs btn-link text-black editPost\" data-id=\"{{ posts_id }}\" data-title=\"{{ title }}\" data-content=\"{{ content }}\" data-publish=\"{{ publish }}\" data-email=\"{{ user_id }}\" data-image=\"{{ image }}\" data-category=\"{{ category }}\" data-sgroup=\"{{ sgroup }}\" data-section=\"{{ section }}\">\n            Editar\n          </button>\n        </li>\n        <li>\n          <button type=\"button\" class=\"btn btn-xs btn-link text-black\" value=\"{{ posts_id }}\">\n            Eliminar\n          </button>\n        </li>\n      </ul>\n    </div>\n  </td>\n</tr>\n{{/posts}}";
          $tb = $(".table-posts > tbody");
          $tb.empty();
          count = 1;
          response.index = function() {
            return count++;
          };
          $tb.html(Mustache.render(template, response));
        } else {
          swal("Oops!", "No se a podido traer los datos, intentelo en unos minutos. " + response.raise, "error");
        }
      });
    };

    Post.prototype.savePost = function() {
      var context;
      if (this.title === void 0 || this.title === "") {
        swal("Alerta!", "El Título no debe de estar vacio.", "warning");
        return false;
      }
      if (this.content === void 0 || this.content === "") {
        swal("Alerta!", "El contenido de post no debe estar vacio.", "warning");
        return false;
      }
      if (this.posts_id === void 0 || this.posts_id === "") {
        if (this.image === void 0 || this.image.length === 0) {
          swal("Alerta!", "Ingrese una imagen.", "warning");
          return false;
        }
      }
      if (this.title === void 0 || this.title === "") {
        swal("Alerta!", "El Título no debe de estar vacio.", "warning");
        return false;
      }
      if (this.sgroup === void 0 || this.sgroup === "") {
        swal("Alerta!", "Al parecer no estas asignado a un grupo.", "warning");
        return false;
      }
      if (this.section === void 0 || this.section === "") {
        swal("Alerta!", "No tienes una sección para publicar.", "warning");
        return false;
      }
      context = new FormData();
      if (this.posts_id !== void 0 || this.posts_id !== "") {
        context.append("posts_id", this.posts_id);
        context.append("editPost", true);
      } else {
        context.append("savePost", true);
      }
      context.append("title", this.title);
      if (typeof this.image !== "undefined") {
        context.append("image", this.image);
      }
      context.append("content", this.content);
      context.append("references", this.references);
      context.append("date", this.date);
      context.append("hour", this.hour);
      context.append("sgroup", this.sgroup);
      context.append("section", this.section);
      context.append("category", this.category);
      $.ajax({
        url: uri + "wservices/post",
        data: context,
        type: "post",
        dataType: "json",
        contentType: false,
        processData: false,
        cache: false,
        success: function(response) {
          if (response.status) {
            swal({
              title: "Felicidades!",
              text: "Se público correctamente!",
              type: "success",
              showButtonConfirm: false,
              timer: 2600
            });
            setTimeout(function() {
              return location.reload();
            }, 2600);
          } else {
            swal("Error", "No se a podido guardar la publicación, por favor intenté en unos minutos.", "warning");
          }
        }
      });
    };

    return Post;

  })();
  return Post;
});
