define ["jquery", "swal", "nail"], ($, sw) ->
  class Profile

    constructor: (options = {}) ->
      {@email, @firstname, @lastname, @flag, @slogan, @photo, @real, @fictional, @sex, @range} = options

    $base = $("[name=base]").val()

    this::saveProfileBasic = ->
      context =
        email: this.email
        firstname: this.firstname
        lastname: this.lastname
        lifer: this.real
        lifef: this.fictional
        sex: this.sex
      if context.email is undefined or context.email is ""
        console.error "data invalid email empty!"
        return false
      $.post "#{$base}/saveprofile", context, (response) ->
        if response.status
          sw
            title: "Felicidades!",
            text: "se ha guardado correctamente los datos del perfil."
            timer: 3000
            type: "success"
            showButtonConfirm: false
          return
        else
          sw
            title: 'Oops!'
            text: "No hemos podido almacenar tus datos, por favor vuelva a intentarlo."
            timer: 3000
            type: "error"
            showButtonConfirm: false
          return
      , "json"
      return

    this::uploadTmp = (imgLoad) ->
      if this.photo is undefined or this.photo.length <= 0
        sw 'Alerta carga de imagen', 'No se a seleccionado una imagen valida!'
        return false
      console.info this
      form = new FormData
      form.append "email", this.email
      form.append "photo", this.photo
      form.append "temp", true
      # console.warn form
      $.ajax
        url: "#{$base}/img/tmp"
        data: form
        type: "POST"
        dataType: "json"
        cache: false
        processData: false
        contentType: false
        success: (response) ->
          console.log response
          if response.status
            $(".photo-loagind").hide()
            $(".#{imgLoad}").attr "src", "#{$base}../../../app/#{response.filename}"
            $(".#{imgLoad}").attr "data-src", "#{response.filename}"
            $(".#{imgLoad}").attr "data-range", "0.2"
            $(".#{imgLoad}").css "background-repeat", "no-repeat"
            #$(".#{imgLoad}").css "background-position", "50%"
            $(".#{imgLoad}").nailthumb
              width: 200
              height: 200
              proportions: 0.2
              method: 'crop'
              fitDirection: 'bottom center'
            $(".imgsrc, .slider").show()
            return
          else
            sw "Oops!!", "#{response.raise}", response.type
            return
      return

    this::saveAccount = ->
      if this.email is undefined
        return false
      context =
        email: this.email
        photo: this.photo
        slogan: this.slogan
        range: this.range
      if context.range is undefined or context.range is "" or context.range is null
        context.range = 0.2
      $.post "#{$base}/save/account", context, (response) ->
        if response.status
          sw
            title: "Felicidades!",
            text: "se ha guardado correctamente los datos del perfil."
            timer: 3000
            type: "success"
            showButtonConfirm: false
          location.reload()
          return
        else
          sw
            title: 'Oops!'
            text: "No hemos podido almacenar tus datos, por favor vuelva a intentarlo. #{response.raise}"
            timer: 3000
            type: "error"
            showButtonConfirm: false
          return
      , "json"
      return
  console.log "module load Profile"
  return Profile