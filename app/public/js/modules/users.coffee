define ['jquery', 'swal'], ->
  class Users
    constructor: (options = {}) ->
      {@email, @username, @password, @passwdold, @active, @csrf, @publish, @section, @comment} = options

    $base = $("[name=base]").val()
    $uri = $("[name=uri]").val()

    this::changePasswd = ->
      context =
        email: this.email
        passwd: this.password
        old: this.passwdold

      if context.email is undefined or context.email is null or context.email is ""
        return false
      $.post "#{$base}/change/passwd", context, (response) ->
        if response.status
          swal "Felicidades!","se ha cambiado correctamente la contraseña.", "success"
          return
        else
          swal "Oops!","No se ha cambiado la contraseña. #{response.raise}", "error"
          return
      , "json"
      return

    this::requestChange = ->
      context =
        email: this.email
      if context.email is undefined or context.email is null or context.email is ""
        return false
      $.post "#{$uri}register/restore", context, (response) ->
        if response.status
          swal "Alerta!", "Se a enviado un correo a la dirección establecida. desde ahí puede restablecer su contraseña.", "info"
          return
        else
          swal "Oops!", "No se a registrado la solicitud para restaurar la Contraseña.", "error"
          return
      , "json"
      return
    this::restorePwd = ->
      context =
        email: this.email
        password: this.password
        csrf: this.csrf
      $.post "#{$uri}restore/pwd", context, (response) ->
        if response.status
          location.href = "#{$uri}login"
          return
        else
          swal
            title: "Opps!"
            text: "Error al cambiar la Contraseña"
            timer: 3000
            type: "error"
            showButtonCornfirm: false
          return
      , "json"
      return
    this::sendPublish = ->
      context =
        email: this.email
        section: this.section
        comment: this.comment
      $.post "#{$base}/send/publish", context, (response) ->
        if response.status
          swal
            title: "Felicidades!"
            text: "se a enviado la solicitud, despues de la evalución se le enviara un correo de confirmación."
            timer: 3000
            type: "success"
            showButtonCornfirm: false
            setTimeout ->
              location.reload()
              return
            , 2600
          return
        else
          swal
            title: "Opps!"
            text: "Error enviar la solicitud de publicación"
            timer: 3000
            type: "error"
            showButtonCornfirm: false
      , "json"
      return
  return Users