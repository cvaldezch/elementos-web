uri = document.getElementsByName("uri")[0].value
require.config
  baseUrl: "#{uri}app/public/"
  removeCombined: true
  paths:
    "jquery": "vendor/jquery/dist/jquery.min"
    "bootstrap": "vendor/bootstrap/dist/js/bootstrap.min"
    "formvalidate": "vendor/jquery-form-validator/form-validator/jquery.form-validator.min"
    "swal": "vendor/sweetalert/dist/sweetalert.min"
    "nail": "static/nailthumb/jquery.nailthumb.1.1"
    "ui": "vendor/jquery-ui/jquery-ui.min"
    "mustache": "vendor/mustache/mustache.min"
    "clock": "vendor/clockpicker/dist/bootstrap-clockpicker.min"
    "tiny": "vendor/tinymce/tinymce.min"
    "chosen": "vendor/chosen/chosen.jquery"
    "profile": "js/modules/profile"
    "users": "js/modules/users"
    "post": "js/modules/post"
  shim:
    # jquery:
    #   exports: '$'
    bootstrap:
      deps: ['jquery']
    formvalidate:
      deps: ['jquery']
    mustache:
      deps: ['jquery']
    clock:
      deps: ['jquery']
    nail:
      deps: ['jquery']
    ui:
      deps: ['jquery']
    tiny:
      deps: ['jquery']
    chosen:
      deps: ['jquery']
    profile: ['jquery', 'swal']
    users: ['jquery', 'swal']
    post: ['jquery', 'swal', 'mustache']

requirejs ['jquery', 'formvalidate', 'bootstrap', 'swal', "nail", "ui", "clock", "mustache", "tiny", "chosen"], ($) ->
  $ ->
    $(".body-list, [name=imgpost]").fadeOut()
    $.validate
      form: "#formprofile"
      onSuccess: ->
        save_profile()
        return false
    $.validate
      form: "#formAccount"
      onSuccess: ->
        saveAccount()
        return false
    $.validate
      form: "#formpasswd"
      modules: 'security'
      onSuccess: ->
        changePassword()
        console.warn "form passwd"
        return false
    $("#imgprofile").on "change", uploadTemp
    $(".btn-sendrequest").on "click", sendPublish
    $(".slider").slider
      value: 0.2,
      min: 0,
      max: 1,
      step: 0.1,
      slide: ( event, ui ) ->
        # console.log ui.value
        $(".imgsrc").nailthumb
          width: 200
          height: 200
          proportions: ui.value
          fitDirection: 'center center'
        .attr "data-range", ui.value
        return
    initialize()
    $("[name=recover]").on "click", requestChange
    $(".cimage").on "click", ->
      $("[name=image]").click()
      return
    tinymce.init
      selector:'[name=content]'
      plugins: 'link image'
      fontsize_formats: "8pt 10pt 12pt 14pt 16pt 18pt 24pt 36pt"
      toolbar: "insertfile undo redo | pastetext | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | fontselect | fontsizeselect | link image"
      height: 300
    $("[name=date]").datepicker
      showAnim: "slide"
      dateFormat: "yy-mm-dd"
      minDate: "0"
    $(".clockpick").clockpicker
      autoclose: true
      donetext: 'Hecho'
    $("[name=categories]").chosen
      width: "100%"
    setTimeout ->
      $(".chosen-choices").addClass 'form-control'
    , 800
    $(".btn-publish").on "click", savePost
    $(".btnListPublish").on "click", listPosts
    $(".btnaddPublish").on "click", addPublish
    $(document).on "click", ".editPost", showEditPost
    return
  save_profile = (event) ->
    require ['profile'], (Profile) ->
      pro = new Profile
        email: $("[name=email]").val()
        firstname: $("[name=firstname]").val()
        lastname: $("[name=lastname]").val()
        real: $("[name=real]").val()
        fictional: $("[name=fictional]").val()
        sex: $("[name=sex]").val()
      pro.saveProfileBasic()
      return
    return
  return

initialize = ->
  $(".photo-loagind, .slider").hide()
  $(".photo-profile").nailthumb
    height: 200
    width: 250
    method: "crop"
    proportions: $(".imgsrc").attr "data-range"
    fitDirection: 'center center'
  if $(".imgsrc").attr("src") isnt ""
    $(".imgsrc").show()
    .nailthumb
      height: 200
      width: 200
      proportions: $(".imgsrc").attr "data-range"
      fitDirection: 'center center'
    return
  else
    $(".imgsrc").hide()
    return

uploadTemp = (event) ->
  img = this
  console.warn img.files
  if img.files.length > 0
    $(".photo-loagind").show()
    require ['profile'], (Profile) ->
      profile = new Profile
        photo: img.files[0]
        email: $("[name=email]").val()
        $(".spinner").css "display", "block"
      tmp = profile.uploadTmp "imgsrc"
      return
  return

saveAccount = (event) ->
  require ['profile'], (Profile) ->
    pro = new Profile
      email: $("[name=email]").val()
      slogan: $("[name=slogan]").val()
      range: $(".imgsrc").attr "data-range"
      photo: $(".imgsrc").attr "data-src"
    pro.saveAccount()
    return
  return

changePassword = (event) ->
  require ['users'], (Users) ->
    user = new Users
      email: $("[name=email]").val()
      password: $("[name=passwd_new]").val()
      passwdold: $("[name=passwd_current]").val()
    user.changePasswd()
    return
  return

requestChange = (event) ->
  console.log "request change pwd"
  require ['users'], (Users) ->
    user = new Users
      email: $("[name=email]").val()
    user.requestChange()
    return
  return

sendPublish = (event) ->
  require ['users'], (Users) ->
    user = new Users
      email: $("[name=email]").val()
      section: $("[name=section]").val()
      comment: $("[name=comment]").val()
    user.sendPublish()
    return
  return

savePost = (event) ->
  require ['post'], (Post) ->
    references = ""
    content = $("#content_ifr").contents().find("body").text().split " "
    for i in [0..40] by 1
      if typeof content[i] is 'undefined' or content[i] is null
        continue
      else
        references += content[i] + " "
    post = new Post
      posts_id: if $("input[name=pedit]").length then $("input[name=pedit]").val() else undefined
      title: $("[name=title]").val()
      content: $("#content_ifr").contents().find("body").html()
      references: references
      image: $("[name=image]").get(0).files[0]
      date: $("[name=date]").val()
      hour: $("[name=hour]").val()
      sgroup: $("[name=section]").find(":selected").attr "data-group"
      section: $("[name=section]").val()
      category: $("[name=categories]").val()
    post.savePost()
    return

addPublish = ->
  $("[name=title]").val ""
  $("#content_ifr").contents().find("body").html ""
  $("[name=imgpost]").attr "src", ""
  $("[name=date]").val ""
  $("[name=hour]").val ""
  $("[name=categories]").val []
  $(".body-list, [name=imgpost]").fadeOut()
  $(".body-add").fadeIn()
    .find "input[name=pedit]"
    .remove()
  return

listPosts = ->
  console.log "here"
  require ['post'], (Post) ->
    post = new Post()
    post.listPosts()
    $(".body-add").fadeOut()
    $(".body-list").fadeIn()
  return

showEditPost = ->
  post = this
  $("[name=title]").val post.getAttribute "data-title"
  $("#content_ifr").contents().find("body").html post.getAttribute "data-content"
  $("[name=imgpost]").attr "src", "#{uri}app/#{post.getAttribute "data-image"}"
  $("[name=date]").val post.getAttribute("data-publish").substring 0, 10
  $("[name=hour]").val post.getAttribute("data-publish").substring 11
  # $("[name=section]").find(":selected").attr "data-group"
  console.log post.getAttribute("data-category").split ","
  $("[name=categories]").val post.getAttribute("data-category").split ","
  $("[name=categories]").trigger "chosen:updated"
  $(".body-list").fadeOut()
  $(".body-add, [name=imgpost]").fadeIn()
  $(".body-add").append "<input type='hidden' name='pedit' value='#{post.getAttribute "data-id"}'>"
  return