var addPublish, changePassword, initialize, listPosts, requestChange, saveAccount, savePost, sendPublish, showEditPost, uploadTemp, uri;

uri = document.getElementsByName("uri")[0].value;

require.config({
  baseUrl: uri + "app/public/",
  removeCombined: true,
  paths: {
    "jquery": "vendor/jquery/dist/jquery.min",
    "bootstrap": "vendor/bootstrap/dist/js/bootstrap.min",
    "formvalidate": "vendor/jquery-form-validator/form-validator/jquery.form-validator.min",
    "swal": "vendor/sweetalert/dist/sweetalert.min",
    "nail": "static/nailthumb/jquery.nailthumb.1.1",
    "ui": "vendor/jquery-ui/jquery-ui.min",
    "mustache": "vendor/mustache/mustache.min",
    "clock": "vendor/clockpicker/dist/bootstrap-clockpicker.min",
    "tiny": "vendor/tinymce/tinymce.min",
    "chosen": "vendor/chosen/chosen.jquery",
    "profile": "js/modules/profile",
    "users": "js/modules/users",
    "post": "js/modules/post"
  },
  shim: {
    bootstrap: {
      deps: ['jquery']
    },
    formvalidate: {
      deps: ['jquery']
    },
    mustache: {
      deps: ['jquery']
    },
    clock: {
      deps: ['jquery']
    },
    nail: {
      deps: ['jquery']
    },
    ui: {
      deps: ['jquery']
    },
    tiny: {
      deps: ['jquery']
    },
    chosen: {
      deps: ['jquery']
    },
    profile: ['jquery', 'swal'],
    users: ['jquery', 'swal'],
    post: ['jquery', 'swal', 'mustache']
  }
});

requirejs(['jquery', 'formvalidate', 'bootstrap', 'swal', "nail", "ui", "clock", "mustache", "tiny", "chosen"], function($) {
  var save_profile;
  $(function() {
    $(".body-list, [name=imgpost]").fadeOut();
    $.validate({
      form: "#formprofile",
      onSuccess: function() {
        save_profile();
        return false;
      }
    });
    $.validate({
      form: "#formAccount",
      onSuccess: function() {
        saveAccount();
        return false;
      }
    });
    $.validate({
      form: "#formpasswd",
      modules: 'security',
      onSuccess: function() {
        changePassword();
        console.warn("form passwd");
        return false;
      }
    });
    $("#imgprofile").on("change", uploadTemp);
    $(".btn-sendrequest").on("click", sendPublish);
    $(".slider").slider({
      value: 0.2,
      min: 0,
      max: 1,
      step: 0.1,
      slide: function(event, ui) {
        $(".imgsrc").nailthumb({
          width: 200,
          height: 200,
          proportions: ui.value,
          fitDirection: 'center center'
        }).attr("data-range", ui.value);
      }
    });
    initialize();
    $("[name=recover]").on("click", requestChange);
    $(".cimage").on("click", function() {
      $("[name=image]").click();
    });
    tinymce.init({
      selector: '[name=content]',
      plugins: 'link image',
      fontsize_formats: "8pt 10pt 12pt 14pt 16pt 18pt 24pt 36pt",
      toolbar: "insertfile undo redo | pastetext | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | fontselect | fontsizeselect | link image",
      height: 300
    });
    $("[name=date]").datepicker({
      showAnim: "slide",
      dateFormat: "yy-mm-dd",
      minDate: "0"
    });
    $(".clockpick").clockpicker({
      autoclose: true,
      donetext: 'Hecho'
    });
    $("[name=categories]").chosen({
      width: "100%"
    });
    setTimeout(function() {
      return $(".chosen-choices").addClass('form-control');
    }, 800);
    $(".btn-publish").on("click", savePost);
    $(".btnListPublish").on("click", listPosts);
    $(".btnaddPublish").on("click", addPublish);
    $(document).on("click", ".editPost", showEditPost);
  });
  save_profile = function(event) {
    require(['profile'], function(Profile) {
      var pro;
      pro = new Profile({
        email: $("[name=email]").val(),
        firstname: $("[name=firstname]").val(),
        lastname: $("[name=lastname]").val(),
        real: $("[name=real]").val(),
        fictional: $("[name=fictional]").val(),
        sex: $("[name=sex]").val()
      });
      pro.saveProfileBasic();
    });
  };
});

initialize = function() {
  $(".photo-loagind, .slider").hide();
  $(".photo-profile").nailthumb({
    height: 200,
    width: 250,
    method: "crop",
    proportions: $(".imgsrc").attr("data-range"),
    fitDirection: 'center center'
  });
  if ($(".imgsrc").attr("src") !== "") {
    $(".imgsrc").show().nailthumb({
      height: 200,
      width: 200,
      proportions: $(".imgsrc").attr("data-range"),
      fitDirection: 'center center'
    });
  } else {
    $(".imgsrc").hide();
  }
};

uploadTemp = function(event) {
  var img;
  img = this;
  console.warn(img.files);
  if (img.files.length > 0) {
    $(".photo-loagind").show();
    require(['profile'], function(Profile) {
      var profile, tmp;
      profile = new Profile({
        photo: img.files[0],
        email: $("[name=email]").val()
      }, $(".spinner").css("display", "block"));
      tmp = profile.uploadTmp("imgsrc");
    });
  }
};

saveAccount = function(event) {
  require(['profile'], function(Profile) {
    var pro;
    pro = new Profile({
      email: $("[name=email]").val(),
      slogan: $("[name=slogan]").val(),
      range: $(".imgsrc").attr("data-range"),
      photo: $(".imgsrc").attr("data-src")
    });
    pro.saveAccount();
  });
};

changePassword = function(event) {
  require(['users'], function(Users) {
    var user;
    user = new Users({
      email: $("[name=email]").val(),
      password: $("[name=passwd_new]").val(),
      passwdold: $("[name=passwd_current]").val()
    });
    user.changePasswd();
  });
};

requestChange = function(event) {
  console.log("request change pwd");
  require(['users'], function(Users) {
    var user;
    user = new Users({
      email: $("[name=email]").val()
    });
    user.requestChange();
  });
};

sendPublish = function(event) {
  require(['users'], function(Users) {
    var user;
    user = new Users({
      email: $("[name=email]").val(),
      section: $("[name=section]").val(),
      comment: $("[name=comment]").val()
    });
    user.sendPublish();
  });
};

savePost = function(event) {
  return require(['post'], function(Post) {
    var content, i, j, post, references;
    references = "";
    content = $("#content_ifr").contents().find("body").text().split(" ");
    for (i = j = 0; j <= 40; i = j += 1) {
      if (typeof content[i] === 'undefined' || content[i] === null) {
        continue;
      } else {
        references += content[i] + " ";
      }
    }
    post = new Post({
      posts_id: $("input[name=pedit]").length ? $("input[name=pedit]").val() : void 0,
      title: $("[name=title]").val(),
      content: $("#content_ifr").contents().find("body").html(),
      references: references,
      image: $("[name=image]").get(0).files[0],
      date: $("[name=date]").val(),
      hour: $("[name=hour]").val(),
      sgroup: $("[name=section]").find(":selected").attr("data-group"),
      section: $("[name=section]").val(),
      category: $("[name=categories]").val()
    });
    post.savePost();
  });
};

addPublish = function() {
  $("[name=title]").val("");
  $("#content_ifr").contents().find("body").html("");
  $("[name=imgpost]").attr("src", "");
  $("[name=date]").val("");
  $("[name=hour]").val("");
  $("[name=categories]").val([]);
  $(".body-list, [name=imgpost]").fadeOut();
  $(".body-add").fadeIn().find("input[name=pedit]").remove();
};

listPosts = function() {
  console.log("here");
  require(['post'], function(Post) {
    var post;
    post = new Post();
    post.listPosts();
    $(".body-add").fadeOut();
    return $(".body-list").fadeIn();
  });
};

showEditPost = function() {
  var post;
  post = this;
  $("[name=title]").val(post.getAttribute("data-title"));
  $("#content_ifr").contents().find("body").html(post.getAttribute("data-content"));
  $("[name=imgpost]").attr("src", uri + "app/" + (post.getAttribute("data-image")));
  $("[name=date]").val(post.getAttribute("data-publish").substring(0, 10));
  $("[name=hour]").val(post.getAttribute("data-publish").substring(11));
  console.log(post.getAttribute("data-category").split(","));
  $("[name=categories]").val(post.getAttribute("data-category").split(","));
  $("[name=categories]").trigger("chosen:updated");
  $(".body-list").fadeOut();
  $(".body-add, [name=imgpost]").fadeIn();
  $(".body-add").append("<input type='hidden' name='pedit' value='" + (post.getAttribute("data-id")) + "'>");
};
