$(document).ready(function() {
  $("*").bind("cut copy paste", function(e){
    e.preventDefault();
  });
  var suscribe;
  $(window).scroll(function() {
    if ($(this).scrollTop() > 50) {
      $('.social-links').fadeOut();
    } else {
      $('.social-links').fadeIn();
    }
  });
  suscribe = true;
  $(".imgpublish").click(function() {
    location.href = this.getAttribute("data-src");
  });
  $(".post-content").click(function() {
    location.href = this.getAttribute("data-ref");
  }).css("cursor", "pointer");
  $('.content-suscribe').hide();
  if (window.screen.width < 768) {
    $("footer").css("padding-bottom", "0px");
    $("#search-desktop").remove();
  } else {
    $("#search-mobile").remove();
  }
  $("[name=btnSuscribe]").on("click", function() {
    var $email;
    $email = $("[name=emails]").val();
    if (typeof $email === "undefined" || $email.indexOf("@") === -1) {
      swal("Oops!", "Correo no valido", "warning");
      return false;
    } else {
      $.post(($("[name=uri]").val()) + "wservices", {
        suscribe: true,
        email: $email
      }, function(response) {
        if (response.status) {
          swal({
            title: "Felicidades",
            text: "Gracias por suscribirte.",
            type: "success",
            showButtonConfirm: false,
            timer: 1600
          });
          $('.content-suscribe').fadeOut(800);
        }
      });
    }
  });
  $(".suscribe-foot a").on("click", function() {
    $('.content-suscribe').fadeOut(800);
  });
  $("[name=emails]").on("focus", function() {
    suscribe = false;
  });
});
