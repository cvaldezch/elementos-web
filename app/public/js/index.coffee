# $.getJSON "https://api.facebook.com/method/links.getStats?urls=https://facebook.com/elementospe&format=json",
#   (response) ->
#     $(".facebook-so small").html response[0].like_count
#     console.log response[0].like_count
#     return
# $.ajax
#     url: 'https://api.instagram.com/v1/users/1528948135'
#     dataType: 'jsonp'
#     type: 'GET'
#     data: client_id: 'de21c27ed73640d8a68b5226e292b06a'
#     success: (data) ->
#       $(".instagram-so small").html data.data.counts.followed_by
#       console.log data.data.counts.followed_by
#       return
#     error: (data) ->
#       console.log data
#       return
# $.ajax
#   url: "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20html%20where%20url%3D%27http%3A%2F%2Fgettwitterid.com%2F%3Fuser_name%3Delementospe%26submit%3DGET%2BUSER%2BID%27&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys"
#   dataType: "jsonp"
#   success: (response) ->
#     tweet = response.query.results.body.div.div[0].table.tbody.tr[3].td[1].p
#     $(".twitter-so small").html tweet.replace ",", ""
#     console.log tweet.replace ",", ""
#     return
$(document).ready ->
  $(window).scroll ->
    # console.log $(this).scrollTop()
    if $(this).scrollTop() > 50
      $('.social-links').fadeOut()
      return
    else
      $('.social-links').fadeIn()
      return
  suscribe = true
  # get count like facebook
  $(".imgpublish").click ->
    location.href = this.getAttribute "data-src"
    return
  $(".post-content").click ->
    location.href = this.getAttribute "data-ref"
    return
  .css "cursor", "pointer"
  $('.content-suscribe').hide()
  # setTimeout ->
  #   if suscribe
  #     $('.content-suscribe').fadeOut 800
  #   return
  # , 5000
  if window.screen.width < 768
    $("footer").css "padding-bottom", "0px"
    $("#search-desktop").remove()
  else
    $("#search-mobile").remove()
  # $(".twitter-so").on "click", ->
  #   window.open "//twitter.com/elementospe",'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600'
  # $(".instagram-so").on "click", ->
  #   window.open "//instagram.com/elementos_pe",'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600'
  # $(".facebook-so").on "click", ->
  #   window.open "//facebook.com/elementospe",'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600'
  $("[name=btnSuscribe]").on "click", ->
    $email = $("[name=emails]").val()
    if typeof($email) is "undefined" or $email.indexOf("@") is -1
      swal "Oops!", "Correo no valido", "warning"
      return false
    else
      $.post "#{$("[name=uri]").val()}wservices",
        suscribe: true
        email: $email
      , (response) ->
        if response.status
          swal
            title: "Felicidades"
            text: "Gracias por suscribirte."
            type: "success"
            showButtonConfirm: false
            timer: 1600
          $('.content-suscribe').fadeOut 800
          return
      return
  $(".suscribe-foot a").on "click", ->
    $('.content-suscribe').fadeOut 800
    return
  $("[name=emails]").on "focus", ->
    suscribe = false
    return
  return