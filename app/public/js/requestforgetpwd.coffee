app = angular.module 'reqFP', []

app.controller 'reqFPCtrl', ($scope, $http) ->
    $scope.vemail = true
    $scope.baseURI = location.host
    $scope.name = ""
    $scope.psend = false
    $scope.pfail = false
    $scope.pp = false
    angular.element(document).ready ->
        # $scope.vemail = false
        $scope.name = angular.element("#name")[0].value
        console.log "here init log"
        return

    $scope.validEmail = ($event) ->
        console.log $event
        data =
            email: $event.currentTarget.value
            validEmail: true
        $http.get "/user/register", params: data
        .success (response) ->
            if response.status
                $scope.vemail = false
            else
                $scope.vemail = true
                console.log 'emial not exists'
                return
        return

    $scope.keyMail = ($event) ->
        patterns = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/
        console.log $event.currentTarget.value
        if patterns.test $event.currentTarget.value
            data =
                email: $event.currentTarget.value
                validEmail: true
            console.log data
            $http.get "/user/register", params: data
            .success (response) ->
                if response.status
                    $scope.vemail = false
                else
                    console.log 'emial not exists'
                    $scope.vemail = true
                    return
        return

    $scope.sendEmail = ($event) ->
        console.log "log en send mail"
        # send and make csrf token
        data =
            email: $scope.mail
            registerRec: true        
        $http
            data: $.param data
            url: '/wservices/pwd/send/recover'
            method: 'post'
            headers:
                'Content-Type': 'application/x-www-form-urlencoded'
        .success (response) ->
            if response.status
                html = """<html lang="es">
                            <head>
                              <meta charset="utf-8">
                              <title>#{$scope.name}</title>
                            </head>
                            <body>
                            <!--<div style="text-align: center;">
                                <a href="#{location.origin}" title="Elements">
                                  <img style="height: 95px;  width: 86px;" src="#{location.host}/app/public/images/#{$scope.name}.png" alt="Elementos">
                                </a>
                            </div>-->
                            <h3>Solicitud cambio de contraseña</h3>
                            <p>
                              Se ha solicitado el cambiado de contraseña recientemente para su cuenta #{response.mail}.
                            </p>
                            <p>
                              Si has solicitado cambiar tú contraseña,  deberás restablecer desde el siguiente formulario.
                            </p>
                            <p>
                              <a href="#{location.origin}/request/change/passwd/#{response.csrf}/#{$scope.mail}">Recuperar Contraseña</a>
                            </p>
                            <p>
                              <small style="text-align:center;">
                                Los signos ortográficos fueron omitidos intencionalmente en este correo electrónico.<br/>
                                      Estás suscrito con la siguiente dirección: #{$scope.mail} Por favor, no respondas a este correo electrónico.
                              </small>
                            </p>
                            </body>
                            </html>"""
                prm =
                    name: $scope.name
                    email: response.mail
                    from : "noreply@#{location.hostname}"
                    subject: "Recupera tu clave de #{$scope.name}"
                    body: html
                console.log prm
                $.ajax
                    url: "http://188.166.105.23:3000"
                    data: prm
                    type: 'GET'
                    crossDomain: true
                    dataType: "text"
                    success: (result) ->
                        result = String result
                        result = JSON.parse result
                        if result.status
                            # location.href = "/register/send"
                            # show message of send mail
                            $scope.psend = true
                            $scope.pp = true
                            $scope.psend = true
                            $scope.pp = true
                            return
                        else
                            # show panel not send mail
                            # location.href = "/register/error"
                            $scope.pfail = true
                            $scope.pp = true
                            $scope.pfail = true
                            $scope.pp = true
                            return
                return
            else
                console.log "error al registar recuperacion de password!."
        
        return
    return
