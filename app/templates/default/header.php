<?php

  header('Content-type: text/html; charset=utf-8');
  header("Access-Control-Allow-Origin: *");

  use Helpers\Assets;
  use Helpers\Url;
  use Helpers\Session;

  $showm = true;
?>
<!DOCTYPE html>
<html lang="<?php echo LANGUAGE_CODE; ?>">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
  <!-- Site meta -->
  <meta charset="utf-8">
  <title><?php echo $data['title']; ?></title>
  <meta name="keywords" content="HTML, CSS, XML, XHTML, JavaScript">
  <meta name="Description" CONTENT="http://elementos.com.pe: Elementos: Disculpen las molestias, esto es una Revolución.">

  <meta name="google-site-verification" content="GFEaVF2MmUCkgHYEGE8IBwt-PpYttt03yd1CZimzJTc" />

  <!-- Favicons -->
  <link rel="shortcut icon" href="<?php echo Url::templatePath(); ?>images/favicon.ico">
  <!-- block google -->
  <link rel="canonical" href="<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>">
  <link rel="publisher" href="https://plus.google.com/+elementospeoficial">
  <meta name="google-site-verification" content="1yDqxcgSl33YlvdjViW4lBssvhrCbcFEyRjCQQeMohg" />
  <!-- block -->
  <!-- block facebook -->
  <meta property="fb:app_id" content="485204271650665">
  <meta property="og:site_name" content="elementos"/>
  <meta property="og:locale" content="es_ES"/>
  <meta property="og:type" content="article"/>
  <meta property='og:title' content='<?php echo strval($data["title"]); ?>'/>
  <meta property='og:url' content="<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>">
  <meta property='og:description' content='<?php echo $data["post"][0]->reference; ?>'/>
  <meta property="og:image" content="<?php echo DIR .'app/'. str_replace(" ", "%20", $data['post'][0]->image); ?>" />
  <meta property="og:image:height" content="600" />
  <meta property="og:image:width" content="600" />
  <meta property="article:publisher" content="https://www.facebook.com/elementospe" />
  <meta name="robots" content="noodp,noydir"/>
  <meta name="robots" content="NOODP">
  <meta name="googlebot" content="NOODP">
  <!-- plugin twitter -->
  <link rel="me" href="<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>">
  <meta name="twitter:card" content="summary_large_image">
  <meta name='twitter:description' content='<?php echo $data["post"][0]->reference; ?>'/>
  <meta name="twitter:title" content='<?php echo strval($data["title"]); ?>'/>
  <meta name="twitter:site" content="@elementospe"/>
  <meta name="twitter:domain" content="elementos"/>
  <meta name="twitter:image:src" content="<?php echo DIR .'app/'. str_replace(" ", "%20", $data['post'][0]->image); ?>">
  <meta name="twitter:creator" content="@elementospe"/>

  <!-- Mobile -->
  <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
  <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
  <script>
    (adsbygoogle = window.adsbygoogle || []).push({
      google_ad_client: "ca-pub-8134194566813718",
      enable_page_level_ads: true
    });
  </script>
  <!-- CSS -->
  <?php
  Assets::css(array(
    Url::pathPublic('vendor') . 'font-awesome/css/font-awesome.min.css',
    Url::pathPublic('vendor') . 'bootstrap/dist/css/bootstrap.min.css',
    Url::pathPublic('css') . 'style.css',
  ));
  ?>
  <!--[if lt IE 8]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
</head>
<body>
<!-- Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-63932348-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- JS -->
<?php
Assets::js(array(
  Url::pathPublic('vendor') . 'jquery/dist/jquery.min.js',
  Url::pathPublic('vendor') . 'bootstrap/dist/js/bootstrap.min.js',
  Url::pathPublic('static') . 'nailthumb/jquery.nailthumb.1.1.min.js',
  Url::pathPublic('vendor') . 'jquery.easing/js/jquery.easing.min.js',
  Url::pathPublic('js') . 'index.js',
));
?>

<!-- Facebook -->
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '486062034898222',
      xfbml      : true,
      version    : 'v2.4'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>
<!-- end  -->
<script>
  $(function  () {
    $(".thumbnail-profile").nailthumb({
      width: 48,
      height: 48,
      method: "crop",
      //proportions: parseFloat($(".thumbnail-profile").attr("data-range")),
      fitDirection: 'center center'
    });
  });
</script>
<div class="header">
  <div class="header_wrap hidden-xs">
    <nav class="superheader_wrap">
      <div>
          <?php if (Session::get('loggedin')){ ?>
            <ul class="nav navbar-nav navbar-right">
                <!-- <li><a href="#">Link</a></li> -->
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span class="glyphicon glyphicon-user"></span>
                    <?php
                      if (Session::get('names')) {
                        echo Session::get('names');
                      }else{
                        echo Session::get('username');
                      }
                    ?>
                    <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">
                        <img data-range="<?php echo Session::get('range'); ?>" src="<?php echo DIR . 'app/' . Session::get('thumbnail'); ?>" class="img-thumbnail img-responsive thumbnail-profile">
                        <?php echo Session::get('email'); ?></a></li>
                        <li><a href="<?php echo DIR; ?>users/profile">Configuraciones</a></li>
                        <li><a href="#">Solicitud de Editor</a></li>
                        <?php if (Session::get('role') == 'administrator') { ?>
                          <li class="divider"></li>
                          <li><a href="<?php echo DIR; ?>admin">Administrar</a></li>
                        <?php } ?>
                        <li class="divider"></li>
                        <li><a href="<?php echo DIR; ?>logout/"><i class="glyphicon glyphicon-log-out"></i> Cerrar Sesion</a></li>
                    </ul>
                </li>
            </ul>
          <?php }else{ ?>
        <ul class="registerandlogin">
            <li>
              <a href="<?php echo DIR . 'user/register'; ?>">Registrate</a>
            </li>
            <li>
                <a href="<?php echo DIR; ?>login">Login</a>
            </li>
        </ul>
          <?php } ?>
        <?php if (count($data['socials'])){ ?>
          <ul class="netsocial">
            <?php foreach ($data['socials'] as $row) { ?>
              <li class="<?php echo $row->social; ?>">
                <a class="c<?php echo $row->social; ?>" href="<?php echo $row->uri; ?>" target="_blank">
                  <span class="fa fa-<?php echo $row->icon; ?>"></span>
                </a>
              </li>
            <?php } ?>
          </ul>
        <?php } ?>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </nav>
    <div class="header_wrapper">
      <a href="<?php echo DIR; ?>" class="lredirect">
        <div class="logo_group">
          <!-- <div class="logo img-responsive" title="ELEMEMTOS" >
          </div> -->
          <img src="<?php echo DIR. 'app/public/images/'. $data['config']->sitename .'.svg' ?>" alt="Elementos" class="img-responsive logo">
          <div class="logo_text"><?php echo $data['config']->sitename; ?></div>
        </div>
      </a>
      <?php
          // echo gettype($data);
          if(in_array('showm', array_keys($data)))
          {
            $showm = $data['showm'];
          }
        ?>
      <?php if ($showm == true){ ?>
      <nav class="header_menu">
          <ul>
            <?php
              $menu = new \Models\Admin\Menu();
              $menu = $menu->getMenu();
              $submenu = new \Models\Admin\Submenu();
              $submenu = $submenu->getSubmenu();
              foreach ($menu as $row) { ?>
              <li>
                <a href="<?php echo DIR . 'group/' . $row->url; ?>"><?php echo $row->menu; ?></a>
                <?php if ($row->submenu > 0){ ?>
                  <ul>
                    <?php
                      foreach ($submenu as $srow) {
                        if ($row->menuid == $srow->menuid) {
                    ?>
                      <li><a href="<?php echo DIR . 'group/' . $row->url . '/section/' .  $srow->url; ?>">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                        <?php echo $srow->submenu; ?>
                        </a></li>
                    <?php
                        }
                      }
                    ?>
                  </ul>
                <?php } ?>
              </li>
            <?php } ?>
              <!-- <li>
                  <div class="dropdown">
                      <button class="btn btn-link dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="true">
                          COMUNIDAD
                      </button>
                      <ul class="dropdown-menu" role="menu">
                          <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Buzón del despecho</a></li>
                          <li role="presentation"><a role="menuitem" tabindex="-1" href="#">El Club de los Aburridos</a></li>
                          <li role="presentation"><a role="menuitem" tabindex="-1" href="#">El Club de los Codiciados</a></li>
                          <li role="presentation"><a role="menuitem" tabindex="-1" href="#">El Club de los poetas muertos</a></li>
                      </ul>
                  </div>
              </li> -->
              <!-- <li>

                  <div class="dropdown">
                      <button class="btn btn-link dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="true">
                          CULTURA
                      </button>
                      <ul class="dropdown-menu" role="menu">
                          <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Arte&nbsp;&nbsp;&nbsp;</a></li>
                          <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Cine&nbsp;&nbsp;&nbsp;</a></li>
                          <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Fotografía</a></li>
                          <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Libros</a></li>
                          <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Música</a></li>
                      </ul>
                  </div>
              </li> -->
              <!-- <li>

                <div class="dropdown">
                  <button class="btn btn-link dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="true">
                    LETRAS
                  </button>
                  <ul class="dropdown-menu" role="menu">
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Filosofia</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Literatura</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Poesía</a></li>
                  </ul>
                </div>
              </li>
              <li> -->
                <!-- <a href="#" target="_blank">VIDA</a> -->
                <!-- <div class="dropdown">
                  <button class="btn btn-link dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="true">
                    VIDA
                  </button>
                  <ul class="dropdown-menu" role="menu">
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Destinos</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Gastronomía</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Salud</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Sexualidad</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Sustentabilidad</a></li>
                  </ul>
                </div>
              </li> -->
              <!-- <li>
                <a href="#" target="_blank">NOTICIAS</a>
              </li> -->
             <!--  <li>
                <div class="dropdown">
                  <button class="btn btn-link dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="true">
                    NOSOTROS
                  </button>
                  <ul class="dropdown-menu" role="menu">
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Credo</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Staff</a></li>
                  </ul>
                </div>
              </li>
              <li>
                <a href="#" target="_blank">CONTACTENOS</a>
              </li> -->
          </ul>
          <div class="clear"></div>
      </nav>
      <?php } ?>
    </div>
    <script>
      //$(function () {
      //When mouse rolls over
      $(".header_menu > ul > li").mouseover(function(){
          $(this).find('ul').eq(0).css("display", "block").stop().animate({height: (($(this).find("li").length + 1) * 25) +"px"},{queue:false, duration:600, easing: 'easeOutBounce'});
          $(this).css("background", "rgb(45,45,46)")
      });

      //When mouse is removed
      $(".header_menu > ul > li").mouseout(function(){
          $(this).find('ul').eq(0).css("display", "none").stop().animate({height:"0px"},{queue:false, duration:600, easing: 'easeOutBounce'});
          $(this).css("background", "none")
      });
        // $(".header_menu > ul > li > ul").mouseover(function(){
        //   $(this).css("display", "block");
        // });
        // $(".header_menu > ul > li > ul").mouseover(function(){
        //   $(this).css("display", "none");
        // });
      //});
    </script>
  </div>
<!-- block nav bar -->
<nav class="navbar navbar-default navbar-fixed-top visible-xs">
  <div class="container-fluid">
    <!-- social links -->
    <div class="row social-links">
      <div class="nav_social_mov">
        <ul class="social-header">
        <?php
          if (count($data['socials'])){
          foreach ($data['socials'] as $row) {
            if ($row->mobile) {
        ?>
            <li class="<?php echo $row->social; ?>">
              <a class="c<?php echo $row->social; ?>" href="<?php echo $row->uri; ?>" target="_blank">
                <span class="fa fa-<?php echo $row->icon; ?>"></span>
              </a>
            </li>
          <?php }}} ?>
          <!-- <li class="facebook"><a href="https://www.facebook.com/elementospe" target="_blank">
            <span class="fa fa-facebook"></span>
          </a></li>
          <li class="twitter"><a href="https://twitter.com/elementospe" target="_blank">
            <span class="fa fa-twitter"></span>
          </a></li>
          <li class="plus"><a href="https://plus.google.com/+ElementospeOficial/posts" target="_blank">
            <span class="fa fa-google-plus"></span>
          </a></li>
          <li class="instagram"><a href="https://instagram.com/elementos_pe/" target="_blank">
            <span class="fa fa-instagram"></span>
          </a></li> -->
        </ul>
      </div>
    </div>
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <div class="header-logo">
        <a class="navbar-brand" href="<?php echo DIR; ?>"><img src="<?php echo DIR; ?>app/public/images/<?php echo $data['config']->sitename; ?>.svg" class="logo img-responsive" alt="<?php echo $data['config']->sitename; ?>"> <span><?php echo $data['config']->sitename; ?></span></a>
      </div>
      <!-- block logo -->
      <!-- end block -->
      <div class="header-part">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <div class="content_search" id="search-mobile">
          <form class="form-horizontal" onsubmit="event.preventDefault();">
            <div class="form-group">
              <!-- <label class="col-md-2 control-label" for="search">Buscar</label> -->
              <div class="input-group">
                  <input type="text" class="form-control" placeholder="Buscar" id="search">
                  <span class="input-group-addon">
                    <i class="glyphicon glyphicon-search"></i>
                  </span>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse " id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <!-- <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li> -->
        <?php
           if (empty($menu)){
            $menu = new \Models\Admin\Menu();
            $menu = $menu->getMenu();
           }
           if (empty($submenu)) {
            $submenu = new \Models\Admin\Submenu();
            $submenu = $submenu->getSubmenu();
           }
           foreach ($menu as $row) { ?>
          <li class="dropdown">
            <a href="<?php echo DIR . 'group/' . $row->url; ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $row->menu; ?> <?php if ($row->submenu > 0){ ?> <span class="caret"></span><?php } ?></a>
            <?php if ($row->submenu > 0){ ?>
              <ul class="dropdown-menu">
                <?php
                  foreach ($submenu as $srow) {
                    if ($row->menuid == $srow->menuid) {
                ?>
                  <li><a href="<?php echo DIR .'group/'. $row->url . '/section/' . $srow->url; ?>">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <?php echo $srow->submenu; ?>
                    </a></li>
                <?php
                    }
                  }
                ?>
              </ul>
            <?php } ?>
          </li>
        <?php } ?>

       <!--  <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Separated link</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">One more separated link</a></li>
          </ul>
        </li>

      </ul> -->
      <!-- <form class="navbar-form navbar-left" role="search">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
      </form> -->
      <?php if (Session::get('loggedin')){ ?>
        <ul class="nav navbar-nav navbar-right">
          <!-- <li><a href="#">Link</a></li> -->
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span class="glyphicon glyphicon-user"></span>
            <?php
              if (Session::get('names')) {
                echo Session::get('names');
              }else{
                echo Session::get('username');
              }
            ?>
            <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">
              <img data-range="<?php echo Session::get('range'); ?>" src="<?php echo DIR . 'app/' . Session::get('thumbnail'); ?>" class="img-thumbnail img-responsive thumbnail-profile">
              <?php echo Session::get('email'); ?></a></li>
              <li><a href="<?php echo DIR; ?>users/profile">Configuraciones</a></li>
              <li><a href="#">Solicitud de Editor</a></li>
              <?php if (Session::get('role') == 'administrator') { ?>
                <li class="divider"></li>
                <li><a href="<?php echo DIR; ?>admin">Administrar</a></li>
              <?php } ?>
              <li class="divider"></li>
              <li><a href="<?php echo DIR; ?>logout/"><i class="glyphicon glyphicon-log-out"></i> Cerrar Sesion</a></li>
            </ul>
          </li>
        </ul>
      <?php }else{ ?>
          <!--<li>
            <a href="<?php //echo DIR . 'user/register'; ?>">Registrate</a>
          </li>
          <li>
              <a href="<?php //echo DIR; ?>login">Login</a>
          </li>
        </ul>-->
        <?php } ?>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<!-- end block -->
</div>
<section>
