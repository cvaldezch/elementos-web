    <footer>
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <nav class="nav_social_mov">
              <ul>
                <?php
                  if (count($data['socials'])){
                 foreach ($data['socials'] as $row){ ?>
                  <li class="<?php echo $row->social; ?>">
                    <a href="<?php echo $row->uri; ?>" target="_blank">
                      <span class="fa fa-<?php echo $row->icon; ?>"></span>
                    </a>
                  </li>
                <?php }} ?>
                </ul>
              </nav>
          </div>
        </div>
        <div class="row">
          <div class="col-md-2"></div>
          <div class="col-md-8 qpaddingmob">
            <div class="subfooter_wrapper">
              <div class="row">
                  <div class="col-md-12">
                    <br>
                  </div>
                  <div class="col-md-12 col-lg-12">
                    <div class="col-md-6">
                        <br><br>
                        <a href="<?php echo DIR; ?>" class="lredirect">
                          <h2 class="text-center">
                              <?php echo $data['config']->sitename; ?>
                          </h2>
                        </a>
                    </div>
                    <div class="col-md-5">
                        <br><br>
                        <!-- <p class="text-white">
                            <strong>Director General: </strong> Luis Angel Valencia
                        </p>
                        <p class="text-white">
                            <strong>Director Editorial: </strong> Rosalia Martinez Melgarejo
                        </p> -->
                        <p class="text-white">
                            <strong>Contactenos: </strong> contacto@elementospe.com
                        </p>
                        <!-- <p class="text-white">Trabaja en Elementos</p> -->
                    </div>
                    <div class="col-md-12">
                        <br>
                        <p>
                            <h5>
                                <strong>SUSCRIBETE</strong>
                                <br>
                                <small>Suscríbete para recibir en tu correo todas nuestras novedades y experiencias.</small>
                            </h5>
                            <div class="form-group has-primary">
                                <div class="input-group">
                                  <input type="text" class="form-control input-lg" name="fmail">
                                  <span class="input-group-btn">
                                      <button type="button" class="btn btn-lg btn-warning" name="btnSuscribe">
                                          ENVIAR
                                      </button>
                                  </span>
                                </div>
                            </div>
                            <h5>
                                <strong>SÍGUENOS EN:</strong>
                            </h5>
                            <div class="row">
                                <?php
                                  if (count($data['socials'])) {
                                  foreach ($data['socials'] as $row){ ?>
                                  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 text-center">
                                      <a href="<?php echo $row->uri; ?>" class="text-white text-bold" target="_blank">
                                        <span class="fa fa-<?php echo $row->icon; ?> fa-2x"></span>
                                      </a>
                                  </div>
                                <?php }} ?>
                          </div>
                        </p>
                    </div>
                    <div class="col-md-12">
                         <br>
                         <p class="text-center" style="color: #fff;">
                             Disculpen las molestias, esto es una Revolución. | <small>© 2016 ELEMENTOS. TODOS LOS  DERECHOS RESERVADOS.</small>
                         </p>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <br>
                  </div>
              </div>
            </div>
            
          </div>
        </div>
    </footer>
    <div class="clear"></div>
</body>
</html>