<article class="slider_bar">
  <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <?php
        for ($i=0; $i < count($data['sliders']); $i++) {
          if ($i == 0) {
            echo "<li data-target=\"#carousel-example-generic\" data-slide-to=\"$i\" class=\"active\"></li>";
          }else{
            echo "<li data-target=\"#carousel-example-generic\" data-slide-to=\"$i\"></li>";
          }
        }
      ?>
    </ol>
    <!-- Wrapper for slides -->
    <div class="carousel-inner" data-interval="10000" role="listbox">
      <?php
        $counter = 0;
        foreach ($data['sliders'] as $row){
      ?>
      <div class="item <?php if($counter == 0){ echo "active";} ?>">
        <a href="<?php echo DIR . '' . $row->posts_id . '/' . preg_replace('([^A-Za-z0-9])', '-', $row->title); ?>">
        <img src="<?php echo DIR . 'app/' . $row->image; ?>" class="img-responsive center-block" alt="<?php echo $row->title; ?>" data-holder-rendered="true">
        </a>
        <!-- <div style="background:url(/images/1.jpg) center center;background-size:cover;" class="slider-size"> -->
        <div class="carousel-caption">
          <h3 id="third-slide-label">
            <a class="anchorjs-link text-white" href="<?php echo DIR . '' . $row->posts_id . '/' . preg_replace('([^A-Za-z0-9])', '-', $row->title); ?>">
            <?php echo $row->title; ?>
              <!-- <span class="glyphicon glyphicon-link"></span> -->
            </a>
          </h3>
          <!-- <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p> -->
        </div>
      </div>
      <?php
          $counter++;
        }
      ?>
    </div>
    <!-- Controls -->
    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</article>