<?php

use Helpers\Assets,
    Helpers\Session,
    Helpers\Url;

?>
<!DOCTYPE html>
<html lang="<?php echo LANGUAGE_CODE; ?>">
<head>

    <!-- Site meta -->
    <meta charset="utf-8">
    <title><?php echo $data['title'].' - '.SITETITLE; //SITETITLE defined in app/core/config.php ?></title>
    <link name="author" href="Christian Valdez Chavez (cvaldez)" />
    <meta name="description" content="Administracion Elementos">

    <!-- Favicons -->
    <link rel="shortcut icon" href="<?php echo Url::templatePath(); ?>images/favicon.ico">

    <!-- Mobile -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSS -->
    <?php
    Assets::css(array(
        Url::pathPublic('vendor') . 'bootstrap/dist/css/bootstrap.min.css',
        Url::templatePath() . 'css/style.css',
        Url::pathPublic('vendor') . 'font-awesome/css/font-awesome.min.css'
    ));
    ?>
</head>
<body>
<!-- Analytics -->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-63932348-1', 'auto');
  ga('send', 'pageview');

</script>
<div class="container">
<?php if (Session::get('loggedin')) { ?>
    <!-- block nav bar for admin -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo DIR; ?>">
                    <img src="<?php echo Url::pathPublic('images'); ?>elementos.svg" alt="Brand" height="20" width="20">
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <!-- <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li> -->
                    <!-- <li><a href="#">Link</a></li> -->
                    <!-- <li><a href="<?php echo DIR; ?>admin/users">Usuarios</a></li> -->
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Usuarios <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="<?php echo DIR; ?>admin/users/request">Solicitudes</a></li>
                        <li><a href="<?php echo DIR; ?>admin/users">Lista</a></li>
                      </ul>
                    </li>
                    <li><a href="<?php echo DIR; ?>admin/categories">Categorias</a></li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Menus <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="<?php echo DIR; ?>admin/menu">Menu</a></li>
                        <li><a href="<?php echo DIR; ?>admin/submenu">Submenu</a></li>
                      </ul>
                    </li>
                    <li><a href="<?php echo DIR; ?>admin/posts/">Publicaciones</a></li>
                    <li><a href="<?php echo DIR; ?>admin/roles">Roles</a></li>
                    <li><a href="<?php echo DIR; ?>admin/general">General</a></li>
                </ul>
            <!-- <form class="navbar-form navbar-left" role="search">
            <div class="form-group">
            <input type="text" class="form-control" placeholder="Search">
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
            </form> -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- <li><a href="#">Link</a></li> -->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span class="glyphicon glyphicon-user"></span>
                        <?php if (Session::get('names')) {
                          echo Session::get('names');
                        }else{
                          echo Session::get('username');
                        }?>
                      <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#"><?php echo Session::get('email'); ?></a></li>
                            <li><a href="#"><?php echo Session::get('role'); ?></a></li>
                            <li><a href="<?php echo DIR; ?>users/profile">Configuraciones</a></li>
                            <li class="divider"></li>
                            <li><a href="<?php echo DIR; ?>">Inicio</a></li>
                            <li class="divider"></li>
                            <li><a href="<?php echo DIR; ?>logout/"><i class="glyphicon glyphicon-log-out"></i> Cerrar Sesion</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
<?php } ?>
