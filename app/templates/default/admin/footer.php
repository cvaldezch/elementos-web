</div>
<!-- Variable utils -->
<input type="hidden" value="<?php echo DIR; ?>" id="urlBase">
<!-- JS -->
<?php
\Helpers\Assets::js(array(
    // <!-- Angular JS -->
    \Helpers\Url::templatePath() . 'js/jquery.js',
    \Helpers\Url::pathPublic('vendor') . 'bootstrap/dist/js/bootstrap.min.js',
    // '//ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js',
));
?>
</body>
</html>
