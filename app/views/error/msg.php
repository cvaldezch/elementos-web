<div class="content_wrapper">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.0.1/sweetalert.min.css">
  <br>
  <br>
  <br>
  <br>
  <div class="row">
    <div class="col-md-6 col-md-offset-3">
      <div class="panel panel-danger">
        <div class="panel-body bg-danger">
          <h4><?php $data['title']; ?></h4>
          <strong>
            <?php echo $data['msg']; ?>
          </strong>
          &nbsp;&nbsp;
          <a href="<?php echo DIR; ?>login" class="btn btn btn-sm btn-primary"><span class="fa fa-user"></span> Iniciar Sesion</a>
        </div>
      </div>
    </div>
  </div>
</div>