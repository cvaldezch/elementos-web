<!-- utils css3 -->
<link rel="stylesheet" type="text/css" href="<?php echo \Helpers\Url::pathPublic('vendor') . 'sweetalert/dist/sweetalert.css'; ?>">
<div class="well" ng-app="MenuApp">
	<header>
		<h2>Submenu</h2>
	</header>
	<section ng-controller="MenuCtrl">
		<h4>
      Lista de Menús
      <p>
        <small>
          por posición
        </small>
      </p>
    </h4>
    <div class="row">
      <div class="col-md-12">
          <div class="col-md-4">
            <div class="form-group">
            <label class="control-label">Menús</label>
            <select ng-model="menuid" class="form-control input-sm" ng-change="loadSubmenu()">
              <option value="">-- Seleccionar --</option>
              <?php foreach ($data['menu'] as $row) {?>
                <option value="<?php echo $row->menuid; ?>"><?php echo $row->menu; ?></option>
              <?php } ?>
            </select>
          </div>
        </div>
      </div>
    </div>
    <div class="panel panel-default panel-global" ng-hide="btnadd">
      <div class="panel-footer">
        <button ng-modal="btnadd" class="btn btn-primary" ng-click="btnadd =! btnadd" ng-disabled="menuid==undefined || menuid == ''">
          <span class="glyphicon glyphicon-plus"></span>
          <span>Agregar Submenu</span>
        </button>
      </div>
      <table class="table table-responsive table-condensed table-menu">
      	<caption>
      		<div class="form-group">
      			<div class="input-group">
      				<span class="input-group-addon">
      					<span class="glyphicon glyphicon-search"></span>
      				</span>
      				<input type="text" class="form-control" ng-model="search">
      			</div>
      		</div>
      	</caption>
        <thead>
          <tr>
            <th>#</th>
            <!-- <th>Código</th> -->
            <th>Descripción</th>
            <th>Url</th>
            <th>Posición</th>
            <th>Acción</th>
          </tr>
        </thead>
        <tbody>
        	<tr ng-repeat="x in menuList | filter:search">
        		<td>{{ $index +1 }}</td>
        		<!-- <td>{{ x.menuid }}</td> -->
        		<td>{{ x.submenu }}</td>
            <td>{{ x.url }}</td>
        		<td>{{ x.position }}</td>
        		<td>
        			<div class="dropdown">
								<button type="button" class="btn btn-link btn-xs text-black dropdown-toggle" data-toggle="dropdown">
									<span class="glyphicon glyphicon-cog"></span>
								</button>
								<ul class="dropdown-menu">
									<li>
                    <button type="button" class="btn btn-xs btn-link text-success" ng-click="showEdit(x.menuid, x.submenuid, x.submenu, x.url, x.position)"><span class="glyphicon glyphicon-edit"></span> Editar</button>
                  </li>
									<li>
                    <button type="button" class="btn btn-xs btn-link text-danger" ng-click="deleteMenu(x.menuid, x.submenuid)"><span class="glyphicon glyphicon-trash"></span> Eliminar</button>
                </ul>
        			</div>
        		</td>
        	</tr>
        </tbody>
      </table>
    </div>
    <!-- Panel add Menu -->
    <div class="panel panel-success" ng-show="btnadd">
      <div class="panel-footer">
        <h3>{{ paneltitle }}</h3>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <div class="col-md-4">
              <label class="control-label">Submenu</label>
              <input type="text" class="form-control input-sm" name="menu" ng-model="menu.submenu" value="{{ menu.submenu }}" required>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group">
            <div class="col-md-4">
              <label class="control-label">Url</label>
              <input type="text" class="form-control input-sm" name="menu" ng-model="menu.url" value="{{ menu.url }}" required>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group">
            <div class="col-md-4">
              <label class="control-label">Posición</label>
              <input type="text" class="form-control input-sm" ng-model="menu.position" value="{{ menu.position }}" required>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group">
            <input type="hidden" name="menuid" ng-model="menu.submenuid" ng-disabled="this.value != '' ? false : true" value="{{ menu.submenuid }}">
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group">
            <div class="col-md-4">
              <button type="button" class="btn btn-default btn-sm" ng-click="btnadd =! btnadd">
                <span class="glyphicon glyphicon-remove"></span>
                Cancelar
              </button>
              <button type="button" class="btn btn-success btn-sm" ng-click="saveSubmenu()">
                <span class="glyphicon glyphicon-save"></span>
                Guardar Cambios
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>

	</section>
</div>
<!-- utils tools -->
<input type="hidden" id="uri" value="<?php echo DIR; ?>">
<!-- module scripts -->
<!--<script type="text/javascript" data-main="<?php// echo Helpers\Url::pathPublic('js'); ?>admin/submenu" src="<?php// echo \Helpers\Url::pathPublic('vendor'); ?>requirejs/require.js"></script>-->
<script src="<?php echo Helpers\Url::pathPublic('vendor'); ?>jquery/dist/jquery.min.js"></script>
<script src="<?php echo Helpers\Url::pathPublic('vendor'); ?>angular/angular.min.js"></script>
<script src="<?php echo Helpers\Url::pathPublic('vendor'); ?>bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?php echo Helpers\Url::pathPublic('vendor'); ?>sweetalert/dist/sweetalert.min.js"></script>
<script src="<?php echo Helpers\Url::pathPublic('js'); ?>admin/submenu.js"></script>