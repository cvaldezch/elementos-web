<!-- utils css3 -->
<link rel="stylesheet" type="text/css" href="<?php echo \Helpers\Url::pathPublic('vendor') . 'sweetalert/dist/sweetalert.css'; ?>">
<div class="well" ng-app="RoleApp">
	<header>
		<h2>Roles</h2>
	</header>
	<section ng-controller="RoleCtrl">
		<h4>
      Lista de Roles
    </h4>
    <div class="panel panel-default panel-global" ng-hide="btnadd">
      <div class="panel-footer">
        <button ng-modal="btnadd" class="btn btn-primary" ng-click="btnadd =! btnadd">
          <span class="glyphicon glyphicon-plus"></span>
          <span>Agregar Rol</span>
        </button>
      </div>
      <table class="table table-responsive table-condensed table-role">
      	<caption>
      		<div class="form-group">
      			<div class="input-group">
      				<span class="input-group-addon">
      					<span class="glyphicon glyphicon-search"></span>
      				</span>
      				<input type="text" class="form-control" ng-model="search">
      			</div>
      		</div>
      	</caption>
        <thead>
          <tr>
            <th>#</th>
            <!-- <th>Código</th> -->
            <th>Rol</th>
            <th>Acción</th>
          </tr>
        </thead>
        <tbody>
        	<tr ng-repeat="x in rolesList | filter:search">
        		<td>{{ $index +1 }}</td>
        		<td>{{ x.description }}</td>
        		<td>
        			<div class="dropdown">
								<button type="button" class="btn btn-link btn-xs text-black dropdown-toggle" data-toggle="dropdown">
									<span class="glyphicon glyphicon-cog"></span>
								</button>
								<ul class="dropdown-menu">
									<li>
                    <button type="button" class="btn btn-xs btn-link text-success" ng-click="showEdit(x.rolesid, x.description)"><span class="glyphicon glyphicon-edit"></span> Editar</button>
                  </li>
									<li>
                    <button type="button" class="btn btn-xs btn-link text-danger" ng-click="deleteRole(x.rolesid)"><span class="glyphicon glyphicon-trash"></span> Eliminar</button>
                </ul>
        			</div>
        		</td>
        	</tr>
        </tbody>
      </table>
    </div>
    <!-- Panel add Menu -->
    <div class="panel panel-success" ng-show="btnadd">
      <div class="panel-footer">
        <h3>{{ paneltitle }}</h3>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <div class="col-md-4">
              <label class="control-label">Rol</label>
              <input type="text" class="form-control input-sm" name="rol" ng-model="role.role" value="{{ role.role }}" required>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group">
            <input type="hidden" name="rolesid" ng-disabled="this.value != '' ? false : true" ng-model="role.rolesid" value="{{ role.rolesid }}">
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group">
            <div class="col-md-4">
              <button type="button" class="btn btn-default btn-sm" ng-click="btnadd =! btnadd">
                <span class="glyphicon glyphicon-remove"></span>
                Cancelar
              </button>
              <button type="button" class="btn btn-success btn-sm" ng-click="saveRole()">
                <span class="glyphicon glyphicon-save"></span>
                Guardar Cambios
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
	</section>
</div>
<!-- utils tools -->
<input type="hidden" id="uri" value="<?php echo DIR; ?>">
<!-- module scripts -->
<script type="text/javascript" data-main="<?php echo Helpers\Url::pathPublic('js'); ?>admin/roles" src="<?php echo \Helpers\Url::pathPublic('vendor'); ?>requirejs/require.js"></script>