<link rel="stylesheet" href="<?php echo \Helpers\Url::pathPublic('vendor') . 'sweetalert/dist/sweetalert.css' ?>">
<div class="container" ng-app="confApp">
  <header>
    <h3>Configuración General</h3>
  </header>
  <section class="well" ng-controller="confCtrl">
    <div class="row">
      <div class="col-md-6">
        <div class="panel panel-default">
          <div class="panel-heading">
            <strong>Slider's</strong>
            <button type="button" class="btn btn-default btn-xs btn-xs-round pull-right" ng-model="btnshslider" ng-click="btnshslider =! btnshslider">
              <span class="glyphicon glyphicon-chevron-up" ng-class="{'glyphicon-chevron-down': btnshslider, 'glyphicon-chevron-up': !btnshslider}"></span>
            </button>
          </div>
          <div class="panel-body" ng-hide="btnshslider">
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon">
                  Número de Sliders
                </span>
                <input type="text" min="1" max="20" class="form-control text-right" ng-model="conf.nsliders" value="{{ conf.nsliders }}">
              </div>
            </div>
            <div type="button" class="form-group text-right" ng-click="saveSlider()">
              <button type="button" class="btn btn-default">
                Guardar
              </button>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="panel panel-default">
          <div class="panel-heading">
            <strong>Definir Roles</strong>
            <button type="button" class="btn btn-default btn-xs btn-xs-round pull-right" ng-model="btnshadmin" ng-click="btnshadmin =! btnshadmin">
              <span class="glyphicon glyphicon-chevron-up" ng-class="{'glyphicon-chevron-down':btnshadmin, 'glyphicon-chevron-up': !btnshadmin}"></span>
            </button>
          </div>
          <div class="panel-body" ng-hide="btnshadmin">
            <div class="form-group">
              <label class="control-label">Administrador</label>
              <select name="admin" class="form-control" ng-model="conf.admin" ng-options="x.rolesid as x.description for x in listRoles">
                <!-- <option ng-repeat="x in listRoles" value="{{ x.rolesid }}">{{ x.description }}</option> -->
              </select>
            </div>
            <div class="form-group">
              <label class="control-label">Moderador</label>
              <select name="admin" class="form-control" ng-model="conf.moderator" ng-options="x.rolesid as x.description for x in listRoles">
                <!-- <option ng-repeat="x in listRoles" value="{{ x.rolesid }}">{{ x.description }}</option> -->
              </select>
            </div>
            <div class="form-group">
              <label class="control-label">Editor</label>
              <select name="admin" class="form-control" ng-model="conf.editor" ng-options="x.rolesid as x.description for x in listRoles">
                <!-- <option ng-repeat="x in listRoles" value="{{ x.rolesid }}">{{ x.description }}</option> -->
              </select>
            </div>
            <div class="form-group text-right">
              <button type="button" class="btn btn-default" ng-click="saveRoles()">
                Guardar
              </button>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="panel panel-default">
          <div class="panel-heading">
            <strong>Nro de Post por Pagina</strong>
            <button class="btn btn-default btn-xs btn-xs-round pull-right" ng-model="btnnpost" ng-click="btnnpost =! btnnpost">
              <span class="glyphicon glyphicon-chevron-up" ng-class="{'glyphicon-chevron-down':btnnpost, 'glyphicon-chevron-up': !btnnpost}"></span>
            </button>
          </div>
          <div class="panel-body" ng-hide="btnnpost">
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon">
                  Nro Post
                </span>
                <input type="text" class="form-control text-right" ng-model="conf.npost" value="{{ conf.npost }}">
              </div>
            </div>
            <div class="form-group text-right">
              <button type="button" class="btn btn-default" ng-click="saveNPost()">
                Guardar
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<input type="hidden" value="<?php echo DIR; ?>" id="uri">
<!-- block script -->
<?php
  \Helpers\Assets::js(array(
    \Helpers\Url::pathPublic('vendor') . 'jquery/dist/jquery.min.js',
    \Helpers\Url::pathPublic('vendor') . 'bootstrap/dist/js/bootstrap.min.js',
    \Helpers\Url::pathPublic('vendor') . 'angularjs/angular.min.js',
    \Helpers\Url::pathPublic('vendor') . 'sweetalert/dist/sweetalert.min.js',
    \Helpers\Url::pathPublic('js') . 'admin/general.js'
  ));
?>
<!-- end block -->