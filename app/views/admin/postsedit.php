<?php
use Helpers\Url;
?>
<link rel="stylesheet" href="<?php echo Url::pathPublic('vendor') . 'chosen/chosen.css'; ?>">
<link rel="stylesheet" href="<?php echo Url::pathPublic('vendor') . 'sweetalert/dist/sweetalert.css'; ?>">
<link rel="stylesheet" href="<?php echo Url::pathPublic('vendor') . 'clockpicker/dist/bootstrap-clockpicker.min.css'; ?>">
<link rel="stylesheet" href="<?php echo Url::pathPublic('vendor') . 'jquery-ui/themes/smoothness/jquery-ui.min.css'; ?>">
<?php echo \Core\Error::display($error); ?>
<header>
  <h3>Editar Posts</h3>
</header>
<section>
    <div class="well">
        <form action="" method="post" id="regpost" enctype="multipart/form-data">
          <!-- <p class="text-right">
              <button class="btn btn-primary">
                  <span class="glyphicon glyphicon-floppy-disk"></span>
                  <span class="glyphicon-class">Publicar</span>
              </button>
          </p> -->
          <p>
              <label class="control-label">Titulo</label>
              <input type="text" name="title" class="form-control" value="<?php echo $data['post'][0]->title; ?>" data-validation="required" data-validation-error-msg="Debe de ingresar un titulo para la <q>Publicación</q>">
          </p>
          <p>
              <label class="control-label">Imagen</label>
              <img src="<?php echo DIR. "app/" .$data['post'][0]->image; ?>" alt="Responsive image" class="img-responsive img-rounded" height="140" width="140">
              <strong><q>Si desea mantener la imagen no carge ninguna imagen.</q></strong>
              <input type="file" class="for-control" name="image" accept="image/*" data-validation-allowing="jpg, png, gif">
          </p>
          <p>
            <label class="control-label">Contenido</label>
            <br>
            <textarea name="content" class="col-md-12 form-control" rows="20" data-validation="required" data-validation-error-msg="Debe de ingresar un contenido."><?php echo $data['post'][0]->content; ?></textarea>
          </p>
          <!-- <p>
              <label class="control-label">Referencias</label>
              <br>
              <textarea name="references" class="col-md-12 form-control"></textarea>
          </p> -->
          <p>
            <label class="control-label">Grupo</label>
            <select name="menu" class="form-control">
              <?php 
                foreach ($data['menu'] as $row) {
                  if ($row->menuid == $data['post'][0]->sgroup) {
                    ?>
                    <option value="<?php echo $row->menuid; ?>" selected><?php echo $row->menu; ?></option>
                    <?php
                  }else{
              ?>
              <option value="<?php echo $row->menuid; ?>"><?php echo $row->menu; ?></option>
              <?php }} ?>
            </select>
          </p>
          <p>
            <label class="control-label">Sección</label>
            <select name="submenu" class="form-control">
              <?php
                foreach ($data['submenu'] as $row) {
                  if ($row->submenuid == $data['post'][0]->section) {
                    ?>
                    <option value="<?php echo $row->submenuid; ?>" selected><?php echo $row->submenu; ?></option>
                    <?php
                  }else{
                    ?>
                    <option value="<?php echo $row->submenuid; ?>"><?php echo $row->submenu; ?></option>
                    <?php
                  }
                }
              ?>
            </select>
          </p>
          <!-- <p>
            <label class="control-label">Fecha Publicación</label>
            <input type="text" name="date" data-validation-format="yyyy-mm-dd" value="<?php // echo date('Y-m-d', strtotime($data['post'][0]->publish)); ?>" class="form-control input-sm" data-validation-error-msg="Ingrese una fecha valida">
          </p>
          <p>
              <label class="control-label">Hora Publicación</label>
              <input type="text" class="form-control input-sm clockpick" name="hour" value="<?php // echo date('H:i', strtotime($data['post'][0]->publish)); ?>" data-validation-error-msg="Ingrese una hora validad." readonly="">
          </p> -->
          <?php if (\Helpers\Session::get('role') == 'administrator'){ ?>
          <p>
              <label class="control-label">Poner en Slider</label>
              <?php
                  if ($data['post'][0]->slider == '1') {
                      echo "<input type=\"checkbox\" name=\"slider\"  value=\"1\" checked>";
                  }else{
                      echo "<input type=\"checkbox\" name=\"slider\"  value=\"1\">";
                  }
              ?>
          </p>
          <?php } ?>
          <p>
              <label class="control-label">Tags</label><span>(Utiliza # para el ingreso de los tag. ejemplo: #internet)</span>
              <br>
              <!-- <select name="category" id="" multiple="" class="form-control" data-validation="required" data-validation-error-msg="Debe de seleccionar por lo menos un tag"> -->
              <?php
                  // $tag = $data['post'][0]->category;
                  // foreach ($data['category'] as $row) {
                  //    if (strpos($tag, strval($row->category_id)) !== false) {
                  //        echo "<option value=\"$row->category_id\" selected>$row->category</option>";
                  //    }else{
                  //       echo "<option value=\"$row->category_id\">$row->category</option>";
                  //    }
                  // }
              ?>
              <!-- </select> -->
              <textarea name="category" rows="5" class="form-control" data-validation="required" data-validation-error-msg="Debe de seleccionar por lo menos un tag"><?php 
                  // foreach ($data['categories'] as $val)
                  // {
                  //   echo '#'. $val->category . ' ';
                  // }
                  // 
                  echo $data['categories'];
                ?></textarea>
          </p>
          <p>
            <label class="control-label">Publicar como <?php echo $data['config']->sitename; ?></label>
            <input type="checkbox" name="anonymous"  value="0" <?php if ($data['post'][0]->anonymous) echo "checked" ?>>
          </p>
          <p>
            <input type="hidden" name="post_id" value="<?php echo $data['post'][0]->posts_id; ?>">
          </p>
          <p class="">
            <button class="btn btn-primary">
                <span class="glyphicon glyphicon-floppy-disk"></span>
                <span class="glyphicon-class">Publicar</span>
            </button>
          </p>
        </form>
        <!--<script src="http://js.nicedit.com/nicEdit-latest.js" type="text/javascript"></script>
        <script type="text/javascript">bkLib.onDomLoaded(nicEditors.allTextAreas);</script>-->
    </div>
</section>
<br>
<br>
<br>
<br>
<?php
  echo \Helpers\Assets::js(array(
    Url::pathPublic('vendor') . 'jquery/dist/jquery.min.js',
    Url::pathPublic('vendor') . 'bootstrap/dist/js/bootstrap.min.js',
    Url::pathPublic('vendor') . 'angularjs/angular.min.js',
    Url::pathPublic('vendor') . 'tinymce/tinymce.min.js',
    Url::pathPublic('vendor') . 'mustache/mustache.min.js',
    Url::pathPublic('vendor') . 'chosen/chosen.jquery.js',
    Url::pathPublic('vendor') . 'sweetalert/dist/sweetalert.min.js',
    Url::pathPublic('vendor') . 'clockpicker/dist/bootstrap-clockpicker.min.js',
    Url::pathPublic('vendor') . 'jquery-form-validator/form-validator/jquery.form-validator.min.js',
    Url::pathPublic('vendor') . 'jquery-ui/jquery-ui.min.js',
    Url::pathPublic('js') . 'admin/category.js',
  ));
?>
<script type="text/javascript">
  $(function () {
    $("[name=date]").datepicker({
        showAnim: "slide",
        dateFormat: "yy-mm-dd",
        minDate: "0"
    });
    tinymce.init({
      selector:'[name=content]',  
      theme: 'modern',
      // plugins: 'link image emoticons paste textcolor colorpicker textpattern media',
      plugins: [
        'advlist autolink lists link image charmap print preview hr anchor pagebreak',
        'searchreplace wordcount visualblocks visualchars code fullscreen',
        'insertdatetime nonbreaking save table contextmenu directionality',
        'emoticons template paste textcolor colorpicker textpattern'
      ],
      fontsize_formats: "8pt 10pt 12pt 14pt 16pt 18pt 24pt 36pt",
      toolbar1: "insertfile undo redo | pastetext | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | fontselect | fontsizeselect | link image",
      toolbar2: 'forecolor backcolor emoticons | insert',
      // menubar: "insert",
      media_live_embeds: true,
      image_advtab: true,
      visualblocks_default_state: false,
      // fontsize_formats: "8pt 9pt 10pt 11pt 12pt 16pt 18pt 24pt 36pt",
      // toolbar: "insertfile undo redo | pastetext | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | fontselect | fontsizeselect | link image"
    });
    //tinymce.init({selector:'[name=references]', menubar: false});
    // $("[name=category]").chosen({width: "100%"});
    // $(".clockpick").clockpicker({
    //     autoclose: true,
    //     donetext: 'Hecho',
    // });
    $.validate({
      form: "#regpost",
      modules: "date, file",
      onSuccess: function () {
          form = new FormData();
          form.append('title', $.trim($("[name=title]").val()));
          $image = $("[name=image]").get(0)
          if ($image.files.length) {
            form.append('image', $image.files[0]);
          };
          var references = ""
          content = $("#content_ifr").contents().find("body").text().split(" ");
          for(var i=0;i < 40;i++){
            if (typeof content[i] == 'undefined' || content[i] == null) {
              continue;
            }
            else
            {
              references += content[i] + " ";
            }
          }
          form.append('content', $("#content_ifr").contents().find("body").html());
          form.append('reference', references);
          //form.append('date', $("[name=date]").val());
          //form.append('hour', $("[name=hour]").val());
          $slider = $("[name=slider]")
          if ($slider.length > 0){
            form.append('slider',  $slider.is(":checked") ? $slider.val().toString() : "0");
          }else{
            form.append('slider', "0");
          }
          form.append('menu', $("[name=menu]").val());
          form.append('submenu', $("[name=submenu]").val());
          form.append('category', $("[name=category]").val().toString());
          form.append('author',  $("[name=anonymous]").is(":checked") ? "1" : "0");
          form.append('update', true);
          $.ajax({
            url: '',
            data: form,
            type: 'POST',
            dataType: 'json',
            cache: false,
            processData: false,
            contentType: false,
            success: function (response) {
              if (response.status) {
                swal({
                  title: "Felicidades?",
                  text: "El post se a generado sin nigun problema.!",
                  type: "success",
                  showConfirmButton: false,
                  timer: 2500
                });
                setTimeout(function() {
                  location.href = "<?php echo DIR; ?>admin/posts";
                }, 2500);
              };
            },
            error: function (error) {
              swal({
                title: "Opps! Tenemos un error.",
                text: "a ocurrido un error al actualizar el post.",
                type: "error",
                showConfirmButton: false,
                timer: 2600
              });
            }
          });
          return false;
      }
    });
    $("[name=menu]").on("click", function ()
    {
      var context = {
        list: true,
        menu: $("[name=menu]").val()
      }
      $.getJSON("<?php echo DIR; ?>wservices/submenu", context, function (response){
        if (response.status) {
          template = "{{#menus}}<option value=\"{{submenuid}}\">{{ submenu }}</option>{{/menus}}"
          $("[name=submenu]").html(Mustache.render(template, response));
        }else{
          console.log("error al traer los datos");
        };
      });
    });
    // setTimeout(function  () {
    //   $(".chosen-choices").addClass('form-control');
    // }, 800);
  });
</script>