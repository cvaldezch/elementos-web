
<h1>
	Posts
</h1>
<?php echo \Helpers\Session::pull('message'); ?>
<section data-ng-app="postApp" data-ng-controller="postCtrl">
  <a href="<?php echo DIR; ?>admin/posts/add" class="btn btn-info">Agregar Posts</a>
  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Inicio</a></li>
    <li role="presentation">
      <a href="#tyear" aria-controls="anio" role="tab" data-toggle="tab">Vista Año</a>
    </li>
    <li role="presentation">
      <a href="#tmonth" aria-controls="anio" role="tab" data-toggle="tab">Vistas Mes</a>
    </li>
    <li role="presentation"><a href="#anio" aria-controls="anio" role="tab" data-toggle="tab" ng-click="drawByAnioAreaChart()">Por Año</a></li>
    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">por mes</a></li>

    <!-- <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Settings</a></li> -->
  </ul>
  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="home">
      <table class="table table-striped table-hover table-condensed table-bordered responsive">
        <caption>
          <div class="form-group has-primary">
            <div class="input-group">
              <span class="input-group-addon">
                <span class="glyphicon glyphicon-search"></span>
              </span>
              <input type="text" class="form-control" data-ng-model="search">
            </div>
          </div>
        </caption>
        <thead>
          <tr>
            <th colspan="8">
              <button type="button" class="btn btn-default text-black" ng-click="deletePost()">
                <i class="fa fa-trash fa-lg"></i>
                Eliminar Seleccionado
              </button>
            </th>
          </tr>
          <tr class="bg-primary">
            <th width="1cm" class="text-center">
              <input type="checkbox" ng-model="chkall">
            </th>
            <th>#</th>
            <th>Titulo</th>
            <th>Registrado</th>
            <th>Publicado</th>
            <th>Vistas</th>
            <th>Slider</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <tr data-ng-repeat="x in lposts = (listposts | filter:{title: search})" data-ng-class="{'info':x.publish >= x.register, 'success': x.modify}">
            <td class="text-center">
              <input type="checkbox" value="{{x.posts_id}}" name="postdel" ng-checked="chkall">
            </td>
            <td>{{$index + 1}}</td>
            <td><b><a href="<?php echo DIR; ?>{{x.posts_id}}/{{x.title.split(' ').join('-')}}" target="_blank">{{x.title}}</a></b></td>
            <td>{{x.register}}</td>
            <td>{{x.modify ? x.modify : x.publish}}</td>
            <th>{{x.visited}}</th>
            <td><input type="checkbox" data-ng-value="x.posts_id" data-ng-checked="x.slider == 1" data-ng-click="changeSlider($event)"></td>
            <td>
              <div class="dropdown">
                <button class="btn btn-xs btn-link text-black dropdown-toggle" data-toggle="dropdown">
                  <span class="glyphicon glyphicon-cog"></span>
                </button>
                <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                  <li role="presentation">
                    <a role="menuitem" tabindex="-1" data-ng-click="openwindow()" href="#">Ver</a>
                  </li>
                  <li class="divider"></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="posts/edit/{{x.posts_id}}">Editar</a></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="posts/disabled/{{x.posts_id}}">Eliminar</a></li>
                </ul>
              </div>
            </td>
          </tr>
        </tbody>
        <tfoot>
          <tr>
            <td>Total post ({{cm = lposts.length}})</td>
          </tr>
        </tfoot>
      </table>
      <div>
        Page {{pinit}} of {{tpage}}
        <p>
          <nav>
            <button class="btn btn-default" ng-click="listPosts(pinit-1)" ng-if="pinit!=1">
              <i class="fa fa-angle-double-left"></i>
              <span>Anterior</span>
            </button>
            <button class="btn btn-default" ng-click="listPosts(pinit+1)" ng-if="pinit!=tpage">
              <span>Siguiente</span>
              <i class="fa fa-angle-double-right"></i>
            </button>
          </nav>
        </p>
        <input type="hidden" ng-model="cm" ng-Change="changeSchangelpost()" >
      </div>
    </div>
    <!-- by year -->
    <div role="tabpanel" class="tab-pane" id="tyear">
      <div class="form-group">
        <label for="">Año</label>
        <select class="form-control" ng-model="svyear" ng-options="x.year as x.year for x in years" ng-change="getDataYears()">
          <option value="">-- Seleccione --</option>
        </select>
      </div>
      <table class="table table-hover table-condensed responsive">
        <caption>
          <div class="form-group has-primary">
            <div class="input-group">
              <span class="input-group-addon">Buscar </span>
              <input type="text" class="form-control" ng-model="vyear">
            </div>
          </div>
        </caption>
        <thead>
          <tr>
            <th>#</th>
            <th>Titulo</th>
            <th>Registrado</th>
            <th>Publicado</th>
            <th>Vistas</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="x in vtyear | filter: vyear">
            <td>{{$index+1}}</td>
            <td><a href="<?php echo DIR; ?>{{x.posts_id}}/{{x.title}}" target="_blank">{{x.title}}</a></td>
            <td>{{x.register}}</td>
            <td>{{x.publish}}</td>
            <td>{{x.visited}}</td>
          </tr>
        </tbody>
      </table>
    </div>
    <!-- by month -->
    <div role="tabpanel" class="tab-pane" id="tmonth">
      <div class="form-group">
        <label for="">Año</label>
        <select class="form-control" ng-model="slyear" ng-options="x.year as x.year for x in years" ng-change="getMonths()">
          <option value="">-- Seleccione --</option>
        </select>
      </div>
      <div class="form-group">
        <label for="">Mes</label>
        <select class="form-control" ng-model="vmonth" ng-options="x.month as months[x.month - 1] for x in monthbyy" ng-change="getDataMonths()">
          <option value="">-- Seleccione --</option>
        </select>
      </div>
      <table class="table table-hover table-condensed responsive">
        <caption>
          <div class="form-group has-primary">
            <div class="input-group">
              <span class="input-group-addon">Buscar </span>
              <input type="text" class="form-control" ng-model="smonth">
            </div>
          </div>
        </caption>
        <thead>
          <tr>
            <th>#</th>
            <th>Titulo</th>
            <th>Registrado</th>
            <th>Publicado</th>
            <th>Vistas</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="x in vtmonth | filter: smonth">
            <td>{{$index+1}}</td>
            <td><a href="<?php echo DIR; ?>{{x.posts_id}}/{{x.title}}" target="_blank">{{x.title}}</a></td>
            <td>{{x.register}}</td>
            <td>{{x.publish}}</td>
            <td>{{x.visited}}</td>
          </tr>
        </tbody>
      </table>
    </div>

    <div role="tabpanel" class="tab-pane" id="anio">
      <div id="byanio_area"></div>
      <div id="byanio_table"></div>
    </div>
    <div role="tabpanel" class="tab-pane" id="messages">
      <div class="panel">
        <div class="panel-body">
          <div class="form-group">
            <label for="">Año</label>
            <select ng-model="mysel" class="form-control" ng-model="yselect" ng-options="x.year as x.year for x in years" ng-change="drawByMonthAreaChart()">
              <option value="">--Seleccione--</option>
            </select>
          </div>
          <div id="bymonth_area"></div>
          <div id="bymonth_table"></div>
        </div>
      </div>
    </div>
  </div>
  
<!-- utils -->
</section>
<link rel="stylesheet" href="<?php echo \Helpers\Url::pathPublic('vendor') . 'sweetalert/dist/sweetalert.css' ?>">
<!-- scripts -->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<!-- <script src="http://www.google.com/uds/modules/gviz/gviz-api.js"></script> -->
<!-- <script type="text/javascript" src="https://www.google.com/jsapi"></script> -->
<script src="<?php echo \Helpers\Url::pathPublic('vendor') . 'angularjs/angular.min.js' ?>"></script>
<script src="<?php echo \Helpers\Url::pathPublic('vendor') . 'angular-sanitize/angular-sanitize.min.js' ?>"></script>
<script src="<?php echo \Helpers\Url::pathPublic('vendor') . 'sweetalert/dist/sweetalert.min.js' ?>"></script>
<script src="<?php echo \Helpers\Url::pathPublic('js') . 'admin/posts.js' ?>"></script>