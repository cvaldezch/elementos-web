<?php echo \Core\Error::display($error); ?>
<div class="well">
	<div class="text-center">
        <img src="<?php echo \Helpers\Url::pathPublic('images'); ?>elementos.svg" alt="" class="img-responsive" style="margin-left: auto; margin-right: auto;" height="100" width="100" alt="Responsive image">
    </div>
	<div class="row">
		<div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
	        <div class="panel panel-info" >
	            <div class="panel-heading">
	                <div class="panel-title">Ingresar</div>
	                <div style="float:right; font-size: 80%; position: relative; top:-15px">
	                	<a href="/pwd/recovery" name="recover" class="btn btn-link btn-sm">
	                		<small>Olvidaste tu contraseña?</small>
	                	</a>
	                </div>
	            </div>
	            <div style="padding-top:30px" class="panel-body" >
					 <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
	                <form action="" method="post">
	                    <div style="margin-bottom: 25px" class="input-group">
	                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
	                        <input id="login-username" type="text" class="form-control" name="username" value="" placeholder="username">
	                    </div>
	                    <div style="margin-bottom: 25px" class="input-group">
	                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
	                        <input id="login-password" type="password" class="form-control" name="password" placeholder="password">
	                    </div>
	                    <!-- <div class="input-group">
	                              <div class="checkbox">
	                                <label>
	                                  <input id="login-remember" type="checkbox" name="remember" value="1"> Remember me
	                                </label>
	                              </div>
	                            </div> -->
	                    <div style="margin-top:10px" class="form-group">
	                        <!-- Button -->
	                        <div class="col-sm-12 controls">
	                          <input id="btn-login" type="submit" name="submit" value="Login" class="btn btn-success">
	                          <a id="btn-fblogin" href="<?php echo DIR; ?>" class="btn btn-primary pull-right">Ir a inicio</a>
                            <a id="btn-" href="<?php echo DIR . 'user/register'; ?>" class="btn btn-link pull-right">Registrate</a>
	                        </div>
	                    </div>
	                    <!-- <div class="form-group">
	                        <div class="col-md-12 control">
	                            <div style="border-top: 1px solid#888; padding-top:15px; font-size:85%" >
	                                Don't have an account!
	                                <a href="#" onClick="$('#loginbox').hide(); $('#signupbox').show()">
	                                    Sign Up Here
	                                </a>
	                            </div>
	                        </div>
	                    </div>-->
	                </form>
	            </div>
	        </div>
	    </div>
	</div>
  <input type="hidden" name="base" value="<?php echo DIR; ?>">
  <input type="hidden" name="uri" value="<?php echo DIR; ?>">
	<!-- <form action="" method="post">
		<p>Username<br><input type="text" name="username"></p>
		<p>Password<br><input type="password" name="password"></p>
		<p><input type="submit" name="submit" value="Login"></p>
	</form> -->
</div>