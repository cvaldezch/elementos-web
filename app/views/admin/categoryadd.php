<section class="well">
	<?php echo \Core\Error::display($error); ?>
	<h1>Agregar Categoria</h1>
	<hr>
	<?php echo \Helpers\Session::pull('message'); ?>
	<form action="" method="post">
		<p>
			<div class="form-group">
				<label class="control-label">Categoria</label>
				<input type="text" class="form-control input-sm" name="category">
			</div>
		</p>
		<p>
			<div class="form-group">
				<button type="reset" class="btn btn-default">
					<span class="glyphicon glyphicon-erase"></span>
					Limpiar
				</button>
				<button type="submit" name="submit" class="btn btn-primary">
					<span class="glyphicon glyphicon-plus"></span>
					Guardar Cambios
				</button>
			</div>
		</p>
	</form>
</section>