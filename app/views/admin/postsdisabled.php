<section>
  <div class="jumbotron container">
  <div class="panel panel-danger">
    <div class="panel-heading">
      <form action="" method="post">
        Realmente desea deshabilitar el post?
        <input type="hidden" name="postid" value="<?php echo $data['post'][0]->posts_id; ?>">
        <button type="submit" name="disabled" value="true" class="btn btn-danger">
          <span class="glyphicon glyphicon-remove-circle"></span>
          Deshabilitar
        </button>
      </form>
    </div>
  </div>
    <img src="<?php echo DIR .'app/'. $data['post'][0]->image; ?>" class="img-responsive" alt="<?php echo $data['post'][0]->title; ?>">
    <h1><?php echo $data['post'][0]->title; ?></h1>
    <article>
      <?php echo $data['post'][0]->content; ?>
    </article>
    <aside>
      <div class="panel panel-default">
        <div class="panel-body">
        <strong>Tags</strong>
        <br>
          <?php
            $tag = $data['post'][0]->category;
            // echo $tag;
            foreach (explode(',', $tag) as $val)
            {
              $cat = new \Models\Admin\Category();
              $cats = $cat->getCategory($val)[0]->category;
              echo "<a href=\"". DIR ."tag/" . $cats ."/filter\"><span class=\"label label-default\"> <span class=\"glyphicon glyphicon-tag\"></span> ". $cats. "</span></a> ";
            }
          ?>
        </div>
      </div>
    </aside>
  </div>
</section>