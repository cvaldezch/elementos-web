<!-- utils css3 -->
<link rel="stylesheet" type="text/css" href="<?php echo \Helpers\Url::pathPublic('vendor') . 'sweetalert/dist/sweetalert.css'; ?>">
<div class="well" ng-app="RequestApp">
	<header>
		<h2>Solitud de Publicación</h2>
	</header>
	<section ng-controller="ReqCtrl">
		<h4>
      Lista de Usuarios
    </h4>
    <div class="panel panel-primary" ng-hide="btnshow">
      <div class="panel-footer">
        <div class="form-group">
          <div class="input-group">
            <span class="input-group-btn">
              <button type="button" class="btn btn-default" ng-click="loadRequest()">
                <span class="glyphicon glyphicon-refresh"></span>
              </button>
            </span>
            <span class="input-group-addon">
              <span class="glyphicon glyphicon-search"></span>
            </span>
            <input class="form-control" ng-model="search" />
          </div>
        </div>
      </div>
      <table class="table table-responsive table-condensed table-hover">
        <thead>
          <tr>
            <th>Item</th>
            <th>Nombres</th>
            <th>Usuario</th>
            <th>Correo</th>
            <th>Acción</th>
          </tr>
        </thead>
        <tbody>
          <tr class="active" ng-repeat="x in listRequest | filter:search">
            <td>{{ $index + 1 }}</td>
            <td>{{ x.name }}</td>
            <td>{{ x.username }}</td>
            <td>{{ x.email }}</td>
            <td>
              <button type="button" class="btn btn-link btn-xs text-black" ng-model="btnshow" ng-click="showDetails(x.username, x.name, x.email, x.menuid, x.submenuid, x.menu, x.submenu, x.comment)">
                <span class="glyphicon glyphicon-cog"></span>
              </button>
            </td>
          </tr>
        </tbody>
      </table>
      <br>
    </div>
    <div class="panel panel-success" ng-show="btnshow">
      <div class="panel-heading">
        <button class="btn btn-default btn-xs" ng-click="btnshow =! btnshow">
          <span class="glyphicon glyphicon-chevron-left"></span>
        </button>
        <strong>Detalle de Usuario</strong>
        <div class="btn-group pull-right">
          <!-- <div class="col-md-2 col-lg-3"> -->
            <!-- <button type="button" class="btn btn-default btn-sm" ng-click="savePublisher(false)">
              <span class="glyphicon glyphicon-thumbs-down"></span>
              Descartar
            </button>
            <button type="button" class="btn btn-success btn-sm text-black" ng-click="savePublisher(true)">
              <span class="glyphicon glyphicon-thumbs-up"></span>
              Aceptar
            </button> -->
          <!-- </div> -->
        </div>
      </div>
      <div class="panel-body">
        <div class="row">
          <div class="col-md-12">
            <div class="form-group has-success">
              <label class="control-label">Acción</label>
              <select class="form-control" ng-change="savePublisher()" ng-model="user.publisher">
                <option value="">-- Seleccionar --</option>
                <?php foreach ($data['roles'] as $row){ ?>
                  <?php if ($data['config']->admin == $row->rolesid) {
                    continue;
                  } ?>
                  <option value="<?php echo $row->rolesid; ?>"><?php echo $row->description; ?> - Aceptar</option>
                <?php } ?>
                <option value="false">Rechazar</option>
              </select>
            </div>
          </div>
          <div class="col-md-12">
            <label class="control-label">Usuario: </label>
            <span>{{ user.username }}</span>
          </div>
          <div class="col-md-12">
            <label class="control-label">Nombres: </label>
            <span>{{ user.name }}</span>
          </div>
          <div class="col-md-12">
            <label class="control-label">Email: </label>
            <span>{{ user.email }}</span>
          </div>
          <div class="col-md-12">
            <label for="" class="control-label">Categoría: </label>
            <p><strong>{{ user.menu }}</strong></p>
          </div>
          <div class="col-md-12">
            <label for="" class="control-label">Sección: </label>
            <p><strong>{{ user.submenu }}</strong></p>
          </div>
          <div class="col-md-12">
            <label class="control-label">Comentario: </label>
            <textarea class="form-control" rows="7" disabled>{{ user.comment }}</textarea>
          </div>
        </div>
      </div>
    </div>
	</section>
</div>
<!-- utils tools -->
<input type="hidden" id="uri" value="<?php echo DIR; ?>">
<!-- module scripts -->
<script src="<?php echo \Helpers\Url::pathPublic('vendor'); ?>angularjs/angular.min.js"></script>
<script src="<?php echo \Helpers\Url::pathPublic('vendor'); ?>jquery/dist/jquery.min.js"></script>
<script src="<?php echo \Helpers\Url::pathPublic('vendor'); ?>bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?php echo \Helpers\Url::pathPublic('vendor'); ?>sweetalert/dist/sweetalert.min.js"></script>
<script src="<?php echo \Helpers\Url::pathPublic('js'); ?>admin/request.js"></script>
<!--<script type="text/javascript" data-main="<?php // echo Helpers\Url::pathPublic('js'); ?>admin/request" src="<?php // echo \Helpers\Url::pathPublic('vendor'); ?>requirejs/require.js"></script>-->