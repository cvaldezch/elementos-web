<!-- load css -->
<?php
  \Helpers\Assets::css(array(
    \Helpers\Url::pathPublic('vendor') . 'font-awesome/css/font-awesome.min.css',
    \Helpers\Url::pathPublic('vendor') . 'sweetalert/dist/sweetalert.css'
  ));
?>
<h1>
  Users
</h1>
<?php echo \Helpers\Session::pull('message'); ?>
<section ng-app="usersApp" ng-controller="userCtrl">
  <p>
    <a href="<?php echo DIR; ?>admin/users/add" class="btn btn-primary" ng-hide="gshow">Agregar Usuario</a>
  </p>
  <div class="panel panel-primary" ng-hide="gshow">
    <div class="panel-footer">
      <div class="input-group">
        <span class="input-group-btn">
          <button type="button" class="btn btn-success" ng-click="loadUsers()">
            <span class="glyphicon glyphicon-refresh"></span>
          </button>
        </span>
        <span class="input-group-addon">
          <span class="glyphicon glyphicon-search"></span>
        </span>
        <input type="text" class="form-control" ng-model="search">
      </div>
    </div>
    <div class="table-responsived">
      <table class="table table-hover table-condensed table-bordered responsive">
      <caption>
        <h4>{{ titletable}}</h4>
      </caption>
        <thead>
          <tr>
            <th class="text-center">#</th>
            <th>Nombres</th>
            <th>Username</th>
            <th>Correo</th>
            <th>Estado</th>
            <th class="text-center">Acción</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="x in usersList | filter:search" ng-class="{'success': x.role == '<?php echo \Helpers\Session::get('conf')->admin; ?>', 'warning': x.role == '<?php echo \Helpers\Session::get('conf')->moderator; ?>', 'info': x.role == '<?php echo \Helpers\Session::get('conf')->editor; ?>'}">
            <td>{{$index + 1}}</td>
            <td>{{x.firstname}} {{x.lastname}}</td>
            <td>{{x.username}}</td>
            <td>{{x.email}}</td>
            <td>{{x.description}}</td>
            <td class="text-center">
              <div class="dropdown">
                <button type="button" class="btn btn-link btn-xs text-black dropdown-toogle" data-toggle="dropdown">
                  <span class="glyphicon glyphicon-cog"></span>
                </button>
                <ul class="dropdown-menu">
                  <li>
                    <button class="btn btn-xs btn-link" type="button" ng-click="conAdmin(x.users_id, x.email)">Administrador</button>
                  </li>
                  <li class="divider"></li>
                  <li>
                    <button class="btn btn-xs btn-link" type="button" ng-click="showRoles()">Roles</button>
                  </li>
                  <!-- <li>
                    <button class="btn btn-xs btn-link" type="button" ng-click="conAdmin(x.users_id, x.email)">Editor</button>
                  </li>-->
                  <li class="divider"></li>
                  <li>
                    <button class="btn btn-xs btn-link" type="button" ng-click="conUsers(x.users_id, x.email)">Usuario</button>
                  </li>
                  <li class="divider"></li>
                  <li>
                    <button class="btn btn-xs btn-link" type="button" ng-click="Desactive(x.users_id, x.email)">Descactivar Cuenta</button>
                  </li>
                </ul>
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="panel-footer"></div>
  </div>
  <!-- panels -->
  <div class="panel panel-warning" ng-show="gshow">
    <div class="panel-heading">
      <h4>Cambiar de Rol : {{mail}}</h4>
    </div>
    <div class="panel-body">
      <div class="form-group">
        <button type="button" ng-click="gshow = !gshow" class="btn btn-default btn-sm">
          <span class="fa fa-times"></span>
          Cancelar
        </button>
      </div>
      <div class="form-group">
        <label class="control-label">Grupo</label>
        <select name="sgroup" class="form-control" ng-model="sgroup" ng-change="loadSubmenus()">
          <!-- <option value="" selected></option> -->
          <option ng-repeat="x in lsgroup" value="{{x.menuid}}" ng-selected="spublish==x.menuid">{{x.menu}}</option>
        </select>
      </div>
      <div class="form-group">
        <label class="control-label">Sección</label>
        <select name="section" class="form-control" ng-model="section">
          <option ng-repeat="x in lsection" value="{{x.submenuid}}" ng-selected="ssection==x.submenuid">{{x.submenu}}</option>
        </select>
      </div>
      <div class="form-group">
        <button type="button" class="btn btn-primary" ng-click="saveEditor()">
          Guardar
        </button>
      </div>
    </div>
  </div>
</section>
<input type="hidden" id="uri" value="<?php echo DIR; ?>">
<!-- block script -->
<?php
  \Helpers\Assets::js(array(
      \Helpers\Url::pathPublic('vendor') . 'angularjs/angular.min.js',
      \Helpers\Url::pathPublic('vendor') . 'sweetalert/dist/sweetalert.min.js',
      \Helpers\Url::pathPublic('js') . 'admin/users.js'
    ));
?>
<!-- end block