<section ng-app="appCategory">
	<div class="well" ng-controller="categoryController">
		<h1>Categoria</h1>
		<?php echo \Helpers\Session::pull('message'); ?>
		<p>
			<!-- <a href="<?php echo DIR; ?>admin/categories/add" class="btn btn-info">
				<span class="glyphicon glyphicon-plus"></span> Add Categoria
			</a> -->
			<button type="button" class="btn btn-info" ng-click="showAddCategory()" data-id="addCategory">
				<span class="glyphicon glyphicon-plus"></span> Add Categoria
			</button>
		</p>
		<table class="table table-hover table-condensed table-bordered table-responsive">
			<thead>
				<tr class="bg-primary">
					<th class="col-md-1 text-center">Item</th>
					<th class="col-md-2 text-center">Código</th>
					<th clas>Descripción</th>
					<th class="col-md-1 text-center">Editar</th>
					<th class="col-md-1 text-center">Eliminar</th>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="x in categories">
					<td class="text-center">{{ $index + 1 }}</td>
					<td>{{ x.category_id }}</td>
					<td>{{ x.category }}</td>
					<td class="text-center">
						<button class="btn btn-xs btn-link text-black" ng-click="showEditCategory(x.category_id, x.category)">
							<span class="glyphicon glyphicon-edit"></span>
						</button>
					</td>
					<td class="text-center">
						<button class="btn btn-xs btn-link text-error" ng-click="showDel(x.category_id)">
							<span class="glyphicon glyphicon-trash"></span>
						</button>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<!-- Block modals -->
	<div class="modal fade" id="addCategory">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Categoria</h4>
				</div>
				<div class="modal-body" ng-controller="categoryController">
					<form>
						<p>
							<div class="form-group">
								<label class="control-label">Categoria</label>
								<input type="text" class="form-control input-sm" name="category" ng-model="category.category">
							</div>
						</p>
						<p>
							<div class="form-group">
								<button type="button" data-dismiss="modal" class="btn btn-default">
									<span class="glyphicon glyphicon-remove"></span>
									Salir
								</button>
								<button name="submit" class="btn btn-primary" ng-click="addCategory(category)">
									<span class="glyphicon glyphicon-plus"></span>
									Guardar Cambios
								</button>
							</div>
						</p>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="editCategory" ng-controller="categoryController">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Edit Categoria</h4>
				</div>
				<div class="modal-body" >
					<form>
						<p>
							<div class="form-group">
								<label class="control-label">Categoria</label>
								<input type="text" class="form-control input-sm" name="ecategory">
								<input type="hidden" class="form-control input-sm" name="ecategoryid">
							</div>
						</p>
						<p>
							<div class="form-group">
								<button type="button" data-dismiss="modal" class="btn btn-default">
									<span class="glyphicon glyphicon-remove"></span>
									Salir
								</button>
								<button name="edit" class="btn btn-success" ng-click="editCategory()">
									<span class="glyphicon glyphicon-edit"></span>
									Guardar Cambios
								</button>
							</div>
						</p>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="delCategory">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<a class="close" data-dismiss="modal">&times;</a>
					<h4 class="modal-title">
						Eliminar Categoria
					</h4>
				</div>
				<div class="modal-body" ng-controller="categoryController">
					Realmante desea eliminar la categoria seleccionada?
					<input type="hidden" name="dcategoryid">
					<p>
						<button type="button" data-dismiss="modal" class="btn btn-default">
							<span class="glyphicon glyphicon-remove"></span>
							Salir
						</button>
						<button name="del" class="btn btn-danger" ng-click="delCategory()">
							<span class="glyphicon glyphicon-trash"></span>
							Eliminar
						</button>
					</p>
				</div>
			</div>
		</div>
	</div>
	<!-- endblock -->
  <script src="<?php echo \Helpers\Url::pathPublic('vendor'); ?>angularjs/angular.min.js"></script>
	<script src="<?php echo \Helpers\Url::pathPublic('js'); ?>admin/category.js"></script>
</section>