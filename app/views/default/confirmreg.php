<div class="content_wrapper">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.0.1/sweetalert.min.css">
  <br>
  <br>
  <br>
  <br>
  <div class="row">
    <div class="col-md-6 col-md-offset-3">
      <div class="well">
        <div class="panel-body">
          <h4>Gracias por registrarte!</h4>
          <p>
            Hemos enviado un correo de confirmacion a la direccion proporcionada, en la cual encontrara un enlace que validara sus datos.
          </p>
          <a href="<?php echo DIR; ?>login" class="btn btn btn-sm btn-primary"><span class="fa fa-user"></span> Iniciar Sesion</a>
        </div>
      </div>
    </div>
  </div>
</div>