<link rel="stylesheet" href="<?php echo \Helpers\Url::pathPublic('vendor'); ?>sweetalert/dist/sweetalert.css">
<div class="content_wrapper" ng-app="chpapp" ng-controller="chpCtrl">
    <br>
    <br>
    <br>
    <br>
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="well" ng-model="pp">
            <?php if ($data['csrfval']) { ?>
                <h2>Cambie su contraseña para su cuenta de <?php echo $data['config']->sitename; ?></h2>
                <br>
                <input type="hidden" name="name" ng-model="name" ng-init="name='<?php echo $data['config']->sitename; ?>'">
                <form action="">
                <div class="form-group">
                    <label>Ingresa su contraseña</label>
                    <input type="password" name="pass" ng-model="pass" ng-keyup="change($event)" class="form-control">
                </div>
                <div class="form-group">
                    <label>Vuelva a ingresar su contraseña</label>
                    <input type="password" name="repass" ng-model="repass" ng-keyup="change($event)" class="form-control" data-validation="confirmation" data-validation-confirm="pass">
                </div>
                <div class="form-group">
                    <input type="hidden" ng-model="csrf" ng-init="csrf = '<?php echo $data['csrf']; ?>'">
                    <input type="hidden" ng-model="mail" ng-init="mail = '<?php echo $data['mail']; ?>'">
                    <button type="button" class="btn btn-primary" type="button" name="submit" value="true" ng-model="bsend" ng-disabled="bsend" ng-click="changePasswd($event);">
                        <span>Cambiar Contraseña</span>
                    </button>
                    <!-- <button type="button" ng-click="test()">test</button> -->
                </div>
                </form>
            <?php }else{ ?>
                <div class="jumbotron">
                    <h1><i class="glyphicon glyphicon-warning-sign"></i></h1>
                    <p>Los datos enviados no son validos o ya han cadudado, por favor realize el proceso de recuperación nuevamente.</p>
                    <p><a class="btn btn-primary btn-sm" href="/pwd/recovery" role="button">Solitar recuperación</a></p>
                </div>
            <?php } ?>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo \Helpers\Url::pathPublic('vendor'); ?>sweetalert/dist/sweetalert.min.js"></script>
<script src="<?php echo \Helpers\Url::pathPublic('vendor'); ?>jquery-form-validator/form-validator/jquery.form-validator.min.js"></script>
<link href="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.2.8/theme-default.min.css"
    rel="stylesheet" type="text/css" />
<script src="<?php echo \Helpers\Url::pathPublic('vendor'); ?>angular/angular.min.js"></script>
<script src="<?php echo \Helpers\Url::pathPublic('js'); ?>changepwd.js"></script>