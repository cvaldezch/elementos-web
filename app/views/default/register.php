<div class="content_wrapper">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.0.1/sweetalert.min.css">
  <link href="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.2.8/theme-default.min.css"
    rel="stylesheet" type="text/css" />
  <br>
  <br>
  <br>
  <br>
  <div class="row">
    <div class="col-md-6 col-md-offset-3">
      <div class="well">
        <h2>Únete a Elementos</h2>
        <form id="frmregister" action="" method="post">
          <div class="form-group">
            <label>Correo</label>
            <div class="input-group vemail">
              <input type="email" name="email" class="form-control" placeholder="ejemplo@dominio.com" data-validation="email" data-validation-error-msg="Ingrese una cuenta de correo valida.">
              <span class="input-group-addon">@</span>
            </div>
          </div>
          <div class="form-group">
            <label for="">Nombres</label>
            <div class="input-group vnames">
              <input type="text" name="firstname" class="form-control" maxlength="16" placeholder="Nombres">
              <span class="input-group-addon">
                <span class="fa fa-font"></span>
              </span>
            </div>
          </div>
          <div class="form-group">
            <label for="">Apellidos</label>
            <div class="input-group vlastnames">
              <input type="text" name="lastname" class="form-control" maxlength="16" placeholder="Apellidos">
              <span class="input-group-addon">
                <span class="fa fa-font"></span>
              </span>
            </div>
          </div>
          <div class="form-group">
            <label for="">Usuario</label>
            <div class="input-group vuser">
              <input type="text" name="user" class="form-control" maxlength="16" placeholder="Usuario" data-validation="length" data-validation-length="1-16" data-validation-error-msg="Ingrese un usuario max. 16 caracteres.">
              <span class="input-group-addon">
                <span class="fa fa-user"></span>
              </span>
            </div>
          </div>
          <div class="form-group">
            <label for="">Contraseña <small>(Min 8 caracteres)</small></label>
            <div class="input-group">
              <input type="password" name="passwd" class="form-control" title="Ingrese Password" data-validation="strength" data-validation-strength="1" data-validation-error-msg="debede ingresar">
              <span class="input-group-addon">
                <span class="fa fa-unlock-alt">
              </span>
            </div>
          </div>
          <hr>
          <div class="form-group">
            <label for="">Repetir Contraseña</label>
            <div class="input-group">
              <input type="password" name="repeat" class="form-control" title="Ingrese Password" data-validation="confirmation" data-validation-confirm="passwd">
              <span class="input-group-addon">
                <span class="fa fa-unlock-alt"></span>
              </span>
            </div>
          </div>
          <input type="hidden" name="ruser" value="true">
          <div class="form-group">
            <button type="submit" name="ruser" value="true" class="btn btn-block btn-primary">
              <span>Regístrate</span>
            </button>
          </div>
        </form>
        <small>
          Al registrarte, aceptas las Condiciones de Servicio y la Política de Privacidad, incluyendo el Uso de Cookies.
        </small>
      </div>
    </div>
  </div>
</div>
<input type="hidden" name="dname" value="<?php echo $data['config']->sitename; ?>">
<?php
  echo \Helpers\Assets::js(array(
    '//cdnjs.cloudflare.com/ajax/libs/sweetalert/1.0.1/sweetalert.min.js',
    '//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.2.1/jquery.form-validator.min.js',
  ));
?>
<script type="text/javascript" src="<?php echo DIR; ?>app/public/js/register.js"></script>