        <link rel="stylesheet" type="text/css" href="<?php echo \Helpers\Url::pathPublic('vendor') . 'sweetalert/dist/sweetalert.css'; ?>">
        <link rel="stylesheet" href="<?php echo \Helpers\Url::pathPublic('css') . 'details.css'; ?>">
        <link rel="stylesheet" href="<?php echo \Helpers\Url::pathPublic('css') . 'lposts.css'; ?>">
        <script type="text/javascript" src="<?php echo \Helpers\Url::pathPublic('js') . 'index.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo \Helpers\Url::pathPublic('vendor') . 'sweetalert/dist/sweetalert.min.js'; ?>"></script>
        <div class="content_wrapper">
          <br><br><br><br>
          <br>
          <br>
          <br>
            <div class="content">
              <h2><?php echo '::' . strtoupper($data['group']) . '::' . strtoupper($data['section']); ?></h2>
                <div class="real_content">
                  <div class="content-posts">
                    <?php
                      $count = 0;
                      foreach ($data['posts'] as $row) {
                        if ($count==0) {
                    ?>
                            <div class="post-content first-post" data-ref="<?php echo DIR . $row->posts_id . '/' . preg_replace('([^A-Za-z0-9])', '-', $row->title); ?>">
                              <img src="<?php echo DIR . 'app/' . $row->image; ?>" alt="responsive">
                              <span class="post-date">
                                <i class="fa fa-bookmark"></i>
                                <small>
                                <?php
                                  //echo date('H:i A', strtotime($row->publish));
                                  //echo gettype($row->publish);
                                  echo date('d/m/Y', strtotime($row->publish));
                                ?>
                                </small>
                              </span>
                              <h2 class="post-title">
                                <small><?php echo mb_strtoupper($row->title, 'UTF-8'); ?></small>
                              </h2>
                              <a class="read-more" href="<?php echo DIR . $row->posts_id . '/' . preg_replace('([^A-Za-z0-9])', '-', $row->title); ?>">Leer Más</a>
                            </div>
                    <?php
                        }else{
                    ?>
                            <div class="post-content" data-ref="<?php echo DIR . $row->posts_id . '/' . preg_replace('([^A-Za-z0-9])', '-', $row->title); ?>">
                              <img src="<?php echo DIR . 'app/' . $row->image; ?>" alt="responsive">
                              <span class="post-date">
                                <i class="fa fa-bookmark"></i>
                                <small>
                                <?php
                                  //echo date('H:i A', strtotime($row->publish));
                                  //echo gettype($row->publish);
                                  echo date('d/m/Y', strtotime($row->publish));
                                ?>
                                </small>
                              </span>
                              <h2 class="post-title">
                                <small><?php echo mb_strtoupper($row->title, 'UTF-8'); ?></small>
                              </h2>
                              <a class="read-more" href="<?php echo DIR . $row->posts_id . '/' . preg_replace('([^A-Za-z0-9])', '-', $row->title); ?>">Leer Más</a>
                            </div>
                    <?php
                        }
                        $count++;
                    ?>
                      <!-- <div class="main_home">
                        <div class="post">
                          <h3 class=" hidden-xs visible-sm-* visible-md-* visible-lg-*">
                            <a href="<?php //echo DIR . 'blog/' . $row->posts_id . '/' . str_replace(' ', '_', $row->title); ?>"><?php// echo $row->title; ?></a>
                            <div class="author_dates_comments">

                                 <span class="author"><?php //echo $row->name; ?></span>
                                <span class="dates"><?php //echo date('H:i A', strtotime($row->publish)); ?></span> -->
                                <!-- <span class="comments">
                                    <a href="#" class="comment_links">13 Comments</a>
                                </span>
                            </div>
                          </h3>
                          <div class="post-body hidden-xs visible-sm-* visible-md-* visible-lg-*">
                            <div>
                              <span>
                              <?php // if (is_null($row->image)){ ?>
                                <img class="img-responsive img-circle imgpublish" src="<?php //echo DIR . "app/public/images/no-image.png"; ?>"  alt="Responsive image" data-src="<?php // echo DIR . 'blog/' . $row->posts_id . '/' . str_replace(' ', '_', $row->title); ?>">
                              <?php //}else{ ?>
                                <img class="img-responsive img-circle imgpublish" src="<?php //echo DIR . "app/" . $row->image; ?>" alt="Responsive image" data-src="<?php // echo DIR . 'blog/' . $row->posts_id . '/' . str_replace(' ', '_', $row->title); ?>">
                              <?php// } ?>
                              </span>
                              <a class="lredirect" href="<?php // echo DIR . 'blog/' . $row->posts_id . '/' . str_replace(' ', '_', $row->title); ?>">
                                <p class="text-justify">
                                <br>
                                  <?php
                                    // echo $row->reference;
                                  ?>
                                  ...
                                </p>
                              </a>
                            </div>
                            <div class="readmore">
                                <a href="<?php // echo DIR . 'blog/' . $row->posts_id . '/' . str_replace(' ', '_', $row->title); ?>">Leer más</a>
                            </div>
                            <div class="clear"></div> -->
                          <!-- </div> -->
                            <!-- block mobile -->

                            <!-- endblock mobile -->
                       <!--  </div>
                      </div> -->
                    <?php } ?>
                  </div>
                    <?php echo $data['page_links']; ?>
                </div>
                <div class="sidebar_wrapper">
                    <!-- <aside class="search-blog">
                        <h3 class="search-title">
                            Buscar en el Blog!
                        </h3>
                        <div class="search-from">
                            <form action="" method="GET">
                                <div class="input_wrapper">
                                    <span class="input_title">Buscar</span>
                                    <input type="text" class="input_field">
                                    <div class="clear"></div>
                                </div>
                                <input type="submit" class="submit_button" value="Enviar">
                            </form>
                        </div>
                    </aside> -->
                    <div class="row">
                      <div class="col-md-12">
                        <div id="fb-root"></div>
                        <script>(function(d, s, id) {
                          var js, fjs = d.getElementsByTagName(s)[0];
                          if (d.getElementById(id)) return;
                          js = d.createElement(s); js.id = id;
                          js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.4&appId=485204271650665";
                          fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));</script>
                        <!-- <div class="fb-page" data-href="https://www.facebook.com/elementospe" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/elementospe"><a href="https://www.facebook.com/elementospe">ELEMENTOS</a></blockquote></div></div> -->
                        <div class="fb-con">
                          <h4>Facebook</h4>
                          <div class="fb-page" data-href="https://www.facebook.com/ELEMENTOSPERU" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/ELEMENTOSPERU"><a href="https://www.facebook.com/ELEMENTOSPERU">ELEMENTOS</a></blockquote></div></div>
                        </div>
                        <br>
                        <br>
                        <br>
                      </div>
                      <div class="col-md-12">
                        <div class="tw-con">
                          <h4>Twitter</h4>
                          <a class="twitter-timeline" href="https://twitter.com/elementospe" data-widget-id="677933962484916226">Tweets por el @elementospe.</a>
                          <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <br>
                        <br>
                        <br>
                        <div class="panel panel-success">
                          <div class="panel-footer">
                            Publicaciones Populares
                          </div>
                          <?php foreach ($data['popular'] as $row) { ?>
                          <div class="media">
                            <div class="media-left">
                              <a href="<?php echo DIR . $row->posts_id .'/'. preg_replace('([^A-Za-z0-9])', '', $row->title);?>">
                                <img class="media-object" src="<?php echo DIR . 'app/' . $row->image; ?>" alt="<?php echo $row->title; ?>">
                              </a>
                            </div>
                            <div class="media-body">
                              <a href="<?php echo DIR . $row->posts_id .'/'. preg_replace('([^A-Za-z0-9])', '-', $row->title);?>">
                                <h3 class="media-heading" style="text-align: left;"><?php echo $row->title; ?></h3>
                                <!-- <div class="media-text hidden-xs hidden-sm"><?php // echo $row->reference ?></div> -->
                                <!-- <div class="media-text visible-xs-* visible-sm-* hidden-lg hidden-md"><?php // echo  substr($row->reference, 0, 100); ?>...</div> -->
                              </a>
                            </div>
                          </div>
                          <?php } ?>
                        </div>
                      </div>
                    </div>
                </div>
                <!-- <div class="clear"></div> -->
                </div>
            </div>
        </div>
    </section>
    <!-- block social -->
    <?php
      if (!\Helpers\Session::get('social')) { ?>
        <div class="content-social">
            <div class="wrapper_social">
                <div class="row">
                    <div class="col-md-12 hidden-xs">
                      <h3 class="text-white social-text">
                        ¡Siguenos y enterate de nuestas ultimas noticias!
                      </h3>
                    </div>
                    <div class="col-md-6">
                      <div class="social_twitter">
                        <a href="https://twitter.com/elementospe">
                          <span class="fa fa-twitter"></span>
                        </a>
                      </div>
                    </div>
                    <div class="col-md-6">
                        <div class="social_instagram">
                          <a href="https://instagram.com/elementos_pe/">
                            <span class="fa fa-instagram"></span>
                          </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      <?php
        \Helpers\Session::set('social', true);
        }
      ?>
    <!-- end block -->
    <input type="hidden" name="uri" value="<?php echo DIR; ?>">
    <script>
      $(function () {
        $(".imgpublish").click(function  (event) {
          location.href = this.getAttribute("data-src");
        }).css("cursor", "pointer");
        setTimeout(function  () {
            $(".content-social").hide()
        }, 5000)
      });
    </script>