        <link rel="stylesheet" type="text/css" href="<?php echo \Helpers\Url::pathPublic('vendor') . 'sweetalert/dist/sweetalert.css'; ?>">
        <link rel="stylesheet" href="<?php echo \Helpers\Url::pathPublic('css') . 'detailsasds.css'; ?>">
        
        <script src="<?php echo \Helpers\Url::pathPublic('vendor'); ?>mustache/mustache.min.js"></script>
        <script type="text/javascript" src="<?php echo \Helpers\Url::pathPublic('js') . 'index.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo \Helpers\Url::pathPublic('vendor') . 'sweetalert/dist/sweetalert.min.js'; ?>"></script>
        <script type="text/javascript" async src="https://platform.twitter.com/widgets.js"></script>
        <link rel="stylesheet" href="<?php echo \Helpers\Url::pathPublic('css') . 'lposts.css'; ?>">
        <div class="clear"></div>
        <div class="content_search" id="search-desktop">
          <form class="form-horizontal" onsubmit="event.preventDefault();">
            <div class="form-group">
              <!-- <label class="col-md-2 control-label" for="search">Buscar</label> -->
              <div class="col-md-8 col-md-offset-2 input-group">
                  <input type="text" class="form-control" placeholder="Buscar" id="search">
                  <span class="input-group-addon">
                    <i class="glyphicon glyphicon-search"></i>
                  </span>
              </div>
            </div>
          </form>
        </div>
        <div class="content_wrapper">
            <div class="content">
                <div class="real_content">
                  <div class="content-posts">
                    <?php
                      $count = 0;
                      foreach ($data['posts'] as $row) {
                        if ($count==0) {
                    ?>
                            <div class="post-content first-post" data-ref="<?php echo DIR . $row->posts_id . '/' . preg_replace('([^A-Za-z0-9])', '-', $row->title); ?>">
                              <img src="<?php echo DIR . 'app/' . $row->image; ?>" alt="responsive">
                              <span class="post-date">
                                <i class="fa fa-bookmark"></i>
                                <small>
                                <?php
                                  //echo date('H:i A', strtotime($row->publish));
                                  //echo gettype($row->publish);
                                  echo date('d/m/Y', strtotime($row->publish));
                                ?>
                                </small>
                              </span>
                              <h2 class="post-title">
                                <small><?php echo mb_strtoupper($row->title, 'UTF-8'); ?></small>
                              </h2>
                              <a class="read-more" href="<?php echo DIR . $row->posts_id . '/' . preg_replace('([^A-Za-z0-9])', '-', $row->title); ?>">Leer Más</a>
                            </div>
                    <?php
                        }else{
                    ?>
                            <div class="post-content" data-ref="<?php echo DIR . $row->posts_id . '/' . preg_replace('([^A-Za-z0-9])', '-', $row->title); ?>">
                              <img src="<?php echo DIR . 'app/' . $row->image; ?>" alt="responsive">
                              <span class="post-date">
                                <i class="fa fa-bookmark"></i>
                                <small>
                                <?php
                                  //echo date('H:i A', strtotime($row->publish));
                                  //echo gettype($row->publish);
                                  echo date('d/m/Y', strtotime($row->publish));
                                ?>
                                </small>
                              </span>
                              <h2 class="post-title">
                                <small><?php echo mb_strtoupper($row->title, 'UTF-8'); ?></small>
                              </h2>
                              <a class="read-more" href="<?php echo DIR . $row->posts_id . '/' . preg_replace('([^A-Za-z0-9])', '-', $row->title); ?>">Leer Más</a>
                            </div>
                    <?php
                        }
                        $count++;
                      }
                    ?>
                  </div>
                  <div class="align-center">
                    
                  </div>
                    <?php // echo $data['page_links']; ?>
                </div>
                <div class="sidebar_wrapper">
                    <!-- <aside class="search-blog">
                        <h3 class="search-title">
                            Buscar en el Blog!
                        </h3>
                        <div class="search-from">
                            <form action="" method="GET">
                                <div class="input_wrapper">
                                    <span class="input_title">Buscar</span>
                                    <input type="text" class="input_field">
                                    <div class="clear"></div>
                                </div>
                                <input type="submit" class="submit_button" value="Enviar">
                            </form>
                        </div>
                    </aside> -->
                    <div class="row">
                      <div class="col-md-12">
                        <div id="fb-root"></div>
                        <script>(function(d, s, id) {
                          var js, fjs = d.getElementsByTagName(s)[0];
                          if (d.getElementById(id)) return;
                          js = d.createElement(s); js.id = id;
                          js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.4&appId=485204271650665";
                          fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));</script>
                        <!-- <div class="fb-page" data-href="https://www.facebook.com/elementospe" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/elementospe"><a href="https://www.facebook.com/elementospe">ELEMENTOS</a></blockquote></div></div> -->
                        <div class="fb-con">
                          <h4>Facebook</h4>
                          <div class="fb-page" data-href="https://www.facebook.com/ELEMENTOSPE" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/ELEMENTOSPE"><a href="https://www.facebook.com/ELEMENTOSPE">ELEMENTOS</a></blockquote></div></div>
                        </div>
                        <!-- <br>
                        <br>
                        <br> -->
                      </div>
                      <!-- <div class="col-md-12">
                        <iframe src="https://twitter.com/intent/follow?screen_name=elementospe" border="0"></iframe>
                      </div> -->
                      <!-- div class="col-md-12">
                        <div class="tw-con">
                          <h4>Twitter</h4>
                          <a class="twitter-timeline" href="https://twitter.com/elementospe" data-widget-id="677933962484916226">Tweets por el @elementospe.</a>
                          <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <br>
                        <br>
                        <br>
                        <div class="inst-con">
                          <h4>Instagram</h4>
                          <blockquote class="instagram-media" data-instgrm-captioned data-instgrm-version="6" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:50.0% 0; text-align:center; width:100%;"> <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAAGFBMVEUiIiI9PT0eHh4gIB4hIBkcHBwcHBwcHBydr+JQAAAACHRSTlMABA4YHyQsM5jtaMwAAADfSURBVDjL7ZVBEgMhCAQBAf//42xcNbpAqakcM0ftUmFAAIBE81IqBJdS3lS6zs3bIpB9WED3YYXFPmHRfT8sgyrCP1x8uEUxLMzNWElFOYCV6mHWWwMzdPEKHlhLw7NWJqkHc4uIZphavDzA2JPzUDsBZziNae2S6owH8xPmX8G7zzgKEOPUoYHvGz1TBCxMkd3kwNVbU0gKHkx+iZILf77IofhrY1nYFnB/lQPb79drWOyJVa/DAvg9B/rLB4cC+Nqgdz/TvBbBnr6GBReqn/nRmDgaQEej7WhonozjF+Y2I/fZou/qAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div> <p style=" margin:8px 0 0 0; padding:0 4px;"> <a href="https://www.instagram.com/p/BECd7wTsQPM/" style=" color:#000; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none; word-wrap:break-word;" target="_blank">#frases #domingo #elementos_pe</a></p> <p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;">Una foto publicada por ELEMENTOS (@elementos_pe) el <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2016-04-10T23:41:52+00:00">10 de Abr de 2016 a la(s) 4:41 PDT</time></p></div></blockquote>
                          <blockquote class="instagram-media" data-instgrm-captioned data-instgrm-version="6" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:50.0% 0; text-align:center; width:100%;"> <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAAGFBMVEUiIiI9PT0eHh4gIB4hIBkcHBwcHBwcHBydr+JQAAAACHRSTlMABA4YHyQsM5jtaMwAAADfSURBVDjL7ZVBEgMhCAQBAf//42xcNbpAqakcM0ftUmFAAIBE81IqBJdS3lS6zs3bIpB9WED3YYXFPmHRfT8sgyrCP1x8uEUxLMzNWElFOYCV6mHWWwMzdPEKHlhLw7NWJqkHc4uIZphavDzA2JPzUDsBZziNae2S6owH8xPmX8G7zzgKEOPUoYHvGz1TBCxMkd3kwNVbU0gKHkx+iZILf77IofhrY1nYFnB/lQPb79drWOyJVa/DAvg9B/rLB4cC+Nqgdz/TvBbBnr6GBReqn/nRmDgaQEej7WhonozjF+Y2I/fZou/qAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div> <p style=" margin:8px 0 0 0; padding:0 4px;"> <a href="https://www.instagram.com/p/BEBripOMQOr/" style=" color:#000; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none; word-wrap:break-word;" target="_blank">#frases #elementos_pe</a></p> <p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;">Una foto publicada por ELEMENTOS (@elementos_pe) el <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2016-04-10T16:21:32+00:00">10 de Abr de 2016 a la(s) 9:21 PDT</time></p></div></blockquote>
                          <script async defer src="//platform.instagram.com/en_US/embeds.js"></script>
                          <!-- <iframe src="http://widget.websta.me/in/elementos_pe/?r=1&w=1&h=3&b=0&p=2&sb=off" allowtransparency="true" frameborder="0" scrolling="yes" style="border:none; overflow:none;width:100%; height: 456px" ></iframe> <!-- websta - websta.me 
                        </div>
                      </div> -->
                      <div class="col-md-12">
                        <br>
                        <br>
                        <br>
                        <div class="panel panel-success">
                          <div class="panel-footer">
                            Publicaciones Populares
                          </div>
                          <?php foreach ($data['popular'] as $row) { ?>
                          <div class="media">
                            <div class="media-left">
                              <a href="<?php echo DIR . $row->posts_id .'/'. preg_replace('([^A-Za-z0-9])', '-', $row->title);?>">
                                <img class="media-object" src="<?php echo DIR . 'app/' . $row->image; ?>" alt="<?php echo $row->title; ?>">
                              </a>
                            </div>
                            <div class="media-body">
                              <a href="<?php echo DIR . $row->posts_id .'/'. preg_replace('([^A-Za-z0-9])', '-', $row->title);?>">
                                <h3 class="media-heading" style="text-align: left;"><?php echo $row->title; ?></h3>
                                <!-- <div class="media-text hidden-xs hidden-sm"><?php // echo $row->reference ?></div> -->
                                <!-- <div class="media-text visible-xs-* visible-sm-* hidden-lg hidden-md"><?php // echo  substr($row->reference, 0, 100); ?>...</div> -->
                              </a>
                            </div>
                          </div>
                          <?php } ?>
                        </div>
                      </div>
                    </div>
                </div>
                <!-- <div class="clear"></div> -->
                </div>
            </div>
        </div>
    </section>
    <!-- block social -->
    <?php
      if (!\Helpers\Session::get('social')) { ?>
        <!--<div class="content-suscribe">
          <div class="wrapper_suscribe">
            <div class="suscribe-header">
              <div class="suscribe-logo">-->
                <!--<img src="<?php // echo \Helpers\Url::pathPublic('images') . 'elements.svg'; ?> class="img-responsive">-->
              <!--</div>
              <h1>
                Suscribete y síguenos en nuestras redes sociales y enterate de nuestras últimas publicaciones.
              </h1>
              <hr>
            </div>
            <div class="suscribe-body">
              <div class="suscribe-follow">
                <div class="twitter-so">
                  <span class="fa fa-twitter"></span>
                    <small>0k</small>
                </div>
                <div class="instagram-so">
                  <span class="fa fa-instagram"></span>
                    <small>0k</small>
                </div>
                <div class="facebook-so fb-like" data-href="https://facebook.com/elementos_pe" data-layout="standard" data-action="like" data-show-faces="false" data-share="false">
                  <span class="fa fa-facebook"></span>
                    <small>0k</small>
                </div>
              </div>
              <div class="suscribe-email">
                <div class="form-group">
                  <div class="input-group">
                    <span class="input-group-addon">
                      @
                    </span>
                    <input class="form-control" name="emails" placeholder="ejemplo@domain.com">
                  </div>
                </div>
                <div class="form-group">
                  <div class="btn-group btn-group-justified">
                    <div class="btn-group">
                      <button class="btn btn-danger" type="button" name="btnSuscribe">
                        Suscríbete
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="suscribe-foot">
              <a href="#">
                No Gracias, NO QUIERO SUSCRIBIRME!
              </a>
            </div>
          </div>
        </div>-->
      <?php
        \Helpers\Session::set('social', true);
        }
      ?>
    <!-- end block -->
    <input type="hidden" name="uri" value="<?php echo DIR; ?>">
