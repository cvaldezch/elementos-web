<?php

header('Content-type: text/html; charset=utf-8');

use Helpers\Assets;
use Helpers\Url;

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <title><?php echo $data['title']; ?></title>
  <link rel="shortcut icon" href="<?php echo Url::templatePath(); ?>images/favicon.ico">
  <?php
        Assets::css(array(
            Url::pathPublic('vendor') . 'bootstrap/dist/css/bootstrap.min.css',
            Url::pathPublic('vendor') . 'font-awesome/css/font-awesome.min.css',
            Url::pathPublic('vendor') . 'startbootstrap-simple-sidebar/css/simple-sidebar.css',
            Url::pathPublic('css') . 'sidebar.css',
        ));
    ?>
</head>
<body>
  <div id="wrapper">
    <div class="page-content-header">
      <a href="<?php echo DIR; ?>">
          <img src="<?php echo Url::pathPublic('images') . 'elements.svg' ?>" alt="Elementos" class="img-circle img-responsive">
          <h1>elementos</h1>
      </a>
  </div>
  <div id="page-content-wrapper">
    <div class="container-fluid">
      <h3>Recuperar Contraseña</h3>
      <form action="" method="post" id="formrecover">
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <div class="col-md-3">
                <label class="control-label">Nuevo Contraseña</label>
                <input type="password" class="form-control" name="passwd" data-validation="length alphanumeric" data-validation-length="6" data-validation-error-msg="Ingrese su nueva contraseña" name="passwd">
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="col-md-3">
              <label class="control-label">Confirmar Contraseña</label>
              <input type="password" class="form-control" name="repeat" data-validation="confirmation" data-validation-confirm="passwd" data-validation-error-msg="Ingrese la confirmacion de la contraseña">
            </div>
          </div>
          <div class="col-md-12">
            <input type="hidden" name="csrf" value="<?php echo $data['csrf']; ?>">
            <input type="hidden" name="email" value="<?php echo $data['email']; ?>">
          </div>
          <div class="col-md-12">
            <div class="col-md-3">
              <br>
              <button class="btn btn-primary btn-justified">
                Cambiar Contraseña
              </button>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
  </div>
  <input type="hidden" name="base" value="<?php echo DIR; ?>">
  <input type="hidden" name="uri" value="<?php echo DIR; ?>">
  <!-- implement requirejs -->
  <script type="text/javascript" data-main="<?php echo Url::pathPublic('js') . 'recover'; ?>" src="<?php echo Url::pathPublic('vendor') . 'requirejs/require.js' ?>"></script>
</body>
</html>