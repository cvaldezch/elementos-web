<div class="content_wrapper" ng-app="reqFP" ng-controller="reqFPCtrl">
    <br>
    <br>
    <br>
    <br>
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="well" ng-model="pp" ng-init="pp=false" ng-hide="pp">
                <h2>Encuentra tu cuenta de <?php echo $data['config']->sitename; ?></h2>
                <br>
                <input type="hidden" id="name" ng-model="name" ng-init="name=<?php echo $data['config']->sitename; ?>" value="<?php echo $data['config']->sitename; ?>">
                <div class="form-group">
                    <label>Ingresa tu correo electrónico.</label>
                    <input type="email" name="mail" id="mail" ng-model="mail" class="form-control" data-blur="validEmail($event);" ng-keyup="keyMail($event);">
                </div>
                <div class="form-group">
                    <button type="button" class="btn btn-primary" type="button" name="submit" value="true" ng-disabled="vemail" ng-click="sendEmail($event);">
                        <span>Enviar Solicitud</span>
                    </button>
                </div>
            </div>
            <div class="panel panel-primary" ng-model="psend" ng-init="psend=false" ng-show="psend">
                <div class="panel-body">
                    <h4>Se ha enviado un correo a tu cuenta para que puedas restablecer su contraseña.</h4>
                </div>
            </div>
            <div class="panel panel-danger" ng-model="pfail" ng-init="pfail=false" ng-show="pfail">
                <div class="panel-body">
                    <h4>No se ha podido enviar el correo para recuperar.</h4>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo \Helpers\Url::pathPublic('vendor'); ?>angular/angular.min.js"></script>
<script src="<?php echo \Helpers\Url::pathPublic('js'); ?>requestforgetpwd.js"></script>