<div class="content_wrapper">
  <br>
  <br>
  <br>
  <br>
  <div class="row">
    <div class="col-md-6 col-md-offset-3">
      <div class="well">
        <div class="panel panel-info">
          <div class="panel-body">
            <h3>Bienvenido a <q>Elementos</q></h3>
            <p>
              Gracias por registrarte, tus datos han sido validados correctamnte, ahora ya eres parte de esta gran comunidad.
            </p>
            <p>
              <a href="<?php echo DIR; ?>" class="btn btn-primary">
                <span class="fa fa-home"></span>
                <span>Inicio</span>
              </a>
              <a href="<?php echo DIR . 'login'; ?>" class="btn btn-info">
                <span class="fa fa-user"></span>
                <span>Ingresa</span>
              </a>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php header("refresh:15; url=" . DIR . "login"); ?>