<?php
header('Content-type: text/html; charset=utf-8');

use Helpers\Assets;
use Helpers\Session;
use Helpers\Url;
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><?php echo $data['title']; ?></title>
    <link rel="shortcut icon" href="<?php echo Url::templatePath(); ?>images/favicon.ico">
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php
        Assets::css(array(
            Url::pathPublic('vendor') . 'bootstrap/dist/css/bootstrap.min.css',
            Url::pathPublic('vendor') . 'font-awesome/css/font-awesome.min.css',
            Url::pathPublic('vendor') . 'startbootstrap-simple-sidebar/css/simple-sidebar.css',
            Url::pathPublic('vendor') . 'sweetalert/dist/sweetalert.css',
            Url::pathPublic('vendor') . 'chosen/chosen.css',
            Url::pathPublic('vendor') . 'clockpicker/dist/bootstrap-clockpicker.min.css',
            Url::pathPublic('vendor') . 'jquery-ui/themes/smoothness/jquery-ui.min.css',
            Url::pathPublic('static') . 'nailthumb/jquery.nailthumb.1.1.css',
            Url::pathPublic('css') . 'sidebar.css',
        ));
    ?>
</head>
<body>
    <div id="wrapper">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    <a href="">
                      <?php
                        if (!is_null($data['profile'][0]->photo)){
                      ?>
                        <img src="<?php echo DIR .'app/'. $data['profile'][0]->photo; ?>" alt="Elementos" class="img-thumbnail img-responsive photo-profile">
                      <?php }else{ ?>
                        <img src="<?php echo Url::pathPublic('images') . 'profile.jpg' ?>" alt="Elementos" class="img-thumbnail img-responsive photo-profile">
                      <?php } ?>
                    </a>
                </li>
                <li>
                    <a href="<?php echo DIR; ?>">Inicio</a>
                </li>
                <li>
                    <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Perfil</a>
                </li>
                <li>
                    <a href="#account" aria-controls="account" role="tab" data-toggle="tab">Cuenta</a>
                </li>
                <li>
                    <a href="#passwd" aria-controls="passwd" role="tab" data-toggle="tab">Contraseña</a>
                </li>
                <li>
                    <a href="#publiser" aria-controls="publiser" role="tab" data-toggle="tab">
                    <?php if ($data['users'][0]->publish == false) { ?>
                      Solicitud Colaboración
                    <?php }else{ ?>
                      Publicar
                    <?php } ?>
                    </a>
                </li>
                <?php if (Session::get('role') == 'administrator') { ?>
                <li>
                  <a href="<?php echo DIR; ?>admin">Administración</a>
                </li>
                <?php } ?>
                <li>
                    <a href="<?php echo DIR . 'logout'; ?>" >Cerrar Sesión</a>
                </li>
            </ul>
        </div>
        <!-- /#sidebar-wrapper -->
        <div class="page-content-header">
            <a href="<?php echo DIR; ?>">
                <img src="<?php echo Url::pathPublic('images') . 'elementos.svg' ?>" alt="Elementos" class="img-circle img-responsive">
                <h1>elementos</h1>
            </a>
        </div>
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="profile">
                                <form id="formprofile" method="post">
                                    <div class="row">
                                        <div class="col-md-6 col-md-offset-1">
                                            <h3>
                                               Perfil
                                               <br>
                                               <small>
                                                   Cambia tus configuraciones básicas de tu perfil.
                                               </small>
                                            </h3>
                                        </div>
                                        <div class="col-md-6 col-md-offset-1">
                                            <div class="form-group">
                                                <label class="control-label">Nombres</label>
                                                <input type="text" name="firstname" class="form-control" placeholder="Ingrese sus Nombres" value="<?php echo $data['profile'][0]->firstname; ?>">
                                            </div>
                                        </div>
                                        <!-- <div class="col-md-7"></div> -->
                                        <div class="col-md-6 col-md-offset-1">
                                            <div class="form-group">
                                                <label class="control-label">Apellidos</label>
                                                <input type="text" name="lastname" class="form-control" placeholder="Ingrese sus Apellidos" value="<?php echo $data['profile'][0]->lastname; ?>">
                                            </div>
                                        </div>
                                        <!-- <div class="col-md-7"></div> -->
                                        <div class="col-md-6 col-md-offset-1">
                                          <div class="form-group">
                                            <label class="control-label">Correo</label>
                                            <input type="email" class="form-control" name="email" placeholder="Ingrese su correo" value="<?php echo Session::get('email'); ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col-md-6 col-md-offset-1">
                                          <div class="form-group">
                                            <label class="control-label">Sexo</label>
                                            <select name="sex" id="sex" class="form-control">
                                              <option value="">--Seleccione--</option>
                                              <option value="M" <?php if ($data['profile'][0]->sex == 'M') echo 'selected'?>>Masculino</option>
                                              <option value="F" <?php if ($data['profile'][0]->sex == 'F') echo 'selected'?>>Femenino</option>
                                            </select>
                                          </div>
                                        </div>
                                        <div class="col-md-12">
                                            <hr>
                                        </div>
                                        <div class="col-md-6 col-md-offset-1">
                                            <div class="form-group">
                                                <label class="control-label">Vida Real</label>
                                                <textarea name="real" rows="6" class="form-control"><?php echo trim($data['profile'][0]->lifer); ?></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-md-offset-1">
                                            <div class="form-group">
                                                <label class="control-label">Vida Ficticia</label>
                                                <textarea name="fictional" rows="6" class="form-control"><?php echo trim($data['profile'][0]->lifef); ?></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-md-offset-1">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary">
                                                    <span class="fa fa-floppy"></span>
                                                    <span>Guardar Cambios</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="account">
                              <form id="formAccount" method="post">
                                <div class="row">
                                  <div class="col-md-6 col-md-offset-1">
                                      <h3>
                                          Cuenta
                                          <br>
                                          <small>Cambia tus configuraciones básicas de tu cuenta.</small>
                                      </h3>
                                  </div>
                                  <div class="col-md-6 col-md-offset-1">
                                    <label class="control-label">Foto Perfil</label>
                                    <div class="form-group">
                                      <span class="fa fa-spinner fa-pulse fa-4x photo-loagind"></span>
                                      <?php
                                        if ($data['profile'][0]->photo != ''){
                                          echo "<img src=\" ". DIR . "app/" . $data['profile'][0]->photo . "\" data-range=\"". $data['profile'][0]->rphoto."\" class=\"imgsrc img-thumbnail img-responsive\" />";
                                        }else{
                                          echo "<img class=\"imgsrc img-thumbnail img-responsive\" />";
                                        }
                                      ?>
                                      <span class="btn btn-primary btn-file">
                                          Seleccionar <input type="file" id="imgprofile">
                                      </span>
                                      <div class="slider"></div>
                                    </div>
                                        <!-- <img src="<?php // echo Url::pathPublic('images') . 'slider3.jpg' ?>" class="imgcrop"> -->
                                        <!-- <input type="file" class="forn-control" name="photo"> -->
                                  </div>
                                  <div class="col-md-6 col-md-offset-1">
                                      <div class="form-group">
                                          <label class="control-label">Usuario</label>
                                          <input type="text" class="form-control" name="username" value="<?php echo Session::get('username'); ?>" readonly>
                                      </div>
                                  </div>
                                  <div class="col-md-6 col-md-offset-1">
                                      <div class="form-group">
                                          <label class="control-label">Eslogan</label>
                                          <textarea class="form-control" name="slogan" rows="5"><?php echo $data['profile'][0]->slogan; ?></textarea>
                                      </div>
                                  </div>
                                  <div class="col-md-6 col-md-offset-1">
                                      <div class="form-group">
                                          <button type="submit" name="account" class="btn btn-primary">
                                              <span class="fa fa-floppy"></span>
                                              <span>Guardar Cambios</span>
                                          </button>
                                      </div>
                                  </div>
                                </div>
                              </form>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="passwd">
                              <form id="formpasswd" method="post">
                                <div class="row">
                                  <div class="col-md-6 col-md-offset-1">
                                    <h3>
                                      Contraseña
                                      <br>
                                      <small>Cambia tu contraseña actual.</small>
                                    </h3>
                                  </div>
                                  <div class="col-md-6 col-md-offset-1">
                                    <div class="form-group">
                                      <label class="control-label">Contraseña Actual</label>
                                      <input type="password" class="form-control" name="passwd_current" data-validation="length alphanumeric" data-validation-length="6"  data-validation-error-msg="Ingrese su contraseña actual.">
                                      <button type="button" name="recover" class="btn btn-link btn-sm"><small>Olvidaste tu contraseña?</small></button>
                                    </div>
                                  </div>
                                  <div class="col-md-6 col-md-offset-1">
                                    <div class="form-group">
                                      <label class="control-label">Contraseña Nueva</label>
                                      <input type="password" class="form-control" data-validation="length alphanumeric" data-validation-length="6" data-validation-error-msg="Ingrese su nueva contraseña" name="passwd_new">
                                    </div>
                                  </div>
                                  <div class="col-md-6 col-md-offset-1">
                                    <div class="form-group">
                                      <label class="control-label">Confirmar Contraseña</label>
                                      <input type="password" class="form-control" name="repeat" data-validation="confirmation" data-validation-confirm="passwd_new">
                                    </div>
                                  </div>
                                  <div class="col-md-11 col-md-offset-1">
                                    <div class="form-group">
                                      <button class="btn btn-primary" name="passwd" type="submit">Cambiar Contraseña</button>
                                    </div>
                                  </div>
                                </div>
                              </form>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="publiser">
                              <h2>Publicar</h2>
                              <?php if (is_null($data['users'][0]->section)){ ?>
                                <p>
                                Envianos una solicitud para que puedas publicar en nuestro blog.
                                </p>
                                <p>
                                  Solo tienes que enviarnos en que sección te gustaria publicar, y nosotros estaremos comunicandonos contigo al correo proporcionado en el registro.
                                </p>
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <div class="col-md-4">
                                        <label class="control-label">Secciones</label>
                                        <select name="section" class="form-control">
                                          <!-- here list sections -->
                                          <?php foreach ($data['menu'] as $row){ ?>
                                            <optgroup label="<?php echo $row->menu; ?>">
                                              <?php
                                                foreach ($data['detmenu'] as $drow){
                                                  if ($row->menuid == $drow->menuid) {
                                              ?>
                                                <option value="<?php echo $drow->submenuid; ?>"><?php echo $drow->submenu; ?></option>
                                              <?php
                                                  }
                                                }
                                              ?>
                                            </optgroup>
                                          <?php } ?>
                                        </select>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <div class="col-md-4">
                                        <label for="" class="control-label">Comentario</label>
                                        <textarea name="comment" rows="4" class="form-control"></textarea>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <div class="col-md-4">
                                        <div class="btn-group btn-group-justified">
                                          <div class="btn-group">
                                            <button type="button" class="btn btn-primary btn-sendrequest">
                                              Enviar Solicitud de Publicación
                                            </button>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              <?php }else if ($data['users'][0]->publish == true){ ?>
                                <div class="row">
                                  <div class="col-md-12">
                                    <h3>
                                    Crear publicación
                                    <small>Categoria: </small>
                                    </h3>
                                  </div>
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <div class="col-md-4">
                                        <select name="section" class="form-control">
                                        <?php foreach ($data['menu'] as $row){ ?>
                                            <?php
                                              foreach ($data['detmenu'] as $drow){
                                                if ($row->menuid == $drow->menuid && $data['users'][0]->section == $drow->submenuid) {
                                            ?>
                                              <optgroup label="<?php echo $row->menu; ?>">
                                              <option value="<?php echo $drow->submenuid; ?>" data-group="<?php echo $row->menuid; ?>"><?php echo $drow->submenu; ?></option>
                                            <?php
                                                }else{
                                                  continue;
                                                }
                                              }
                                            ?>
                                          </optgroup>
                                        <?php } ?>
                                        </select>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-md-12">
                                    <hr>
                                  </div>
                                  <!-- body Posts -->
                                  <div class="col-md-12">
                                    <div class="btn-group">
                                      <button type="button" class="btn btn-default btnaddPublish">
                                        <span class="glyphicon glyphicon-plus"></span>
                                        <span>Agregar Post</span>
                                      </button>
                                      <button type="button" class="btn btn-default btnListPublish">
                                        <span class="glyphicon glyphicon-refresh"></span>
                                        <span>Lista Post</span>
                                      </button>
                                      <!-- here block Moderator -->

                                      <!-- end block -->
                                    </div>
                                    <div class="panel panel-default">
                                    <div class="panel-body body-add">
                                      <div class="row">
                                          <div class="col-md-12">
                                            <div class="form-gruop">
                                              <div class="btn-group btn-group-justified">
                                                <div class="btn-group">
                                                  <button class="btn btn-primary btn-lg btn-publish">
                                                    <span class="glyphicon glyphicon-floppy-disk"></span>
                                                    Publicar
                                                  </button>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                          <div class="col-md-12">
                                            <div class="form-group">
                                              <label class="control-label">Título</label>
                                              <input type="text" class="form-control" name="title">
                                            </div>
                                          </div>
                                          <div class="col-md-12">
                                            <div class="form-group">
                                              <label class="control-label">Imagen Central</label>
                                              <img src="" class="imgedit img-responsive" name="imgpost" alt="img-responsive" height="120" width="240">
                                              <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                  <div class="text-center">
                                                    <strong>
                                                      <a href="#" class="text-white cimage">
                                                      Seleccionar Aquí <span class="glyphicon glyphicon-paperclip"></span>
                                                      </a>
                                                    </strong>
                                                  </div>
                                                </div>
                                              </div>
                                              <input type="file" class="form-control hide" name="image" accept="image/*">
                                            </div>
                                          </div>
                                          <div class="col-md-12">
                                            <div class="form-group">
                                              <label class="control-label">Contenido</label>
                                              <textarea name="content" class="col-md-12 form-control" rows="20"></textarea>
                                            </div>
                                          </div>
                                          <div class="col-md-12">
                                            <div class="form-group">
                                              <label class="control-label">Fecha de Publicación</label>
                                              <input type="text" class="form-control" maxlength="10" placeholder="aaaa/mm/dd" name="date">
                                            </div>
                                          </div>
                                          <div class="col-md-12">
                                            <div class="form-group">
                                              <label class="control-label">Hora de Publicación</label>
                                              <input type="text" class="form-control clockpick" maxlength="5" placeholder="hh:mm" readonly name="hour">
                                            </div>
                                          </div>
                                          <div class="col-md-12">
                                            <div class="form-group">
                                              <label class="control-label">Etiquetas</label>
                                              <select name="categories" class="chosen-choices" multiple>
                                                <?php foreach ($data['category'] as $row) { ?>
                                                  <option value="<?php echo $row->category_id ?>"><?php echo $row->category; ?></option>
                                                <?php } ?>
                                              </select>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="panel-body body-list">
                                      <table class="table table-responsive table-condensed table-hover table-posts">
                                        <thead>
                                          <tr>
                                            <th>Item</th>
                                            <th>Título</th>
                                            <th>Registrado</th>
                                            <th>Publicado</th>
                                            <th>Acción</th>
                                          </tr>
                                        </thead>
                                        <tbody></tbody>
                                      </table>
                                    </div>
                                  </div>
                                </div>
                              <?php } else { ?>
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="form-grup">
                                      <div class="col-md-4">
                                        <div class="alert alert-info">
                                          <strong>Estamos evaluando tu colaboración.</strong>
                                          <p>
                                            Tu solicitud para poder publicar en <strong>elementos</strong> esta siendo procesada en breve estaras recibiedo un email de confirmcación.
                                          </p>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->
    </div>

    <!-- /#wrapper -->
    <!-- utils -->
    <input type="hidden" name="base" value="<?php echo DIR; ?>users/profile">
    <input type="hidden" name="uri" value="<?php echo DIR; ?>">
    <!-- implement requirejs -->
    <script type="text/javascript" data-main="<?php echo Url::pathPublic('js') . 'mprofile'; ?>" src="<?php echo Url::pathPublic('vendor') . 'requirejs/require.js' ?>"></script>
    <?php
        //Assets::js(array(
            // Url::pathPublic('vendor') . 'jquery/dist/jquery.min.js',
            // Url::pathPublic('vendor') . 'bootstrap/dist/js/bootstrap.min.js',
            // Url::pathPublic('vendor') . 'tinymce/tinymce.min.js',
            // Url::pathPublic('vendor') . 'mustache/mustache.min.js',
            // Url::pathPublic('vendor') . 'chosen/chosen.jquery.min.js',
            // Url::pathPublic('vendor') . 'sweetalert/dist/sweetalert.min.js',
            // Url::pathPublic('vendor') . 'clockpicker/dist/bootstrap-clockpicker.min.js',
            // Url::pathPublic('vendor') . 'jquery-form-validator/form-validator/jquery.form-validator.min.js',
            // Url::pathPublic('vendor') . 'jquery-ui/jquery-ui.min.js',
            // Url::pathPublic('js') . 'profile.js',
        //));
    ?>
    <!-- Menu Toggle Script -->
    <script>
    // $("#menu-toggle").click(function(e) {
    //     e.preventDefault();
    //     $("#wrapper").toggleClass("toggled");
    // });
    </script>
</body>
</html>