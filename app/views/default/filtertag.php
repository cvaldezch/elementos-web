        <link rel="stylesheet" type="text/css" href="<?php echo \Helpers\Url::pathPublic('vendor') . 'sweetalert/dist/sweetalert.css'; ?>">
        <script type="text/javascript" src="<?php echo \Helpers\Url::pathPublic('js') . 'index.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo \Helpers\Url::pathPublic('vendor') . 'sweetalert/dist/sweetalert.min.js'; ?>"></script>
        <div class="clear"></div>
        <br>
        <br>
        <br>
        <br>
        <div class="content_wrapper">
            <div class="content">
                <div class="real_content">
                    <?php
                        foreach ($data['posts'] as $row) {
                    ?>
                    <div class="main_home">
                        <div class="post">
                            <h3 class=" hidden-xs visible-sm-* visible-md-* visible-lg-*">
                                <a href="<?php echo DIR . 'blog/' . $row->posts_id . '/' . str_replace(' ', '_', $row->title); ?>"><?php echo $row->title; ?></a>
                                <div class="author_dates_comments">

                                    <!-- <span class="author"><?php //echo $row->name; ?></span>
                                    <span class="dates"><?php //echo date('H:i A', strtotime($row->publish)); ?></span> -->
                                    <!-- <span class="comments">
                                        <a href="#" class="comment_links">13 Comments</a>
                                    </span> -->
                                </div>
                            </h3>
                            <div class="post-body hidden-xs visible-sm-* visible-md-* visible-lg-*">
                              <div>
                                <span>
                                <?php if (is_null($row->image)){ ?>
                                  <img class="img-responsive img-circle imgpublish" src="<?php echo DIR . "app/public/images/no-image.png"; ?>"  alt="Responsive image" data-src="<?php echo DIR . 'blog/' . $row->posts_id . '/' . str_replace(' ', '_', $row->title); ?>">
                                <?php }else{ ?>
                                  <img class="img-responsive img-circle imgpublish" src="<?php echo DIR . "app/" . $row->image; ?>" alt="Responsive image" data-src="<?php echo DIR . 'blog/' . $row->posts_id . '/' . str_replace(' ', '_', $row->title); ?>">
                                <?php } ?>
                                </span>
                                <a class="lredirect" href="<?php echo DIR . 'blog/' . $row->posts_id . '/' . str_replace(' ', '_', $row->title); ?>">
                                  <p class="text-justify">
                                  <br>
                                    <?php
                                     echo $row->reference;
                                    ?>
                                    ...
                                  </p>
                                </a>
                              </div>
                                <div class="readmore">
                                    <a href="<?php echo DIR . 'blog/' . $row->posts_id . '/' . str_replace(' ', '_', $row->title); ?>">Leer más</a>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="row visible-xs-* visible-sm-* hidden-sm hidden-md hidden-lg">
                            <div class="col-sm-12 col-md-12">
                              <div class="thumbnail">
                                <?php if (is_null($row->image)){ ?>
                                    <img class="img-responsive imgpublish" src="<?php echo DIR . "app/public/images/no-image.png"; ?>"  alt="Responsive image" data-src="<?php echo DIR . 'blog/' . $row->posts_id . '/' . str_replace(' ', '_', $row->title); ?>">
                                <?php }else{ ?>
                                    <img class="img-responsive imgpublish" src="<?php echo DIR . "app/" . $row->image; ?>" alt="Responsive image" data-src="<?php echo DIR . 'blog/' . $row->posts_id . '/' . str_replace(' ', '_', $row->title); ?>">
                                <?php } ?>
                                <div class="caption">
                                  <h3><a class="text-black" href="<?php echo DIR . 'blog/' . $row->posts_id . '/' . str_replace(' ', '_', $row->title); ?>"><?php echo $row->title; ?></a></h3>
                                    <a class="lredirect text-black" href="<?php echo DIR . 'blog/' . $row->posts_id . '/' . str_replace(' ', '_', $row->title); ?>">
                                      <p class="text-justify text-mobile">
                                            <?php
                                             echo $row->reference;
                                            ?>
                                            ...
                                      </p>
                                    </a>
                                    <div class="author_dates_comments">
                                  <!-- <span class="author"><?php // echo $row->name; ?></span> -->
                                  <!-- <span class="dates"><?php // echo date('H:i A', strtotime($row->publish)); ?></span> -->
                                  <!-- <span class="comments">
                                      <a href="#" class="comment_links">13 Comments</a>
                                  </span> -->
                                </div>
                                  <!-- <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p> -->
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                    <?php } ?>
                    <p>
                      <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                      <!-- anuncio1 -->
                      <ins class="adsbygoogle"
                           style="display:block"
                           data-ad-client="ca-pub-8134194566813718"
                           data-ad-slot="2107860788"
                           data-ad-format="auto"></ins>
                      <script>
                      (adsbygoogle = window.adsbygoogle || []).push({});
                      </script>
                    </p>
                    <?php echo $data['page_links']; ?>
                    <div class="panel panel-default hidden-sm hidden-md hidden-lg visible-xs-*">
                      <div class="panel-body">
                        <a href="https://www.facebook.com/mindofbrando" target="_blank">
                          <img class="thumbnail-250 img-responsive" src="<?php echo \Helpers\Url::pathPublic('images'). 'mindofbrando.jpg'; ?>" alt="Mind Of Brando">
                        </a>
                      </div>
                    </div>
                    <div class="panel panel-success">
                      <div class="panel-footer">
                        Publicaciones Populares
                      </div>
                      <?php foreach ($data['popular'] as $row) { ?>
                      <div class="media">
                        <div class="media-left">
                          <a href="<?php echo DIR; ?>blog/<?php echo $row->posts_id .'/'. str_replace(' ', '_', $row->title);?>">
                            <img class="media-object" src="<?php echo DIR . 'app/' . $row->image; ?>" alt="<?php echo $row->title; ?>">
                          </a>
                        </div>
                        <div class="media-body">
                          <a href="<?php echo DIR; ?>blog/<?php echo $row->posts_id .'/'. str_replace(' ', '_', $row->title);?>">
                            <h4 class="media-heading"><?php echo $row->title; ?></h4>
                            <div class="media-text hidden-xs hidden-sm"><?php echo $row->reference ?></div>
                            <div class="media-text visible-xs-* visible-sm-* hidden-lg hidden-md"><?php echo  substr($row->reference, 0, 100); ?>...</div>
                          </a>
                        </div>
                      </div>
                      <?php } ?>
                    </div>

                    <div class="row">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <nav class="nav_social_mov">
                          <ul>
                            <li class="facebook"><a href="https://www.facebook.com/elementospe" target="_blank">
                              <span class="fa fa-facebook"></span>
                            </a></li>
                            <li class="twitter"><a href="https://twitter.com/elementospe" target="_blank">
                              <span class="fa fa-twitter"></span>
                            </a></li>
                            <li class="youtube"><a href="https://www.youtube.com/elementospe" target="_blank">
                              <span class="fa fa-youtube-play"></span>
                            </a></li>
                            <li class="tumblr"><a href="http://elementospe.tumblr.com/" target="_blank">
                              <span class="fa fa-tumblr"></span>
                            </a></li>
                            <li class="instagram"><a href="https://instagram.com/elementos_pe/" target="_blank">
                              <span class="fa fa-instagram"></span>
                            </a></li>
                            <li class="plus"><a href="https://plus.google.com/+ElementospeOficial/posts" target="_blank">
                              <span class="fa fa-google-plus"></span>
                            </a></li>
                          </ul>
                        </nav>
                      </div>
                    </div>
                </div>
                <div class="sidebar_wrapper hidden-xs visible-md-*">
                    <!-- <aside class="search-blog">
                        <h3 class="search-title">
                            Buscar en el Blog!
                        </h3>
                        <div class="search-from">
                            <form action="" method="GET">
                                <div class="input_wrapper">
                                    <span class="input_title">Buscar</span>
                                    <input type="text" class="input_field">
                                    <div class="clear"></div>
                                </div>
                                <input type="submit" class="submit_button" value="Enviar">
                            </form>
                        </div>
                    </aside> -->
                    <div id="fb-root"></div>
                    <script>(function(d, s, id) {
                      var js, fjs = d.getElementsByTagName(s)[0];
                      if (d.getElementById(id)) return;
                      js = d.createElement(s); js.id = id;
                      js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.4&appId=485204271650665";
                      fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));</script>
                    <div class="fb-page" data-href="https://www.facebook.com/elementospe" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/elementospe"><a href="https://www.facebook.com/elementospe">ELEMENTOS</a></blockquote></div></div>

                    <!-- embebed tweet -->

                    <!-- link static -->
                    <br>
                    <br>
                    <a href="https://www.facebook.com/mindofbrando" target="_blank">
                      <img class="thumbnail-250 img-responsive" src="<?php echo \Helpers\Url::pathPublic('images'). 'mindofbrando.jpg'; ?>" alt="Mind Of Brando">
                    </a>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </section>
    <!-- block social -->
    <?php
      if (!\Helpers\Session::get('social')) { ?>
        <div class="content-suscribe">
          <div class="wrapper_suscribe">
            <div class="suscribe-header">
              <div class="suscribe-logo">
                <img src="<?php echo \Helpers\Url::pathPublic('images') . 'elements.svg'; ?>" class="img-responsive">
              </div>
              <h1>
                Suscribete y síguenos en nuestras redes sociales y enterate de nuestras últimas publicaciones.
              </h1>
              <hr>
            </div>
            <div class="suscribe-body">
              <div class="suscribe-follow">
                <div class="twitter-so">
                  <span class="fa fa-twitter"></span>
                    <small>0k</small>
                </div>
                <div class="instagram-so">
                  <span class="fa fa-instagram"></span>
                    <small>0k</small>
                </div>
                <div class="facebook-so fb-like" data-href="https://facebook.com/elementos_pe" data-layout="standard" data-action="like" data-show-faces="false" data-share="false">
                  <span class="fa fa-facebook"></span>
                    <small>0k</small>
                </div>
              </div>
              <div class="suscribe-email">
                <div class="form-group">
                  <div class="input-group">
                    <span class="input-group-addon">
                      @
                    </span>
                    <input class="form-control" name="emails" placeholder="ejemplo@domain.com">
                  </div>
                </div>
                <div class="form-group">
                  <div class="btn-group btn-group-justified">
                    <div class="btn-group">
                      <button class="btn btn-danger" type="button" name="btnSuscribe">
                        Suscríbete
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="suscribe-foot">
              <a href="#">
                No Gracias, NO QUIERO SUSCRIBIRME!
              </a>
            </div>
          </div>
        </div>
      <?php
        \Helpers\Session::set('social', true);
        }
      ?>
    <!-- end block -->
    <input type="hidden" name="uri" value="<?php echo DIR; ?>">