<div class="content_wrapper">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.0.1/sweetalert.min.css">
  <br>
  <br>
  <br>
  <br>
  <div class="row">
    <div class="col-md-6 col-md-offset-3">
      <div class="well">
        <div class="panel-body">
          <h4>No hemos podido realizar su registro!</h4>
          <p>
            Hemos tenido problemas al procesar su correo electronico, vuelva a intentarlo en unos minutos.
          </p>
          <a href="<?php echo DIR; ?>user/register" class="btn btn btn-sm btn-primary"><span class="fa fa-user"></span> Registrate!</a>
        </div>
      </div>
    </div>
  </div>
</div>