var gulp = require('gulp'),
    stylus = require('gulp-stylus'),
    watch = require('gulp-watch')
    nib = require('nib'),
    plumber = require('gulp-plumber');
var coffee = require('gulp-coffee');
var gutil = require('gulp-util');
//var autoprefixer = require('gulp-autoprefixer-core');
// tarea que compila los archivos de stylus a css
gulp.task('stylus2css', function() {
  gulp.src('app/public/**/*.styl')
    .pipe(plumber())
    .pipe(stylus({
        use: nib(),
      }))
    .pipe(gulp.dest('app/public/'));
    console.info("Stylus Compile!!! - " + new Date());
});

// gulp.task('prefixer', function() {
//   gulp.src('app/public/**/*.css')
//     .pipe(plumber())
//     .pipe(autoprefixer({ browsers: ['> 1%', '> IE 7'], cascade: false }))
//     .pipe(gulp.dest('app/public/'));
//     console.info("Prefixer add!!! - " + new Date());
// });
//
// tarea cque compila los archivos de coffeescript a javascript
gulp.task('coffee', function(){
  gulp.src('app/public/**/*.coffee')
  .pipe(plumber())
  .pipe(coffee({bare: true}))
  .pipe(gulp.dest('app/public/'));
});
// realizar tareas por defecto
gulp.task('watch', function() {
  gulp.watch('app/public/**/*.coffee', ['coffee']);
  gulp.watch('app/public/**/*.styl', ['stylus2css']);
});

// arrancar tareas por defecto siempre esta escuchando
gulp.task('default', ['watch']);