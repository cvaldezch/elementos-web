<?php

# echo dirname(__File__);

if (file_exists('vendor/autoload.php')) {
    require 'vendor/autoload.php';
} else {
    echo "<h1>Please install via composer.json</h1>";
    echo "<p>Install Composer instructions: <a href='https://getcomposer.org/doc/00-intro.md#globally'>https://getcomposer.org/doc/00-intro.md#globally</a></p>";
    echo "<p>Once composer is installed navigate to the working directory in your terminal/command promt and enter 'composer install'</p>";
    exit;
}

if (!is_readable('app/Core/Config.php')) {
    die('No Config.php found, configure and rename Config.example.php to Config.php in app/Core.');
}

/*
 *---------------------------------------------------------------
 * APPLICATION ENVIRONMENT
 *---------------------------------------------------------------
 *
 * You can load different configurations depending on your
 * current environment. Setting the environment also influences
 * things like logging and error reporting.
 *
 * This can be set to anything, but default usage is:
 *
 *     development
 *     production
 *
 * NOTE: If you change these, also change the error_reporting() code below
 *
 */
    define('ENVIRONMENT', 'development');
/*
 *---------------------------------------------------------------
 * ERROR REPORTING
 *---------------------------------------------------------------
 *
 * Different environments will require different levels of error reporting.
 * By default development will show errors but production will hide them.
 */

if (defined('ENVIRONMENT')) {
  switch (ENVIRONMENT) {
    case 'development':
      error_reporting(E_ALL);
      break;
    case 'production':
      error_reporting(0);
      break;
    default:
      exit('The application environment is not set correctly.');
  }
}

//initiate config
new Core\Config();

//create alias for Router
use Core\Router;

//define mapping routes
// Urls auth admin
Router::any('login', 'Controllers\Admin\Auth@login');
Router::any('logout', 'Controllers\Admin\Auth@logout');
Router::any('admin', 'Controllers\Admin\Admin@index');
// Urls Config
Router::any('admin/general', 'Controllers\Admin\General@index');
Router::any('wservices/conf', 'Controllers\Admin\General@ResponseJSON');
//Urls keep users
Router::any('admin/users', 'Controllers\Admin\Users@index');
Router::any('admin/users/add', 'Controllers\Admin\Users@add');
Router::any('admin/users/edit/(:num)', 'Controllers\Admin\Users@edit');
Router::any('wservices/users', 'Controllers\Admin\Users@ResponseJSON');
// Users requests
Router::any('admin/users/request', 'Controllers\Admin\Request@index');
Router::any('wservices/request', 'Controllers\Admin\Request@ReponseJSON');

// Urls keep Posts
Router::get('admin/posts', 'Controllers\Admin\Posts@index');
Router::any('admin/posts/add', 'Controllers\Admin\Posts@add');
Router::any('admin/posts/edit/(:num)', 'Controllers\Admin\Posts@edit');
Router::any('admin/posts/disabled/(:num)', 'Controllers\Admin\Posts@disabled');
Router::any('admin/posts/services', 'Controllers\Admin\Posts@ResponseJSON');
// Urls keep Categories
Router::get('admin/categories', 'Controllers\Admin\Category@index');
Router::any('admin/categories/add', 'Controllers\Admin\Category@add');
Router::post('admin/categories/edit', 'Controllers\Admin\Category@edit');
Router::post('admin/categories/del', 'Controllers\Admin\Category@del');
// Urls keep menu
Router::any('admin/menu', 'Controllers\Admin\Menu@index');
Router::any('wservices/menu', 'Controllers\Admin\Menu@ReponseJSON');
// Urls keep submenu
Router::any('admin/submenu', 'Controllers\Admin\Submenu@index');
Router::any('wservices/submenu', 'Controllers\Admin\Submenu@ReponseJSON');
// Urls keep Roles
Router::any('admin/roles', 'Controllers\Admin\Roles@index');
Router::any('wservices/roles', 'Controllers\Admin\Roles@ReponseJSON');

// User register
Router::any('user/register', 'Controllers\Register@usresgister');
Router::any('register/send', 'Controllers\Register@send');
Router::any('register/error', 'Controllers\Register@error');
Router::any('active/register/(:any)/(:any)/(:any)', 'Controllers\Register@valid');
//Profile
Router::any('users/profile', 'Controllers\Profile@index');
Router::post('users/profile/saveprofile', 'Controllers\Profile@saveProfile');
Router::post('users/profile/img/tmp', 'Controllers\Profile@imgTmp');
Router::post('users/profile/save/account', 'Controllers\Profile@saveAccount');
Router::post('users/profile/change/passwd', 'Controllers\Profile@changePasswd');
Router::post('register/restore', 'Controllers\Recover@registerRestore');
Router::get('recover/passwd/(:any)/(:any)', 'Controllers\Recover@recoverPasswd');
Router::post('restore/pwd', 'Controllers\Recover@restorePwd');
Router::post('users/profile/send/publish', 'Controllers\Profile@sendPublish');
Router::post('wservices/post', 'Controllers\Profile@savePost');
Router::any('wservices/profile', 'Controllers\Profile@ReponseJSON');

Router::get('group/(:any)', 'Controllers\Section@index');
Router::get('group/(:any)/section/(:any)', 'Controllers\Section@index');


// index Blog
Router::any('', 'Controllers\blog@index');
Router::any('wservices', 'Controllers\blog@ResponseJSON');
Router::any('(:num)/(:any)', 'Controllers\post@details');
Router::get('(:num)/(:any)/wservices', 'Controllers\post@JSONResponse');
// filter tag post
Router::get('tag/(:any)/filter', 'Controllers\Tags@index');

// Make Password
Router::any('pwd', 'Controllers\Pwd@make');
Router::get('pwd/recovery', 'Controllers\Pwd@recovery');
Router::any('change/img', 'Controllers\Pwd@makeimage');
Router::post('wservices/pwd/send/recover', 'Controllers\Pwd@registerRecover');
Router::get('request/change/passwd/(:any)/(:any)', 'Controllers\Pwd@RestorePasswd');
Router::post('change/passwd', 'Controllers\Pwd@ChangePasswd');

//if no route found
Router::error('Core\Error@index');

//turn on old style routing
Router::$fallback = false;

//execute matched routes
Router::dispatch();
